/// get_dirdist(x1,y1,x2,y2)

// Returns an array containing direction and distance between two points.

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;

var dir_dist = 0;
dir_dist[0] = point_direction(x1,y1,x2,y2);
dir_dist[1] = point_distance(x1,y1,x2,y2);

return dir_dist;
