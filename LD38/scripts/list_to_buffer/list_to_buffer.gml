/// list_to_buffer(list, datatype [, prefix [, include_length=true]])
//  creates a buffer that must be externally deleted to prevent memory leaks
//  Buffer data structure is: [prefix u16] + [number of values u32] + [value1 datatype ] ...
//  datatype must be one of the buffer constants
//  prefix must fit inside a u16, or be undefined

var list  = argument[0];
var type  = argument[1];

if is_undefined(list)
    show_error("Attempting to write an undefined list to buffer!",true);

var prefix         = undefined ;
var include_length = true ;
var prefix_bytes   = 0 ;
var length_bytes   = 4 ;

if(argument_count > 2){ prefix         = argument[2] ;}
if(argument_count > 3){ include_length = argument[3] ;}

if( not include_length )      { length_bytes = 0 ;}
if( not is_undefined(prefix) ){ prefix_bytes = 2 ;}

var idx;

var datapoints = ds_list_size(list);
var typesize = buffer_sizeof(type) ;
var buffer_length = datapoints * typesize+ prefix_bytes + length_bytes
if(type==buffer_string){
    buffer_length = prefix_bytes + length_bytes ;
    for(idx=0;idx<datapoints;idx++){
        buffer_length += string_length(list[|idx]) + 1;
    }
}

var output_buffer = buffer_create( buffer_length, buffer_fixed, 1 );

if(not is_undefined(prefix))
{
    buffer_write(output_buffer,buffer_u16,prefix);
}

if(include_length)
{
    buffer_write(output_buffer,buffer_u32,datapoints);
}


for( idx=0; idx<datapoints; idx++)
{
    buffer_write(output_buffer,type,list[|idx]);
}

return output_buffer ;
