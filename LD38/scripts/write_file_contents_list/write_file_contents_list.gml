/// write_file_contents_list( filename, list_of_strings )
var f_xlp = file_text_open_write( argument0 ) ;
if( f_xlp != -1 )
{
    var oidx_xlp;
    for(oidx_xlp=0;oidx_xlp<ds_list_size(argument1);oidx_xlp++)
    {
        file_text_write_string( f_xlp, string_reverse(base64_encode( ds_list_find_value(argument1,oidx_xlp )))+chr(10)) ;
    }
}
file_text_close(f_xlp) ;
