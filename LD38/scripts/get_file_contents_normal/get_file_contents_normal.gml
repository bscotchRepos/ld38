/// get_file_contents_normal( filename )
//  returns file contents if file exists, else '', without expecting obfuscation
var contents_uipz = "";
if( file_exists( argument0 ) ){
    var f_uipz = file_text_open_read( argument0 ) ;
    while(! file_text_eof(f_uipz) ){
        contents_uipz += file_text_readln( f_uipz ) ;
    }
    file_text_close(f_uipz) ;
}
return contents_uipz
