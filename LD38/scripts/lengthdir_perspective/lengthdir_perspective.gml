/// @arg base_length
/// @arg base_dir
/// @arg point_dir
/// @arg squish_amt

// Squishes lengthdir x and y based on perspective.
// Straight top-down perspective requires no squishing.

// How to determine squish amount:
// If your perspective renders what would ordinarily be a square
//      as a 2:1 rectangle, then the squish amount is 50%. (height/width).

var base_length = argument0;
var base_dir = argument1;
var dir_point = argument2;
var squish_amt = argument3;

var ld = 0;
if squish_amt == 0 {
    ld[0] = length;
    ld[1] = dir;
}
else {
    var rot_x = lengthdir_x(1,dir_point);
    var rot_y = lengthdir_y(1,dir_point)/squish_amt;   
    var new_rotation = point_direction(0,0,rot_x,rot_y);
    
    var new_x = lengthdir_x(base_length, base_dir+new_rotation);
    var new_y = lengthdir_y(base_length*squish_amt, base_dir+new_rotation);
    
    ld[0] = point_distance(0,0,new_x,new_y);
    ld[1] = point_direction(0,0,new_x,new_y);
}
return ld;
