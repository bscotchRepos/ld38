/// @arg sound_id
/// @arg x
/// @arg y
/// @arg z
/// @arg looping

var s = audio_play_sound_at(argument[0],argument[1],argument[2],argument[3],100,850,1,argument[4],aud_get_priority(argument[0]));
audio_sound_gain(s, aud_get_gain(argument[0])*SOUND_SETTING_VOLUME, 0);
