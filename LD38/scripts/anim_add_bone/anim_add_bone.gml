/// @arg sprite
/// @arg parent_id
/// @arg xscale
/// @arg yscale
/// @arg xpos_on_parent
/// @arg ypos_on_parent
/// @arg rotation_offset
/// @arg depth
/// @arg subimage
/// @arg blend_mode

var sp = argument0;
var parent_id = argument1;
var xsc = argument2;
var ysc = argument3;
var xpos_on_parent = argument4;
var ypos_on_parent = argument5;
var rotation_offset = argument6; // If the bone is pre-rotated, by how much?
var bone_depth = argument7;
var bone_subimage = argument8;
var bone_blend = argument9;

var bone_id = array_height_2d(anim_bones);

anim_bones[bone_id, anim_sprite] = sp;
anim_bones[bone_id, anim_parent] = parent_id;
anim_bones[bone_id, anim_xscale] = xsc;
anim_bones[bone_id, anim_yscale] = ysc;
anim_bones[bone_id, anim_rotation_offset] = rotation_offset;
anim_bones[bone_id, anim_depth] = bone_depth;
anim_bones[bone_id, anim_xdraw] = 0;
anim_bones[bone_id, anim_ydraw] = 0;
anim_bones[bone_id, anim_subimage] = bone_subimage;
anim_bones[bone_id, anim_rotation] = 0;
anim_bones[bone_id, anim_rotation_relative] = true;
anim_bones[bone_id, anim_active] = true;
anim_bones[bone_id, anim_xoffset] = 0;
anim_bones[bone_id, anim_yoffset] = 0;
anim_bones[bone_id, anim_offset_relative] = false;
anim_bones[bone_id, anim_rotation_render] = 0;
anim_bones[bone_id, anim_blendmode] = bone_blend;

if parent_id != -1 {
    if anim_bones[parent_id, anim_sprite] == -1 {
        anim_bones[bone_id, anim_dist_from_parent] = point_distance(0,0,xpos_on_parent,ypos_on_parent);
        anim_bones[bone_id, anim_dir_from_parent] = point_direction(0,0,xpos_on_parent,ypos_on_parent);
    }    
    else {
        var par_xoffset = sprite_get_xoffset(anim_bones[parent_id, anim_sprite]);
        var par_yoffset = sprite_get_yoffset(anim_bones[parent_id, anim_sprite]);
        anim_bones[bone_id, anim_dist_from_parent] = point_distance(par_xoffset,par_yoffset,xpos_on_parent,ypos_on_parent);
        anim_bones[bone_id, anim_dir_from_parent] = point_direction(par_xoffset,par_yoffset,xpos_on_parent,ypos_on_parent);
    }
}
else {
    anim_bones[bone_id, anim_dist_from_parent] = point_distance(0,0,xpos_on_parent,ypos_on_parent);
    anim_bones[bone_id, anim_dir_from_parent] = point_direction(0,0,xpos_on_parent,ypos_on_parent);
}

return bone_id;

