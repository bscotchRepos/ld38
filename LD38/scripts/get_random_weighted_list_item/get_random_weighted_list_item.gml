/// get_random_weighted_list_item(list)

// List must be formatted as:
//      Even indices are the elements
//      Odd indices are the weights

var the_list = argument0;
var weight_list = ds_list_create();
var item_list = ds_list_create();

if ds_list_size(the_list) mod 2 != 0 {
    show_error("List is not structured properly to retrieve weighted items.",true);
}

for ( var i = 0; i < ds_list_size(the_list); i+=2) { 
    ds_list_add(item_list,the_list[|i]);
    ds_list_add(weight_list,the_list[|(i+1)]);
}

var chosen_item = ds_list_find_value(item_list, get_list_index_by_weight(weight_list));
ds_list_destroy(weight_list);
ds_list_destroy(item_list);
return chosen_item;
