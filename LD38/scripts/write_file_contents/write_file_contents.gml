/// write_file_contents( filename, contents )
delete_file(argument0);
var f_hzl = file_text_open_write( argument0 ) ;
if( f_hzl != -1 ){
    file_text_write_string( f_hzl, string_reverse(base64_encode( argument1 ))) ;
    file_text_close(f_hzl) ;
    return true ;
}else{
    file_text_close(f_hzl) ;
    return false ;
}
