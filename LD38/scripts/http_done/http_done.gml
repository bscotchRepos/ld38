/// @desc Ask an http_request object if it is done (return true if done or DNE, else false)
/// @param http_request_id
var http_request_id = argument[0];
if( not instance_exists(http_request_id) ){
	return false;
}
with(http_request_id){
	if(done){ return true;}
}

return false;