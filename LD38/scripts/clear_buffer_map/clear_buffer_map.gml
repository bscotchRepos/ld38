/// clear_buffer_map(map_of_buffers)
//  assumes all list components are buffers, deletes them

var bli ;
var blk = ds_map_find_first(argument0);
for(bli=0;bli<ds_map_size(argument0);bli++){
    buffer_delete(argument0[?blk]);
    blk = ds_map_find_next(argument0,blk);
}
ds_map_clear(argument0);
return true;
