/// @arg target_instance
/// @arg target_priority

with o_camera {
    if instance_exists(argument0) {
        ds_list_add(my_targets, argument0);
        ds_list_add(target_weights, argument1);
    }
}
