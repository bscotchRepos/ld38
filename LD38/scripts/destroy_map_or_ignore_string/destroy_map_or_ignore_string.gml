/// @desc Destroy a map or ignore a string. ONLY use on params that are EITHER a map or string!
/// @param map_or_string
var map_or_string = argument[0];
if( ds_map_does_exist(map_or_string) ){
	ds_map_destroy(map_or_string);
}
return true;
