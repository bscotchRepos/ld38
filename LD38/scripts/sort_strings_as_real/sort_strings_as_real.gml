///sort_strings_as_real(list_of_numeric_strings)
// Tally the maps for which map[key]==value
var sorted_2vs8 = ds_list_create() ;

if(ds_exists(argument0,ds_type_list))
{
    if(ds_list_size(argument0)>0)
    {
        // make new list with strings as numbers
        var numeric_list = ds_list_create();
        var i_2vs8;
        for(i_2vs8=0;i_2vs8<ds_list_size(argument0);i_2vs8++)
        {
            ds_list_add(numeric_list,real(argument0[|i_2vs8]));
        }
        
        ds_list_sort(numeric_list,true);
        
        for(i_2vs8=0;i_2vs8<ds_list_size(numeric_list);i_2vs8++)
        {
            ds_list_add(sorted_2vs8,string(numeric_list[|i_2vs8]));
        }
        
        ds_list_destroy(numeric_list) ;
    }
    ds_list_destroy(argument0) ;
}

return sorted_2vs8 ;
