globalvar KEY_DISPLAYS;
KEY_DISPLAYS = ds_map_create();

// This maps keyboard inputs to KEY displays.
ds_map_add(KEY_DISPLAYS, -3, "Left Mouse");
ds_map_add(KEY_DISPLAYS, -2, "Right Mouse");
ds_map_add(KEY_DISPLAYS,ord("A"),"A");
ds_map_add(KEY_DISPLAYS,ord("B"),"B");
ds_map_add(KEY_DISPLAYS,ord("C"),"C");
ds_map_add(KEY_DISPLAYS,ord("D"),"D");
ds_map_add(KEY_DISPLAYS,ord("E"),"E");
ds_map_add(KEY_DISPLAYS,ord("F"),"F");
ds_map_add(KEY_DISPLAYS,ord("G"),"G");
ds_map_add(KEY_DISPLAYS,ord("H"),"H");
ds_map_add(KEY_DISPLAYS,ord("I"),"I");
ds_map_add(KEY_DISPLAYS,ord("J"),"J");
ds_map_add(KEY_DISPLAYS,ord("K"),"K");
ds_map_add(KEY_DISPLAYS,ord("L"),"L");
ds_map_add(KEY_DISPLAYS,ord("M"),"M");
ds_map_add(KEY_DISPLAYS,ord("N"),"N");
ds_map_add(KEY_DISPLAYS,ord("O"),"O");
ds_map_add(KEY_DISPLAYS,ord("P"),"P");
ds_map_add(KEY_DISPLAYS,ord("Q"),"Q");
ds_map_add(KEY_DISPLAYS,ord("R"),"R");
ds_map_add(KEY_DISPLAYS,ord("S"),"S");
ds_map_add(KEY_DISPLAYS,ord("T"),"T");
ds_map_add(KEY_DISPLAYS,ord("U"),"U");
ds_map_add(KEY_DISPLAYS,ord("V"),"V");
ds_map_add(KEY_DISPLAYS,ord("W"),"W");
ds_map_add(KEY_DISPLAYS,ord("X"),"X");
ds_map_add(KEY_DISPLAYS,ord("Y"),"Y");
ds_map_add(KEY_DISPLAYS,ord("Z"),"Z");

ds_map_add(KEY_DISPLAYS,ord("0"),"0");
ds_map_add(KEY_DISPLAYS,ord("1"),"1");
ds_map_add(KEY_DISPLAYS,ord("2"),"2");
ds_map_add(KEY_DISPLAYS,ord("3"),"3");
ds_map_add(KEY_DISPLAYS,ord("4"),"4");
ds_map_add(KEY_DISPLAYS,ord("5"),"5");
ds_map_add(KEY_DISPLAYS,ord("6"),"6");
ds_map_add(KEY_DISPLAYS,ord("7"),"7");
ds_map_add(KEY_DISPLAYS,ord("8"),"8");
ds_map_add(KEY_DISPLAYS,ord("9"),"9");

ds_map_add(KEY_DISPLAYS,189,"-");
ds_map_add(KEY_DISPLAYS,187,"=");

ds_map_add(KEY_DISPLAYS,219,"[");
ds_map_add(KEY_DISPLAYS,221,"]");
ds_map_add(KEY_DISPLAYS,186,";");
ds_map_add(KEY_DISPLAYS,222,"\'");
ds_map_add(KEY_DISPLAYS,188,",");
ds_map_add(KEY_DISPLAYS,190,".");
ds_map_add(KEY_DISPLAYS,220,"\\");
ds_map_add(KEY_DISPLAYS,191,"/");

ds_map_add(KEY_DISPLAYS,vk_up,"Up");
ds_map_add(KEY_DISPLAYS,vk_down,"Dn");
ds_map_add(KEY_DISPLAYS,vk_left,"Lf");
ds_map_add(KEY_DISPLAYS,vk_right,"Rt");
ds_map_add(KEY_DISPLAYS,vk_space,"Sp");

ds_map_add(KEY_DISPLAYS,vk_f1,"F1");
ds_map_add(KEY_DISPLAYS,vk_f2,"F2");
ds_map_add(KEY_DISPLAYS,vk_f3,"F3");
ds_map_add(KEY_DISPLAYS,vk_f4,"F4");
ds_map_add(KEY_DISPLAYS,vk_f5,"F5");
ds_map_add(KEY_DISPLAYS,vk_f6,"F6");
ds_map_add(KEY_DISPLAYS,vk_f7,"F7");
ds_map_add(KEY_DISPLAYS,vk_f8,"F8");
ds_map_add(KEY_DISPLAYS,vk_f9,"F9");
ds_map_add(KEY_DISPLAYS,vk_f10,"F10");
ds_map_add(KEY_DISPLAYS,vk_f11,"F11");
ds_map_add(KEY_DISPLAYS,vk_f12,"F12");
ds_map_add(KEY_DISPLAYS,vk_tab,"Tab");
ds_map_add(KEY_DISPLAYS,vk_lalt,"LAlt");
ds_map_add(KEY_DISPLAYS,vk_lcontrol,"LCtl");
ds_map_add(KEY_DISPLAYS,vk_ralt,"RAlt");
ds_map_add(KEY_DISPLAYS,vk_rcontrol,"RCtl");
ds_map_add(KEY_DISPLAYS,9,"Tab");
ds_map_add(KEY_DISPLAYS,vk_lshift,"LSh");
ds_map_add(KEY_DISPLAYS,vk_rshift,"RSh");

ds_map_add(KEY_DISPLAYS,vk_delete,"Del");
ds_map_add(KEY_DISPLAYS,vk_insert,"Ins");
ds_map_add(KEY_DISPLAYS,vk_home,"Home");
ds_map_add(KEY_DISPLAYS,vk_end,"End");
ds_map_add(KEY_DISPLAYS,vk_pagedown,"PgDn");
ds_map_add(KEY_DISPLAYS,vk_pageup,"PgUp");

ds_map_add(KEY_DISPLAYS,vk_escape,"Esc");
ds_map_add(KEY_DISPLAYS,vk_backspace,"Bk");

ds_map_add(KEY_DISPLAYS,vk_numpad0,"N0");
ds_map_add(KEY_DISPLAYS,vk_numpad1,"N1");
ds_map_add(KEY_DISPLAYS,vk_numpad2,"N2");
ds_map_add(KEY_DISPLAYS,vk_numpad3,"N3");
ds_map_add(KEY_DISPLAYS,vk_numpad4,"N4");
ds_map_add(KEY_DISPLAYS,vk_numpad5,"N5");
ds_map_add(KEY_DISPLAYS,vk_numpad6,"N6");
ds_map_add(KEY_DISPLAYS,vk_numpad7,"N7");
ds_map_add(KEY_DISPLAYS,vk_numpad8,"N8");
ds_map_add(KEY_DISPLAYS,vk_numpad9,"N9");

ds_map_add(KEY_DISPLAYS,vk_decimal,"N.");
ds_map_add(KEY_DISPLAYS,vk_divide,"N/");
ds_map_add(KEY_DISPLAYS,vk_multiply,"N*");
ds_map_add(KEY_DISPLAYS,vk_subtract,"N-");
ds_map_add(KEY_DISPLAYS,vk_add,"N+");


globalvar GAMEPAD_KEY_DISPLAYS; GAMEPAD_KEY_DISPLAYS = ds_map_create();

ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_axislh, "Left Stick");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_axislv, "Left Stick");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_axisrh, "Right Stick");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_axisrv, "Right Stick");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_face1, "A");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_face2, "B");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_face3, "X");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_face4, "Y");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_padu, "Dpad Up");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_padd, "Dpad Down");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_padl, "Dpad Left");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_padr, "Dpad Right");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_select, "Select");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_shoulderl, "L Bumper");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_shoulderlb, "L Trigger");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_shoulderr, "R Bumper");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_shoulderrb, "R Trigger");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_start, "Start");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_stickl, "L Stick Click");
ds_map_add(GAMEPAD_KEY_DISPLAYS, gp_stickr, "R Stick Click");

globalvar GAMEPAD_COLORS;
GAMEPAD_COLORS = ds_map_create();
ds_map_add(GAMEPAD_COLORS, gp_face1, make_colour_rgb(25,255,0));
ds_map_add(GAMEPAD_COLORS, gp_face2, make_colour_rgb(255,25,0));
ds_map_add(GAMEPAD_COLORS, gp_face3, make_colour_rgb(25,100,255));
ds_map_add(GAMEPAD_COLORS, gp_face4, make_colour_rgb(255,255,25));