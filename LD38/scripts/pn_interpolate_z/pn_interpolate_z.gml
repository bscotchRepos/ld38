/// interpolate_z(grid_x,grid_y,wavelength,seed_modifier)

var xx = argument0, yy = argument1, waveL = argument2, z, seedmod = argument3;
// Interpolate Z based on surrounding things.
    var x_to_chunk = floor(xx/waveL),
        y_to_chunk = floor(yy/waveL),
        xchunk_xx = x_to_chunk*waveL,
        ychunk_yy = y_to_chunk*waveL,
        z;
        
// The value of Z should be interpolated.
if (xx == xchunk_xx && yy == ychunk_yy) {
    z = pn_get_base_noise(xx,yy,waveL,seedmod);
}
else {
    // PROPORTIONS OF INFLUENCE N SHIT.
    var next_xchunk = x_to_chunk+1,
        next_ychunk = y_to_chunk+1,
        next_xx = next_xchunk*waveL,
        next_yy = next_ychunk*waveL,        
        influence_dist = waveL;
    
    var x0y0 = max(1-point_distance(xx,yy,xchunk_xx,ychunk_yy)/influence_dist ,0),
        x1y0 = max(1-point_distance(xx,yy,next_xx,ychunk_yy)/influence_dist ,0),
        x0y1 = max(1-point_distance(xx,yy,xchunk_xx,next_yy)/influence_dist ,0),
        x1y1 = max(1-point_distance(xx,yy,next_xx,next_yy)/influence_dist ,0);

    z = x0y0*pn_get_base_noise(xchunk_xx,ychunk_yy,waveL,seedmod)+
        x1y0*pn_get_base_noise(next_xx,ychunk_yy,waveL,seedmod)+
        x0y1*pn_get_base_noise(xchunk_xx,next_yy,waveL,seedmod)+
        x1y1*pn_get_base_noise(next_xx,next_yy,waveL,seedmod)
}       
return z;
