/// @arg channel
/// @arg amt
/// @arg percentage_boolean
/// @arg [trigger_cooldown]

var ch = argument[0];
var amt = argument[1];
var percentage_boolean = argument[2];
var trigger_cooldown = resources[ch, rscinfo_maxcool] > 0;

if argument_count > 3 {
    trigger_cooldown = argument[3];
}

// Returns true if the resources was successfully spent.

var can_spend = false;
if percentage_boolean {
    amt = amt*resources[ch,rscinfo_max];
}

can_spend = resources[ch,rscinfo_current] >= abs(amt) || amt > 0;

if can_spend {
    if trigger_cooldown {
        resources[ch,rscinfo_cooldown] = max(resources[ch,rscinfo_cooldown], resources[ch,rscinfo_maxcool])
    }
    resources[ch,rscinfo_current] = clamp(resources[ch,rscinfo_current] + amt, 0, resources[ch,rscinfo_max]);
}

return can_spend;
