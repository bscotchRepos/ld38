/// string_split(string,separator)
//  returns array of strings, split on the separator
var separator = argument1;
var to_split  = argument0;
var exploded ;
exploded[0] = "" ;
var si = 1 ;
var exploded_idx = 0 ;
while(true){
    // Get the next character in the string
    if(si>string_length(to_split)){break;}
    var char = string_char_at(to_split,si);
    if(char==separator){exploded_idx++;exploded[exploded_idx]="";}
    else{exploded[exploded_idx]+= char;}
    si++;
}
return exploded ;
