/// write_file_contents_list_append( filename, string_or_list_of_strings )
var contents_gs9v = get_file_contents_list(argument0) ;
if(is_string(argument1))
{
    ds_list_add(contents_gs9v,argument1);
}
else
{
    var i_gs9v ;
    for( i_gs9v=0 ; i_gs9v<ds_list_size(argument1) ; i_gs9v++ )
    {
        ds_list_add(contents_gs9v,argument1[|i_gs9v]);
    }
}
write_file_contents_list(argument0,contents_gs9v);
ds_list_destroy(contents_gs9v);
