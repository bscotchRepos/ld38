/// clear_list_of_lists(list_of_lists)
var l = argument0;

for (var i = 0; i < ds_list_size(l); i++){
    ds_list_destroy(l[|i]);
}
ds_list_clear(l);
