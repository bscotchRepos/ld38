/// @arg x
/// @arg y

var xx = argument0;
var yy = argument1;

var item = noone;
var d = 10000;
with o_interface_item {
    if depth < d {
        if (abs(x-xx) <= width*.5 && abs(y-yy) <= height*.5) || point_distance(x,y,xx,yy) <= radius {
            item = id;
            d = depth;
        }
    }
}

return item;
