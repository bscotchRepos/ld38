/// file_bin_write(bin_file_handle,type,value)
var file_start = file_bin_position(argument[0])

// convert the value into a buffer
var bytes = buffer_sizeof(argument[1])
var tempbuff = buffer_create(bytes,buffer_fixed,1)
buffer_write(tempbuff,argument[1],argument[2])

// read them back and write to the file
buffer_seek(tempbuff,buffer_seek_start,0)
for(var b=0; b<bytes; b++){
    var byte_value = buffer_read(tempbuff,buffer_u8)
    file_bin_write_byte(argument[0],byte_value)
}
buffer_delete(tempbuff)

// if successful, will have advanced file position appropriately
return (file_bin_position(argument[0])-file_start)==bytes
