/// @arg channel_id
/// @arg x0
/// @arg y0
/// @arg x1
/// @arg y1

var chid    = argument[0];
var x0      = argument[1];
var y0      = argument[2];
var x1      = argument[3];
var y1      = argument[4];

var ckdev = -1;

// MOUSE WHEEL
if sc_allow[chid,sc_mwheel_vert[chid]] {
	var mx = x_to_gui(mouse_x);
	var my = y_to_gui(mouse_y);
    if mouse_wheel_up() {
        if point_in_rectangle(mx,my,x0,y0,x1,y1) {        
                sc_targ[chid,sc_mwheel_vert[chid]] += view_hview()*.5;
        }
    }
    if mouse_wheel_down() {
        if point_in_rectangle(mx,my,x0,y0,x1,y1) {
            sc_targ[chid,sc_mwheel_vert[chid]] -= view_hview()*.5;            
        }
    }
}

// HOTKEYS
if !on_mobile() {
    var sc_spd = view_wview()*SECONDS_SINCE_UPDATE;
    
    if sc_hk_up[chid] != -1 {
        if keyboard_check(sc_hk_up[chid]) {
            sc_targ[chid,1] += sc_spd;
        }
    }
    if sc_hk_down[chid] != -1 {
        if keyboard_check(sc_hk_down[chid]) {
            sc_targ[chid,1] -= sc_spd;
        }
    }
    if sc_hk_left[chid] != -1 {
        if keyboard_check(sc_hk_left[chid]) {
            sc_targ[chid,0] -= sc_spd;
        }
    }
    if sc_hk_right[chid] != -1 {
        if keyboard_check(sc_hk_right[chid]) {
            sc_targ[chid,0] += sc_spd;
        }
    }
}

// Gamepad
check_show_gamepad();
if CURRENT_GAMEPAD >= 0 {
    var gpaxis = 0;
    gpaxis[0] = 0;
    gpaxis[1] = 0;
    
    if sc_gplaxis[chid] {
        var gpx = get_gamepad_axis(CURRENT_GAMEPAD,gp_axislh,gp_axislv,false);
        gpaxis[0] = gpx[0];
        gpaxis[1] = -gpx[1];        
    }
    if sc_gpraxis[chid] {
        var gpx = get_gamepad_axis(CURRENT_GAMEPAD,gp_axisrh,gp_axisrv,false);
        var dd = get_dirdist(0,0,gpx[0],gpx[1]);
        var cur_dd = get_dirdist(0,0,gpaxis[0],gpaxis[1]);
        if dd[1] > cur_dd[1] {
            gpaxis[0] = gpx[0];
            gpaxis[1] = -gpx[1];
        }
    }
    if sc_gpdpad[chid] {
        var gpx = get_gamepad_axis(CURRENT_GAMEPAD,-1,-1,true);
        var dd = get_dirdist(0,0,gpx[0],gpx[1]);
        var cur_dd = get_dirdist(0,0,gpaxis[0],gpaxis[1]);
        if dd[1] > cur_dd[1] {
            gpaxis[0] = gpx[0];
            gpaxis[1] = -gpx[1];
        }        
    }
    
    var dirdist = get_dirdist(0,0,gpaxis[0],gpaxis[1]);
    if dirdist[1] >= GAMEPAD_DEADZONE {
        // SCROLL DAT SHIIIZZZZ. X and Y independently.
        var scspeed = view_wview()*SECONDS_SINCE_UPDATE;
        for ( var i = 0; i <= 1; i++) {
            if sc_allow[chid,i] {
                sc_targ[chid,i] += gpaxis[i]*scspeed;
            }
        }
    }
}


// DO SCROLLING!
if sc_device[chid] == -1 {    
    sc_clicktimer[chid] = 0;
    for (var d = 0; d < 5; d++){
        var mpos;
        mpos[0] = device_mouse_x_to_gui(d);
        mpos[1] = device_mouse_y_to_gui(d);
        if device_mouse_check_button_pressed(d, mb_left) {
            if point_in_rectangle(mpos[0],mpos[1],x0,y0,x1,y1) {
                for ( var i = 0; i <= 1; i++) {
                    if sc_allow[chid,i] { // THIS AXIS IS ALLOWED! DO IT! DO IT NOW!
                        sc_anchor[chid,i] = mpos[i];
                        sc_last[chid,i] = sc_offset[chid,i];
                        sc_device[chid] = d;
                        ds_list_clear(sc_lastpositions[chid,i]);
                    }
                }
            }
        }                    
    }
}

if ( sc_device[chid] != -1 ) { 
    sc_clicktimer[chid] += SECONDS_SINCE_UPDATE;
    var m;
    m[0] = device_mouse_x_to_gui(sc_device[chid]);
    m[1] = device_mouse_y_to_gui(sc_device[chid]);
    
    if device_mouse_check_button(sc_device[chid], mb_left) {
        for ( var i = 0; i <= 1; i++) {
            if sc_allow[chid,i] {
                //sc_offset[chid,i] = sc_last[chid,i] + (m[i]-sc_anchor[chid,i]);
                sc_targ[chid,i] = sc_last[chid,i] + (m[i]-sc_anchor[chid,i]);//sc_offset[chid,i];
                ds_list_insert(sc_lastpositions[chid,i],0,sc_targ[chid,i]);
                if ds_list_size(sc_lastpositions[chid,i]) > 3 {
                    ds_list_delete(sc_lastpositions[chid,i], ds_list_size(sc_lastpositions[chid,i])-1);
                }
            }
        }
    }
    else {
        for ( var i = 0; i <= 1; i++){
            // Set the speed!
            if ds_list_size(sc_lastpositions[chid,i]) > 1 {
                sc_speed[chid,i] = ds_list_find_value(sc_lastpositions[chid,i],0) - ds_list_find_value(sc_lastpositions[chid,i],ds_list_size(sc_lastpositions[chid,i])-1);
            }
        }
        // Should we classify this as a click?
        if sc_clicktimer[chid] <= .3 && point_distance(sc_last[chid,0],sc_last[chid,1],sc_targ[chid,0],sc_targ[chid,1]) < 30 {
            ckdev = sc_device[chid];
        }        
        sc_device[chid] = -1;
    }
}

for ( var i = 0; i <= 1; i++) {
    if sc_allow[chid,i] {
        sc_targ[chid,i] += sc_speed[chid,i];
        sc_speed[chid,i] *= .93;
        sc_targ[chid,i] = clamp( sc_targ[chid,i], sc_min[chid,i], sc_max[chid,i] );   
        sc_offset[chid,i] += tween(sc_offset[chid,i], sc_targ[chid,i], .4);
    }
}

sc_click_cooldown[chid] = max(0,sc_click_cooldown[chid]-SECONDS_SINCE_UPDATE);

return ckdev;
