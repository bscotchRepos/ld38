/// @arg sound_id

if ds_map_exists(SOUND_GAINS, argument0)
	return SOUND_GAINS[?argument0];
return 1;