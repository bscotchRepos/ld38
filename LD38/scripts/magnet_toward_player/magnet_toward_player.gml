/// @arg upgrade_reference

var my_upg = argument0;
var me = id
var fly_dir = 0;
var pull_strength = 0;
var pullstrength_base = upgrade_get_effect(argument0);
	
with o_player{
	var v_dis = max(0,1-point_distance(me.x,me.y,x,y)/(PULL_RANGE+hitbox+me.hitbox));	
	if v_dis > 0 { v_dis = max(v_dis, .15);
		pull_strength = v_dis * pullstrength_base;
		fly_dir = point_direction(me.x,me.y,x,y);
	}
}
x += lengthdir_x(pull_strength*SLOMO_SECONDS, fly_dir);
y += lengthdir_y(pull_strength*SLOMO_SECONDS, fly_dir);