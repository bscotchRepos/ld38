/// draw_nine_slice(sprite,x,y,width,height,border_pixels)

var reference_sprite = argument0, xx = argument1, yy = argument2, w = argument3, h = argument4, border = argument5;

var start_width = sprite_get_width(reference_sprite)-2*border;
var start_height = sprite_get_height(reference_sprite)-2*border;
var new_width  = w-2*border;
var new_height = h-2*border;
var xscale = new_width/start_width;
var yscale = new_height/start_height;

// Top row
var top = 0;
var ydraw = yy;
draw_sprite_part_ext(reference_sprite,0,0,top,border,border,xx,ydraw,1,1,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border,top,start_width,border,xx+border,ydraw,xscale,1,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border+start_width,top,border,border,xx+new_width+border,ydraw,1,1,c_white,1);

// Middle row
top = border;
ydraw = yy+border;
draw_sprite_part_ext(reference_sprite,0,0,border,top,start_height,xx,ydraw,1,yscale,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border,top,start_width,start_height,xx+border,ydraw,xscale,yscale,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border+start_width,top,border,start_height,xx+new_width+border,ydraw,1,yscale,c_white,1);

// Bottom row
top = border+start_height;
ydraw = yy+border+new_height;
draw_sprite_part_ext(reference_sprite,0,0,top,border,border,xx,ydraw,1,1,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border,top,start_width,border,xx+border,ydraw,xscale,1,c_white,1);
draw_sprite_part_ext(reference_sprite,0,border+start_width,top,border,border,xx+new_width+border,ydraw,1,1,c_white,1);
