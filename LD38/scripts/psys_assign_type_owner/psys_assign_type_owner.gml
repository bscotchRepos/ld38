/// psys_assign_type_owner(p_type, ps_owner)
var ptype = argument[0];
var ps_owner = argument[1];

with o_particle_manager {
    var ptype_list = -1;
    if !ds_map_exists(particle_types, ps_owner) {
        ptype_list = ds_list_create();
        ds_map_add_list(particle_types, ps_owner, ptype_list);    
    }
    else {
        ptype_list = ds_map_find_value(particle_types, ps_owner);
    }
    
    if ptype_list == -1 {
        show_error("Tried to create a particle type for a particle system that does not exist.", true);
    }
    else ds_list_add(ptype_list, ptype);
}

