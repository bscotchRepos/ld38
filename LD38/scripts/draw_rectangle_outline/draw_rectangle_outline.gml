/// @arg x1
/// @arg y1
/// @arg x2
/// @arg y2
/// @arg linewidth
/// @arg linecolor

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var linewidth = argument4;
var linecolor = argument5;

draw_line_width_colour(x1,y1,x2,y1,linewidth,linecolor,linecolor);
draw_line_width_colour(x1,y2,x2,y2,linewidth,linecolor,linecolor);
draw_line_width_colour(x1,y1,x1,y2,linewidth,linecolor,linecolor);
draw_line_width_colour(x2,y1,x2,y2,linewidth,linecolor,linecolor);

