/// get_file_contents_list( filename )
//  returns file contents as list (one per line), else an empty list
var out_qzo = ds_list_create();
if( file_exists( argument0 ) )
{
    var f_qzo = file_text_open_read( argument0 ) ;
    while(not file_text_eof(f_qzo))
    {
        var contents_qzo = file_text_read_string( f_qzo ) ;
        if(contents_qzo==""){continue;}
        contents_qzo = base64_decode(string_reverse(contents_qzo)) ;
        file_text_readln( f_qzo )
        ds_list_add(out_qzo,contents_qzo);
    }
    file_text_close(f_qzo) ;
}
return out_qzo;
