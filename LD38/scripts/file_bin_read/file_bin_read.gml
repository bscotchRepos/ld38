/// file_bin_read(bin_file_handle,type)
var val = "";
if(argument[1]==buffer_string){
    // read until hit a nullbyte
    var byte ;
    while(true){
        byte=file_bin_read_byte(argument[0]);
        if(byte==0){break;}
        else{ val += chr(byte) }
    }
}
else{
    var bytes = buffer_sizeof(argument[1])
    var tempbuff = buffer_create(bytes,buffer_fixed,1)
    for(var b=0; b<bytes; b++){
        var byte_value = file_bin_read_byte(argument[0])
        buffer_write(tempbuff,buffer_u8,byte_value)
    }
    buffer_seek(tempbuff,buffer_seek_start,0)
    val = buffer_read(tempbuff,argument[1])
    buffer_delete(tempbuff)
}
return val
