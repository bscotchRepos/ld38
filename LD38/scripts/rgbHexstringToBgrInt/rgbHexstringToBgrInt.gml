/// rgbHexstringToBgrInt( rgb_hexstring )
//  For converting rgb values as string representations of hexadecimal
//  to bgr integer values for gamemaker colors. Input should be 6 characters
//  e.g. ff00ff ould be max red with max blue.
hexstring = string_lower( argument0 );
hexLookupTable = ds_map_create() ;
ds_map_add( hexLookupTable, "0", 0 ) ;
ds_map_add( hexLookupTable, "1", 1 ) ;
ds_map_add( hexLookupTable, "2", 2 ) ;
ds_map_add( hexLookupTable, "3", 3 ) ;
ds_map_add( hexLookupTable, "4", 4 ) ;
ds_map_add( hexLookupTable, "5", 5 ) ;
ds_map_add( hexLookupTable, "6", 6 ) ;
ds_map_add( hexLookupTable, "7", 7 ) ;
ds_map_add( hexLookupTable, "8", 8 ) ;
ds_map_add( hexLookupTable, "9", 9 ) ;
ds_map_add( hexLookupTable, "a", 10 ) ;
ds_map_add( hexLookupTable, "b", 11 ) ;
ds_map_add( hexLookupTable, "c", 12 ) ;
ds_map_add( hexLookupTable, "d", 13 ) ;
ds_map_add( hexLookupTable, "e", 14 ) ;
ds_map_add( hexLookupTable, "f", 15 ) ;
// Gamemaker uses BRG instead of RGB for some reason, so need to
// swap the first two with the last two characters
hexstring = string_copy(hexstring,5,2)+string_copy(hexstring,3,2)+string_copy(hexstring,1,2) ;
integer      = 0 ;
stringLength = string_length(hexstring)
for( place = 0 ; place<stringLength ; place++ ){
    integer += power(16,place) * ds_map_find_value(hexLookupTable,string_char_at (hexstring, stringLength-place )) ;
}
ds_map_destroy( hexLookupTable ) ;
return( integer ) ;
