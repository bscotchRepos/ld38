/// @arg xmid
/// @arg ymid
/// @arg width
/// @arg height
/// @arg vert_spacing
/// @arg font
/// @arg category_list
/// @arg category_contents_map
/// @arg id_names_map
/// @arg id_sprites_map
/// @arg folder_text_maxscale
/// @arg item_text_maxscale

// Needs:
	// List of categories
	// Map of lists of category contents (the list contains ID numbers)
	// Map of names associated with the ID numbers
	// Map of sprite IDs associated with the ID numbers

with instance_create(argument[0],argument[1],o_categorizer) {
	// To set upon spawning.
	width				= argument[2];
	height				= argument[3];
	selector_height		= argument[4]; // Vertical spacing
	font				= argument[5];	
	category_list		= argument[6];
	category_contents	= argument[7];
	id_names			= argument[8];
	id_sprites			= argument[9];
	folder_maxscale		= argument[10];
	text_maxscale		= argument[11];
	return id;
}