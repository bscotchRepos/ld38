// Returns the state of the mouse wheel.
if mouse_wheel_up() { return 1; }
if mouse_wheel_down() { return -1; }
return 0;