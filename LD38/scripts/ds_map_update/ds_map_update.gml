/// @desc Replace the values of one map with the values of another
/// @param map
/// @param update_map
/// @param [destroy_update_map=false]
var map = argument[0];
var update_map = argument[1];
var destroy_update_map = false;
if(argument_count>2){ destroy_update_map=argument[2]; }

if( ds_map_does_exist(map) && ds_map_does_exist(update_map)){
	if(ds_map_size(update_map)>0){
		var key = ds_map_find_first(update_map);
		for(var i=0; i<ds_map_size(update_map); i++){
			map[?key] = update_map[?key];
			key = ds_map_find_next(update_map,key);
		}
	}
	if(destroy_update_map){ ds_map_destroy(update_map);}
}

return map;