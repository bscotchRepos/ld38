/// get_zspeed_dist_movespd_ht(distance,movespeed,start_z,end_z)

var dist = argument0;
var mvspd = argument1;
var start_z = argument2;
var end_z = argument3;

var time = dist/max(.001,abs(mvspd));
// Movespeed is in "pixels per second"
// Gravity is in "pixels per second"
// Zspeed is in "pixels per second"

var new_zspeed = (end_z - start_z + GRAVITY*power(time,2)/2)/time;
return new_zspeed;

//V = z_final - z_initial + g*(t^2)/2
