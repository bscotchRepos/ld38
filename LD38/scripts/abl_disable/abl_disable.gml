//// abl_disable(ability_id_or_name)

var abl_to_disable = argument0;

if is_string(abl_to_disable) {
    var namecheck = string_lower(abl_to_disable);
    var found = false;
    for ( var i = 0; i < array_height_2d(abilities) && !found; i++) {
        if string_lower(abilities[i, abl_name]) == namecheck {
            found = true;
            abl_to_disable = i;
        }
    }
}

ds_list_delete_item(abilities_enabled, abl_to_disable);
abilities[abl_to_disable, abl_enabled] = false;
