/// @arg x
/// @arg y
/// @arg width
/// @arg margins
/// @arg hd_font Headers
/// @arg hd_scale
/// @arg hd_spacing_below
/// @arg hd_h_align
/// @arg hd_color
/// @arg bd_font Bodies
/// @arg bd_scale
/// @arg bd_spacing_below
/// @arg bd_h_align
/// @arg bd_color
/// @arg corner_radius
/// @arg bg_color
/// @arg bg_alpha
/// @arg text
/// @arg text_type
/// @arg [text]
/// @arg [text_type]
/// @arg [text]...

with instance_create(argument[0], argument[1], o_generic_text_box) {

	width = argument[2];
	margins = argument[3];
	
	header_font = argument[4];
	header_scale = argument[5];
	header_spacing_below = argument[6];
	header_alignment = argument[7];
	header_color = argument[8];

	body_font = argument[9];
	body_scale = argument[10];
	body_spacing_below = argument[11];
	body_alignment = argument[12];
	body_color = argument[13];

	corner_radius = argument[14];

	bg_color = argument[15];
	bg_alpha = argument[16];

	for ( var i = 17; i < argument_count; i++){
		ds_list_add(my_text, argument[i]);
	}
	
	return id;
}