/// urand_make_random_buffer( seed, length, max )
var current = argument[0];
var length  = argument[1];
var maxval  = argument[2];
var out_buffer = buffer_create(length*2,buffer_fixed,1);

for(i=0;i<length;i++){
    current = urand_random(current);
    buffer_write(out_buffer,buffer_u16,current / 65536 /32767*maxval);
}
return out_buffer;
