/// @arg target_direction

var tdir = argument0+random_range(-mv_turn_randomness,mv_turn_randomness);

if tdir < 0 { tdir += 360; }
if tdir > 360 { tdir -= 360; }

if mv_dirchange_maxtime > 0 {
    mv_turn_timer -= SLOMO_SECONDS;
    if mv_turn_timer <= 0 {
        mv_turn_timer = random_range( mv_dirchange_mintime, mv_dirchange_maxtime );
        mv_movedir_target = tdir;
    }
}
else mv_movedir_target = tdir;

var ts = mv_turn_speed*SLOMO_SECONDS;

if ts < 0 {
    mv_movedir = mv_movedir_target mod 360;
}
else {
    mv_movedir += clamp(angle_diff(mv_movedir_target,mv_movedir), -ts, ts);
    mv_movedir = mv_movedir mod 360;
    if mv_movedir < 0 { mv_movedir += 360; }
}

