/// coord_map_to_buffer(map, datatype [, prefix [, include_length=true]])
//  creates a buffer that must be externally deleted to prevent memory leaks
//  Input map is expected to be keyed by u32 encoding of xy coords
//  Buffer data structure is: [prefix u16] + [number of values u32] + [value1 datatype ] ...
//  datatype must be one of the buffer constants
//  prefix must fit inside a u16, or be undefined

var map  = argument[0];
var type  = argument[1];

var prefix         = undefined ;
var include_length = true ;
var prefix_bytes   = 0 ;
var length_bytes   = 4 ;

if(argument_count > 2){ prefix         = argument[2] ;}
if(argument_count > 3){ include_length = argument[3] ;}

if( not include_length )      { length_bytes = 0 ;}
if( not is_undefined(prefix) ){ prefix_bytes = 2 ;}


var datapoints = ds_map_size(map);
var typesize = buffer_sizeof(type) ;
var value_length = datapoints * typesize;
if(type==buffer_string){
    value_length=0;
    var key = ds_map_find_first(map);
    for(idx=0;idx<datapoints;idx++){
        value_length += string_length(map[?key]) + 1;
        key = ds_map_find_next(map,key);
    }
}
var output_buffer = buffer_create( value_length+datapoints*4 + 
                                   prefix_bytes + 
                                   length_bytes, buffer_fixed, 1 );

if(not is_undefined(prefix))
{
    buffer_write(output_buffer,buffer_u16,prefix);
}

if(include_length)
{
    buffer_write(output_buffer,buffer_u32,datapoints);
}

var idx;
var coordkey=ds_map_find_first(map);
for( idx=0; idx<datapoints; idx++)
{
    buffer_write(output_buffer,buffer_s32,coordkey);
    buffer_write(output_buffer,type,map[?coordkey]);
    coordkey=ds_map_find_next(map,coordkey);
}
return output_buffer ;
