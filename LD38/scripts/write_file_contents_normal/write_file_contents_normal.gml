/// write_file_contents( filename, string )
delete_file(argument0);
var success = false;
var f_hzl = file_text_open_write( argument0 ) ;
if( f_hzl != -1 ){
    file_text_write_string( f_hzl, argument1 ) ;
    success = true ;
}
file_text_close(f_hzl) ;
return success ;

