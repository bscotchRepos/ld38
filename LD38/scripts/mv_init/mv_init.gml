/// @arg base_speed
/// @arg accel_%
/// @arg decel_%
/// @arg turnspeed
/// @arg turn_randomness
/// @arg directionchange_mintime
/// @arg directionchange_maxtime

// This script is purely for movement on a 2D plane.
// Gravity, etc... are not taken into account.

mv_speed_base = argument[0];
mv_accel = argument[1]*mv_speed_base;
mv_decel = argument[2]*mv_speed_base;
mv_turn_speed = argument[3]; // Set turnspeed to -1 for instant turning
mv_turn_randomness = argument[4];
mv_dirchange_mintime = argument[5];
mv_dirchange_maxtime = argument[6];

mv_movespeed = 0;
mv_turn_timer = 0;
mv_movedir = 0;
mv_movedir_target = 0;
