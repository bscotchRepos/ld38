/// @arg channel
/// @arg xmin
/// @arg xmax
/// @arg ymin
/// @arg ymax

var channel = argument[0];
var xmin    = argument[1];
var xmax    = argument[2];
var ymin    = argument[3];
var ymax    = argument[4];

sc_min[channel,0] = xmin;
sc_min[channel,1] = ymin;
sc_max[channel,0] = xmax;
sc_max[channel,1] = ymax;
