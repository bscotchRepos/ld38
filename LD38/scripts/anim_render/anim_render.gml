/// @arg xdraw
/// @arg ydraw
/// @arg xscale
/// @arg yscale
/// @arg color
/// @arg rotation
var xd = argument0;
var yd = argument1;
var xsc = argument2;
var ysc = argument3;
var color = argument4;
var rot = argument5;

anim_process_bone_positions();

for ( var i = 0; i < ds_list_size(anim_draw_order); i++) {
    var bone_id = anim_draw_order[|i];
    
    if anim_bones[bone_id, anim_sprite] != -1 && anim_bones[bone_id, anim_active] {
        gpu_set_blendmode(anim_bones[bone_id, anim_blendmode])
        draw_sprite_ext(
            anim_bones[bone_id, anim_sprite],
            anim_bones[bone_id, anim_subimage],
            xd+anim_bones[bone_id, anim_xdraw]*xsc,
            yd+anim_bones[bone_id, anim_ydraw]*ysc,
            anim_bones[bone_id, anim_xscale]*xsc,
            anim_bones[bone_id, anim_yscale]*ysc,
            (anim_bones[bone_id, anim_rotation_render])*sign(xsc),
            color, 1);        
    }    
}
gpu_set_blendmode(bm_normal)

