/// @arg base_color
/// @arg tint_color

var base_color = argument0;
var tint_color = argument1;

/// Tint one color by another one.

return make_colour_rgb(
	color_get_red(base_color)*(color_get_red(tint_color)/255),
	color_get_green(base_color)*(color_get_green(tint_color)/255),
	color_get_blue(base_color)*(color_get_blue(tint_color)/255));