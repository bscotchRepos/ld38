#macro upg_movespeed 0
#macro upg_inflate_time 1
#macro upg_balloom_magnet 2
#macro upg_hp 3
#macro upg_fatitude 4
#macro upg_mallowfind 5
#macro upg_dirbpoke 6
#macro upg_beak_magnet 7
#macro upg_mallow_magnet 8

#macro upginfo_name 0
#macro upginfo_level 1
#macro upginfo_maxlevel	2
#macro upginfo_cost_mod	3
#macro upginfo_description 4
#macro upginfo_baseline 5
#macro upginfo_change 6
#macro upginfo_change_linear 7
#macro upginfo_label 8
#macro upginfo_rounded 9

globalvar BOOST_LEVELS; BOOST_LEVELS = 2;
globalvar UPGRADES; UPGRADES = 0;
globalvar UPGRADE_BASE_COST; UPGRADE_BASE_COST = 50;
globalvar UPGRADE_COST_GROWTH; UPGRADE_COST_GROWTH = 1;

upgrade_setup(upg_movespeed, "Speud", "Dregume faster around the world!", PLAYER_MOVESPEED_SMALL, 5, PLAYER_MOVESPEED_SMALL*.1, true, 1, "Speeds", false);
upgrade_setup(upg_inflate_time, "Mallowboop", "Get longer boop from MALLOW EAT!", INFLATE_BASE_TIME, 4, .25, true, 1.25, "Seconds", false);
upgrade_setup(upg_balloom_magnet, "Magnamooms", "Ballooms fly right to your shiny cheek!", 0, 8, 200, true, 2, "Balloomsuck Power", true);
upgrade_setup(upg_hp, "Terfness", "GET TERF AND SHOW DIRBS WHO'S THE BEAN!", 50, 10, .5, false, 1.5, "Hardbody", true);
upgrade_setup(upg_fatitude, "Beanitude", "Get bounced less from them there dang DIRBS!", 0, 5, .1, true, 3, "Beanness", false);
upgrade_setup(upg_mallowfind, "Mallowfind", "Those mallows not so hard to get NOW!", 0, 5, .05, true, 3, "Findy", false);
upgrade_setup(upg_dirbpoke, "Dirbpokes", "The only good dirb is a POKED dirb!", 50, 10, .5, false, 1.1, "Damage", true);
upgrade_setup(upg_mallow_magnet, "Mallow Magnet", "Get those MALLOWS in your MOUTH!", 0, 8, 200, true, 2, "Mallowsuck Power", true);
upgrade_setup(upg_beak_magnet, "Baek Magnet", "SUCK THOSE BAEKS!",0, 8, 200, true, 2, "Baeksuck Power", true);

var total_cost = 0;
for ( var u = 0; u < array_height_2d(UPGRADES); u++){
	echo("Cost for upgrade:", UPGRADES[u, upginfo_name]);
	for ( var lev = 0; lev <= UPGRADES[u, upginfo_maxlevel]; lev++) {
		var this_cost = upgrade_get_cost(u,lev);
		echo("   Level",lev,":",this_cost," ... Effect:",upgrade_get_effect(u, lev));
		total_cost += this_cost;
	}
}

echo("Cost for all upgrades COMBINED:", total_cost);