globalvar RANDBUFFER_LENGTH; RANDBUFFER_LENGTH = 32000;
globalvar MAX_RAND_NUMBER; MAX_RAND_NUMBER = 10000;
globalvar BASE_SEED, XMOD_RAND, YMOD_RAND, RANDOM_Y_SEED, RANDOM_X_SEED, RANDOM_SEED;
globalvar RANDOM_X, RANDOM_Y, RANDOM;

XMOD_RAND = choose(3,5,7,11,13,17,23,31,37,41,43)
do { YMOD_RAND = choose(3,5,7,11,13,17,23,31,37,41,43)
} until YMOD_RAND  != XMOD_RAND;

BASE_SEED = (date_get_minute(date_current_datetime()) mod 73);
RANDOM_X_SEED = irandom(MAX_RAND_NUMBER);
RANDOM_Y_SEED = irandom(MAX_RAND_NUMBER);
RANDOM_SEED   = irandom(MAX_RAND_NUMBER);
urand_generate_random_buffers();
