/// @desc Make a list, add stuff to that list, and return it.
var a_new_list = ds_list_create();
for( var i=0; i<argument_count; i++){
	ds_list_add(a_new_list,argument[i]);
}
return a_new_list ;