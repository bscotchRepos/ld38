/// @arg upgrade_id
/// @arg [level]

var upg = argument[0];
var lev = UPGRADES[upg, upginfo_level];

if argument_count > 1 { lev = argument[1]; }

return round( UPGRADES[upg, upginfo_cost_mod]* UPGRADE_BASE_COST * power(1+UPGRADE_COST_GROWTH, lev) );

