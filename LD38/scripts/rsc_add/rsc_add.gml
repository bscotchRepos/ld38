/// @arg name
/// @arg max_value
/// @arg regen_per_second
/// @arg display_boolean
/// @arg display_color
/// @arg start_amt
/// @arg max_cooldown

var channel = array_height_2d(resources);
resources[channel, rscinfo_name] = argument0;
resources[channel, rscinfo_max] = argument1;
resources[channel, rscinfo_regen] = argument2;
resources[channel, rscinfo_display_type] = argument3; // 0 or 1 for now, for display / hide
resources[channel, rscinfo_display_color] = argument4;
resources[channel, rscinfo_current] = argument5;
resources[channel, rscinfo_maxcool] = argument6; // Can trigger a cooldown for the resource to start regenerating after it is spent.
resources[channel, rscinfo_cooldown] = 0;
return channel;
