ds_list_clear(anim_draw_order);
ds_list_clear(anim_position_order);

//======== ANIMATION DRAW ORDER
var p = ds_priority_create();

for ( var i = 0; i < array_height_2d(anim_bones); i++) {
    ds_priority_add(p, i, anim_bones[i, anim_depth]);
}

while !ds_priority_empty(p) {
    var highest_depth = ds_priority_delete_max(p);
    ds_list_add(anim_draw_order, highest_depth);
}

//======== POSITION PROCESSING ORDER (based on number of parents)
ds_priority_clear(p);

for ( var i = 0; i < array_height_2d(anim_bones); i++) {
    var num_parents = 0;
    var next_parent = anim_bones[i, anim_parent];
    while next_parent != -1 {
        num_parents++;
        next_parent = anim_bones[next_parent, anim_parent];
    }
    ds_priority_add(p, i, num_parents);
}

while !ds_priority_empty(p) {
    var lowest_parents = ds_priority_delete_min(p);
    ds_list_add(anim_position_order, lowest_parents);
}

ds_priority_destroy(p);

