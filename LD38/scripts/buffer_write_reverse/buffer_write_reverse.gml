/// buffer_write_reverse(buffer,type,value)
var bytes = buffer_sizeof(argument[1]);
var temp_buff = buffer_create(bytes,buffer_fixed,1);
buffer_write(temp_buff,argument[1],argument[2]);
for(var b=0;b<bytes;b++){
    buffer_seek(temp_buff,buffer_seek_start,bytes-b-1)
    buffer_write(argument[0],buffer_u8,buffer_read(temp_buff,buffer_u8));
}
buffer_delete(temp_buff);
