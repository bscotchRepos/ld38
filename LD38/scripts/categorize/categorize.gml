/// @arg id_number
/// @arg category_list
/// @arg contents_map
/// @arg [category_string]
/// @arg [subcategory_string]
/// @arg [subcategory_string]
/// @arg [...]

var iid = argument[0];
var cat_list = argument[1];
var contents_map = argument[2];

var cur_location = "";

for ( var i = 3; i < argument_count; i++){	
	cur_location += argument[i];
	ds_list_add_nodupe(cat_list, cur_location);
	
	if !ds_map_exists(contents_map, cur_location) {
		ds_map_add_list(contents_map, cur_location, ds_list_create());
	}
	
	if i != argument_count-1 {
		cur_location += "/"
	}
}

var this_list = contents_map[?cur_location];
ds_list_add_nodupe(this_list, iid);
