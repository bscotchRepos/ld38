/// @arg xdraw
/// @arg ydraw
/// @arg width
/// @arg height
/// @arg color
/// @arg percent
/// @arg margin

var xd          = argument0;
var yd          = argument1;
var barwidth    = argument2;
var barheight   = argument3;
var barcolor    = argument4;
var percent     = argument5;
var margin      = argument6;

var ht = barheight;
var dk = color_change(barcolor,.5);
draw_rectangle_colour(xd-barwidth*.5-margin, yd-ht*.5-margin, xd+barwidth*.5+margin, yd+ht*.5+margin, c_black, c_black, c_black, c_black, 0);
draw_rectangle_colour(xd-barwidth*.5, yd-ht*.5, xd-barwidth*.5+barwidth*percent, yd+ht*.5, dk, barcolor, barcolor, dk, 0);
