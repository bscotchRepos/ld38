/// @arg sound_id
/// @arg priority
/// @arg looping

return play_sound_pitched(argument0,argument1,argument2,random_range(.8,1.2))
