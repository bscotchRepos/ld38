/// file_rename_if_exists(old_name,new_name)
var old_name=argument[0];
var new_name=argument[1];
if(file_exists(old_name)){
    if(file_exists(new_name)){ file_delete(new_name); }
    file_rename(old_name,new_name);
    return true;
}
else{ return false }
