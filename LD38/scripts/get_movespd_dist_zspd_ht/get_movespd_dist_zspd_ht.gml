/// get_movespd_dist_zspd_ht(distance,start_z,end_z,zspeed)

var dist = argument0;
var start_z = argument1;
var end_z = argument2;
var zspd = argument3;

// Movespeed is in "pixels per second"
// Gravity is in "pixels per second"
// Zspeed is in "pixels per second"

var sec_to_max_height = zspd/GRAVITY;
var sec_to_return_to_initial = 2*sec_to_max_height;

// Now we have the time to get back to start_z, but need
// the time to go from there to 0.

// distance = (vi + vf)/2 * t
// what's final velocity?
// vf^2 = vi^2 + 2*GRAVITY*dist

var final_zspd = sqrt( power(zspd,2) + 2*GRAVITY*dist);
// time to finish ->    distance/((vi+vf)/2)

var sec_to_finish_falling = dist / ((zspd + final_zspd)/2);
var total_flight_time = sec_to_return_to_initial + sec_to_finish_falling;

// Once we know the flight time, figure out the movespeed based on that.

return dist/max(total_flight_time,.01);

//V = z_final - z_initial + g*(t^2)/2
