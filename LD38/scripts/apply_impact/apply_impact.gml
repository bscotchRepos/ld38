///@desc apply_impact()
///@arg impact_speed
///@arg impact_direction
///@arg impact_timer
impact_speed = argument0;
impact_dir = argument1;
impacted_dur = argument2;
shake = 10;
impacted = true;
impacted_timer = impacted_duration