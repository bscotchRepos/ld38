/// draw_text_to_fit(string,pixelwidth,pixelheight,linespacing,font,valignment,halignment,xdraw,ydraw,minscale)

var txt = argument0;
var pixelwidth = argument1;
var pixelheight = argument2;
var linespacing = argument3;
var font = argument4;
var valignment = argument5;
var halignment = argument6,
var xdraw = argument7,
var ydraw = argument8;
var minscale = argument9;

draw_set_font(font);
draw_set_halign(halignment);
draw_set_valign(valignment);

var tscale = 1;
    
while true && tscale > minscale {
    if (string_height_ext(string_hash_to_newline(txt),linespacing,pixelwidth/tscale) < pixelheight/tscale)
            break;
    else tscale -= .025;
}

draw_text_ext_transformed_colour(xdraw,ydraw,string_hash_to_newline(txt),
    linespacing,
    pixelwidth/tscale,
    tscale,tscale,
    0,c_white,c_white,c_white,c_white,1)
