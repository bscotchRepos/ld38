/// @arg item
/// @arg quantity

var the_item = argument0;
var the_quantity = argument1;

if the_quantity < 0 {
    if get_total_owned(the_item) < abs(the_quantity) {
        // We have tried to remove more items than we own. FAIL!
        return false;
    }
}

var quantity_remaining = the_quantity;

// Do we already have it?
var have_item = false;
var owned_list = ITEMS_OWNED;
var stacks_list = ITEM_STACKS;
for ( var i = 0; i < ds_list_size(owned_list) && quantity_remaining != 0; i++){
    var itemtype = owned_list[|i];    
    if itemtype == the_item {
        have_item = true;
        var cur_quantity = stacks_list[|i];        
        if the_quantity < 0 {
            cur_quantity += the_quantity;        
            quantity_remaining = 0;    
        }
        else {
            cur_quantity += the_quantity;
            quantity_remaining = 0;
        }
        ds_list_replace(stacks_list, i, cur_quantity);
    }
}

if !have_item {
    if the_quantity > 0 {
        ds_list_add(ITEMS_OWNED, the_item);
        ds_list_add(ITEM_STACKS, the_quantity);
        ds_list_add(INVENTORY, ds_list_size(ITEMS_OWNED)-1);
    }
}

modify_total_owned(the_item, the_quantity);

return true;
