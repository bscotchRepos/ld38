/// @arg ability_index
/// @arg [direction=0]

var abl = argument[0];
var ab_dir = 0;
if argument_count > 1 {
	var ab_dir = argument[1];
}

var activate_success = false;

if abl_can_use(abl) {
    var the_resource = abilities[abl, abl_resource_channel];
    
    if ( the_resource != -1 ) {
        var use_cooldown = (abilities[abl, abl_resource_cost] > 0)
        if rsc_modify( the_resource, -abilities[abl, abl_resource_cost], false, use_cooldown ) {            
            activate_success = true;            
        }
    }
    else activate_success = true;
}

if activate_success {
    // ACTIVATE THE ABILITY!
    abilities[abl, abl_state] = 0;
    abilities[abl, abl_direction] = round(ab_dir) mod 360;
	
    if abilities[abl, abl_max_charges] > 0
        abilities[abl, abl_charges]--;
    
    if abilities[abl, abl_charges] <= 0 && abilities[abl, abl_reload_maxcool] > 0 {
        abilities[ abl, abl_cooldown] = abilities[abl, abl_reload_maxcool];
    }
    else {
		if abilities[abl, abl_cooldown] <= 0 { // Only turn on the cooldown if the cooldown isn't already active.
			abilities[ abl, abl_cooldown] = abilities[abl, abl_charge_maxcool];
		}
	}
    
    if abilities[abl, abl_gcd_lockout] > 0 {
        abl_start_global_cooldown(abilities[abl, abl_gcd_lockout]);
    }
}

return activate_success;
