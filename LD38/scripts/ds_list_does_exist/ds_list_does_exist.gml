/// @desc A map is not undefined, and exists
/// @param {real|undefined} map

if(is_undefined(argument[0])){ return false; }
if( typeof(argument[0]) != "number"){ return false;}
if(ds_exists(argument[0],ds_type_list)){ return true;}
return false;
