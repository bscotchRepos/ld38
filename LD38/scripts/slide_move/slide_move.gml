if slide_speed > 0 {
    var slide_sp = slide_speed*SLOMO_SECONDS;
    var slide_ld = lengthdir_perspective(slide_sp,0,slide_dir,GRID_RATIO);
    mv_move_ext(slide_dir,slide_ld[0],false);
    slide_speed += tween(slide_speed,0,.05*SPD);
    if slide_speed < .5 {
        slide_speed = 0;
    }
}
