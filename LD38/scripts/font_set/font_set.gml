/// @arg font
/// @arg halign
/// @arg valign

draw_set_font(argument0)
draw_set_halign(argument1)
draw_set_valign(argument2)
if ds_map_exists(FONT_SPACING_MAP,argument0) {
    FONT_SPACING = FONT_SPACING_MAP[?argument0];
}
