var abl = argument0;

if abilities[abl, abl_state] >= 0 {
    abilities[abl, abl_state] = -1;
    abilities[abl, abl_timer] = 0;
    abilities[abl, abl_trigger] = 0;
}
