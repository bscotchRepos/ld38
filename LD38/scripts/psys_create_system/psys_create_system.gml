/// psys_create()
var ps = part_system_create();
part_system_automatic_update(ps, false);

with o_particle_manager {
    ds_list_add(particle_systems, ps);
}

return ps;
