/// @arg min
/// @arg max
/// @arg seed
/// @arg buffer

var min_x = argument0;
var max_x = argument1;
var seed = argument2;
var random_buffer = argument3;

seed = 2*floor(seed mod buffer_get_size(random_buffer)/2);

buffer_seek(random_buffer,buffer_seek_start,seed);
var rand = buffer_read(random_buffer,buffer_u16);

var ratio = rand/MAX_RAND_NUMBER;
return round(abs(max_x-min_x)*ratio+min(min_x,max_x));
