/// @arg bone_id
/// @arg direction
/// @arg tween_speed
/// @arg relative_bool

var bone_id = argument0;
var newdir = argument1;
var tween_speed = argument2;
var relative = argument3;

anim_bones[bone_id, anim_rotation_relative] = relative;
// PROBLEM: A bone's direction is derived from its parent rotation.
// By setting "relative," we are setting the bone's rotation relative to its parent.
// By setting relative to false, we will point the bone in this direction without regard to the parent.

// Also have to take the rotation offset into account.
// Take the target rotation minus the offset. If target is 0 and offset is 270, -270 is the target.

var dir_targ = newdir-anim_bones[bone_id, anim_rotation_offset];

if tween_speed != 0 {
    anim_bones[bone_id, anim_rotation] += angle_difference(dir_targ,anim_bones[bone_id, anim_rotation])*(tween_speed*SLOMO_SECONDS)//angle_diff(dir_targ,anim_bones[bone_id, anim_rotation])*(tween_speed*SLOMO_SECONDS);
}
else anim_bones[bone_id, anim_rotation] = dir_targ;

