if flash > 0 {
    shader_set(shad_flash);
    flashing = true;
    shader_set_uniform_f(flashpass,flash);
    flash = max(0,flash-5*SECONDS_SINCE_UPDATE);    
}
else flashing = false;
