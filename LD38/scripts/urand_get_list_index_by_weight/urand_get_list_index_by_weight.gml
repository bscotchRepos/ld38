/// @arg list_of_weights
/// @arg seed
/// @arg random_buffer

var wtlist = argument0; // GIVE IT A LIST OF WEIGHTS!
var seed = argument1;
var randbuffer = argument2;

// Returns the index of the item chosen.

var wt = 0;
var totalweights = ds_list_sum(wtlist);
var standard = 10000;
if totalweights == 0
    return 0;
var modifier = standard/totalweights;
var chosen_item = -1;
var roll = urand_get_irange(0,standard,seed,randbuffer);

var i, done = false;
for (i = 0; i < ds_list_size(wtlist) && !done; i++){
    var new_wt = modifier*ds_list_find_value(wtlist,i);
    if roll >= wt {
        chosen_item = i;
    }
    else done = true;
    wt += new_wt;    
}

return chosen_item;



// This will return
