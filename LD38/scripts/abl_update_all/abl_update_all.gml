/// @arg ability_speed_modifier

var ability_speed_mod  = 1;
if argument_count > 0 { ability_speed_mod  = argument[0] };

var timepass = SLOMO_SECONDS * ability_speed_mod;
ability_global_cooldown = max(0, ability_global_cooldown - timepass);

for ( var i = 0; i < ds_list_size(abilities_enabled); i++) {
    var ab = abilities_enabled[|i];
    // Update cooldowns!
    if abilities[ ab, abl_cooldown] >= 0 {
        abilities[ ab, abl_cooldown] = max(0, abilities[ ab, abl_cooldown]-timepass);
        if abilities[ ab, abl_cooldown] <= 0 {
            // THEN IT'S TIME. To replenish!
            if abilities[ ab, abl_charges] <= 0 && abilities[ ab, abl_reload_maxcool] > 0 {
                // REPLENISH THE CHARGES!
                abilities[ ab, abl_charges] = abilities[ ab, abl_max_charges];
            }
            else {
                abilities[ ab, abl_charges] = min(abilities[ ab, abl_max_charges], abilities[ ab, abl_charges]+1);
                if abilities[ ab, abl_charges] < abilities[ ab, abl_max_charges] {
                    abilities[ ab, abl_cooldown] = abilities[ ab, abl_charge_maxcool];
                }
            }
        }
    }
    
    // PROGRESS THE TIMERS!
    var curstate = abilities[ ab, abl_state];
    if  curstate >= 0 {
        abilities[ ab, abl_timer] += timepass;
        
        var state_durations = abilities[ ab, abl_state_durations];
        if ds_list_size( state_durations ) > curstate {            
            var curstate_duration = state_durations[|curstate];
            if curstate_duration >= 0 {
                if abilities[ ab, abl_timer] >= curstate_duration {
                    abl_state_advance(ab);
                }
            }
        }
        else {
            abl_stop(ab);
        }   
        
        // Consume resources?
        if abilities [ab, abl_resource_cost_per_second] > 0 {
            var ab_rsc = abilities[ ab, abl_resource_channel];
            var rsc_amt = -abilities[ab, abl_resource_cost_per_second]*timepass;
            if resources[ ab_rsc, rscinfo_current] < abs(rsc_amt) {
                abl_stop(ab);
            }
            else rsc_modify( ab_rsc, rsc_amt, false, true)
        }             
    }
}
