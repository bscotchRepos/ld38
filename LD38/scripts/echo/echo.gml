/// echo(item1 [,item2 [,..]])
var echo_item;
var echo_string="";
for(echo_item=0;echo_item<argument_count;echo_item++){
    echo_string+=string(argument[echo_item])+" ";
}
show_debug_message(object_get_name(object_index) + " :: " + echo_string)
