/// @arg sound_id
/// @arg looping
/// @arg pitch

var s = aud_play_sound(argument0,argument1);
audio_sound_pitch(s,argument2);
return s;
