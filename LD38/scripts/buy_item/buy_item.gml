/// @arg item_to_buy
/// @arg num_to_buy
/// @arg item_to_spend
/// @arg num_to_spend

var item_to_buy = argument[0];
var num_to_buy = argument[1];
var item_to_spend = argument[2];
var num_to_spend = argument[3];

if modify_inventory_item_quantity(item_to_spend, -num_to_spend) {
    modify_inventory_item_quantity(item_to_buy, num_to_buy);
    return true;
}
else return false;
