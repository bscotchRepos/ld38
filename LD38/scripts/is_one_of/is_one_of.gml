/// @desc See if first arg is equal to one of the other args. (Circumvents ass-pain of ds_lists.)
/// @param needle
/// @param haystack1
/// @param [haystack2,...]

var needle = argument[0];
for( var i=1; i<argument_count; i++){
	if( is_equal(needle,argument[i]) ){ return true;}
}

return false;