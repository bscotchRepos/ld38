/// s32_to_grid_coord_s16(s32_int)
//  converts a s32 into x,y coords
buffer_seek (BUFFER_4BYTE,buffer_seek_start,0);
buffer_write(BUFFER_4BYTE,buffer_s32,argument0);
buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
var xy = 0;
xy[1]   = buffer_read(BUFFER_4BYTE,buffer_s16);
xy[0]   = buffer_read(BUFFER_4BYTE,buffer_s16);
return xy;
