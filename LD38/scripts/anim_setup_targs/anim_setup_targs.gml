anim_targs = 0;
for ( var i = 0; i < array_height_2d(anim_bones); i++) {
	anim_targs[i, anim_xoffset_targ] = anim_bones[i, anim_xoffset];
	anim_targs[i, anim_yoffset_targ] = anim_bones[i, anim_yoffset];
	anim_targs[i, anim_rotation_targ] = anim_bones[i, anim_rotation];
	anim_targs[i, anim_xoffset_speed] = 15;
	anim_targs[i, anim_yoffset_speed] = 15;
	anim_targs[i, anim_rotation_speed] = 15;
}