/// clear_buffer_list(list_of_buffers)
//  assumes all list components are buffers, deletes them

var bli;
for(bli=0;bli<ds_list_size(argument0);bli++){
    buffer_delete(argument0[|bli]);
}
ds_list_clear(argument0);
return true;
