/// @arg x
/// @arg y
/// @arg text
/// @arg color
/// @arg size
/// @arg time
/// @arg [fly_distance=150]

var xx		= argument[0],
	yy		= argument[1],
	txt		= argument[2],
	col		= argument[3],
	size	= argument[4],
	timer	= argument[5]; 

var flydist = 100*random_multiplier(.25)
if argument_count > 6 { flydist = argument[6]*random_multiplier(.25); }

if instance_number(o_flying_text) < 10{
    with instance_create(xx,yy,o_flying_text){
         mytext = txt;
         color = col;
         scalemod = size;
         if timer{
            vis = timer;
         }
		 
		 var flydir = point_direction(view_center_x(),view_center_y(),x,y)+random_range(-20,20);		 		 
		 xtarg = x+lengthdir_x(flydist,flydir);
		 ytarg = y+lengthdir_y(flydist,flydir);
    } 
}
