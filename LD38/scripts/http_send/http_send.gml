/// @desc Send an HTTP request. Use http_request_(poll|done) on the resulting object
/// @param {string} method
/// @param {string} URL
/// @param {string|ds_map} [data]
/// @param {ds_map} [headers] // For authentication, sending binary, and other specific applications will need to supply extra headers values
/// @param {string} [preferred="string"]
/// @param {bool} [destroy_headers=true]
// Be careful when sending data as ds_ types! These are not copied, so if you destroy them you might change the request.

var http_method = argument[0];
var http_url = argument[1];
var http_data = "";
var header_vals = undefined;
var http_preferred_response_type = "string";// || "map"
var destroy_headers = true;
if(argument_count>2){ http_data = argument[2]; }
if(argument_count>3){ header_vals = argument[3]; }
if(argument_count>4){ http_preferred_response_type = argument[4]; }
if(argument_count>5){ destroy_headers = argument[5]; }

// Make sure everything is done properly.
if( not is_one_of(http_method,"GET","POST","PUT","PATCH","DELETE")){
	show_error("Invalid HTTP method provided.",true);
}

// Send a copy of the data, instead of the data itself!

var created_id = instance_create(-99999,-99999,o_http);
created_id.method = http_method;
created_id.url = http_url;
created_id.data = http_data;
created_id.preferred_response_type = http_preferred_response_type;
if(ds_map_does_exist(header_vals)){
	ds_map_update(created_id.headers,header_vals);
	if(destroy_headers){
		ds_map_destroy(header_vals);
	}
}

return created_id ;
