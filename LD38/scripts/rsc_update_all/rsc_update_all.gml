for ( var i = 0; i < array_height_2d(resources); i++){
    if resources[i, rscinfo_cooldown] > 0 {
        resources[i, rscinfo_cooldown] = max(0, resources[i, rscinfo_cooldown]-SLOMO_SECONDS);
    }
    if resources[i, rscinfo_cooldown] <= 0
        resources[i, rscinfo_current] = clamp(resources[i, rscinfo_current]+resources[i, rscinfo_regen]*SLOMO_SECONDS,0,resources[i, rscinfo_max]);
}
