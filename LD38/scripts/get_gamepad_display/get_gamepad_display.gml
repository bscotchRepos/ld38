/// @arg the_button
var the_key = argument0;

if ds_map_exists(GAMEPAD_KEY_DISPLAYS, the_key) {
    return GAMEPAD_KEY_DISPLAYS[?the_key];
}
else return "--";
