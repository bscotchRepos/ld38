/// psys_assign_instance(part_system, instance)

/*
If an instance is assigned to a particle system, that particle system
will be destroyed when its instance stops existing. However,
if the particle system is already being destroyed by a timer, it will
ignore the instance assignment.
*/

var ps = argument0;
var inst = argument1;

with o_particle_manager {
    ds_map_replace(particle_instances, ps, inst);
}
