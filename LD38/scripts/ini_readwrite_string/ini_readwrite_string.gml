/// @arg section
/// @arg key
/// @arg string
/// @arg default
/// @arg write_boolean

// This script can be used to save OR load to an INI file.

var section = argument0;
var key = argument1;
var str = argument2;
var def = argument3;
var write = argument4;

if write {
    ini_write_string(section,key,str);
    return str;
}
else {
    return ini_read_string(section,key,def)    
}
