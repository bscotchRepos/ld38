/// @arg z_start
/// @arg z_end
/// @arg sec_to_dest
/// @arg dest_x
/// @arg dest_y

var zstart        = argument0;
var zend          = argument1;
var time_to_dest  = argument2;
var destination_x = argument3;
var destination_y = argument4;

z = zstart;
airdir = point_direction(x,y,destination_x,destination_y);
var pdist = point_distance(x,y,destination_x,destination_y);
airspeed = pdist/time_to_dest;
zspeed = get_zspeed_dist_movespd_ht(pdist, airspeed, z, zend);
