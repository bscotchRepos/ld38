/// @desc Cannot compare things to undefined, this first returns false if the value is the wrong type or does not exist
/// @param map
/// @param key
/// @param comparison_value
var map = argument[0];
var key = argument[1];
var val = argument[2];
if(not ds_map_does_exist(map)){ 
	return false;
}
if( is_undefined(map[?key]) ){ 
	return false; 
}
return is_equal(map[?key],val);