/// base64_to_binary_file( base64_string, destination_file )
//  For some reason buffer_base64_decode() does not necessarily end in the exactly-correct length!
var blen = string_length(argument0)
var padding = 0
if(string_char_at(argument0,blen)=="="){
    padding += 1
    if(string_char_at(argument0,blen-1)=="="){ padding += 1}
}
var total_bytes = 3 * blen/4 - padding
var temp_buffer = buffer_create(total_bytes,buffer_fixed,1)
buffer_base64_decode_ext(temp_buffer,argument0,0)
buffer_save(temp_buffer,argument1)
buffer_delete(temp_buffer)
