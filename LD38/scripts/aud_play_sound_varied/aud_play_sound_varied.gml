/// @arg sound_id
/// @arg looping

return aud_play_sound_pitched(argument0,argument1,random_range(.8,1.2))
