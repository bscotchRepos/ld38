/// @arg value
/// @arg round_amt
var value = argument0;
var round_amt = argument1;

return round_amt*(round(value/round_amt));
