/// buffer_read_reverse(buffer,type)
var bytes = buffer_sizeof(argument[1]);
var temp_buff = buffer_create(bytes,buffer_fixed,1);
for(var b=0;b<bytes;b++){
    buffer_seek(temp_buff,buffer_seek_start,bytes-b-1)
    buffer_write(temp_buff,buffer_u8,buffer_read(argument[0],buffer_u8));
}
buffer_seek(temp_buff,buffer_seek_start,0)
var output = buffer_read(temp_buff,argument[1]);
buffer_delete(temp_buff);
return output;
