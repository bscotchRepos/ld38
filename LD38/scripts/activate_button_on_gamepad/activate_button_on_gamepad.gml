/// activate_button_on_gamepad(gamepad_button [, force = false])

var keycheck = get_gpconfig_button(argument[0]);
var force = false; // Always check this, regardless of confirmation windows
if argument_count > 1 {
    force = argument[1];
}

if CURRENT_GAMEPAD != -1 && keycheck != -1 {
    if gamepad_button_check_pressed(CURRENT_GAMEPAD,keycheck){
        if (!instance_exists(o_bsid_confirmation_window) || force) && active {
            alarm[0] = .1*room_speed;
            play_sound_gv(snd_menuclick,10,false);
            scale = .85;
        }
    }
}
