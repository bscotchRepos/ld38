/// @desc True if type and value exactly match, else false.
/// @param value1
/// @param value2

var val1 = argument[0];
var val2 = argument[1];
if( typeof(val1) != typeof(val2) ){ 
	echo("is_equal: value types are not the same");
	return false;
}
return val1 == val2 ;
