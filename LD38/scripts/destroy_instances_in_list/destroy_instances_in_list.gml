/// @arg list_id
/// @arg [destroy_list_bool]

var list_id = argument[0];
for (var i = 0; i < ds_list_size(list_id); i++){
	with list_id[|i] { instance_destroy(); }
}
ds_list_clear(list_id);

if argument_count > 1 {
	if argument[1] {
		ds_list_destroy(list_id);
	}
}