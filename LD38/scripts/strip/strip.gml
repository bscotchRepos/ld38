///strip(string [,chars])
// Removes the characters from the start and end of STRING (in any order)
// Defaults to whitespace if no argument given
// (behaves like Python's str.strip())

var input_qrm = argument[0] ;
var chars_qrm = chr(9)+chr(10)+" "+chr(13) ; // whitespace characters
if( argument_count == 2 )
{
    chars_qrm = argument[1] ;
}

// Strip start of string
var i_qrm ;
var dir_qrm ;
for( dir_qrm=0; dir_qrm<2 ; dir_qrm++)
{
    if(dir_qrm==1){
        input_qrm = string_reverse(input_qrm) ;
    }
    var strlen_qrm = string_length(input_qrm);
    for( i_qrm=strlen_qrm ; i_qrm >0 ; i_qrm-- )
    {
        if( string_pos( string_char_at(input_qrm,i_qrm), chars_qrm)>0)
        {
            input_qrm = string_copy(input_qrm,1,string_length(input_qrm)-1)
        }
        else
        {
            break ;
        }
    }
}

return string_reverse(input_qrm) ;
