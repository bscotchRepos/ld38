/// @arg name
/// @arg rsc_channel
/// @arg rsc_cost
/// @arg rsc_cost_per_sec
/// @arg max_charges
/// @arg start_charges
/// @arg gcd_lockout_time
/// @arg ignore_gcd
/// @arg charge_regen_maxcool
/// @arg reload_maxcool
/// @arg stop_other_abilities
/// @arg movement_mod
/// @arg cast_effect_object
/// @arg [state_duration0...]

var this_ab = array_height_2d(abilities);

abilities[ this_ab, abl_name ] = argument[0];
abilities[ this_ab, abl_resource_channel ] = argument[1];
abilities[ this_ab, abl_resource_cost ] = argument[2];
abilities[ this_ab, abl_resource_cost_per_second ] = argument[3]; // While this ability is active, continually consume resource.
abilities[ this_ab, abl_max_charges ] = argument[4];
abilities[ this_ab, abl_start_charges ] = argument[5] ;
abilities[ this_ab, abl_charges ] = argument[5];
abilities[ this_ab, abl_gcd_lockout ] = argument[6];
abilities[ this_ab, abl_ignore_gcd ] = argument[7];
abilities[ this_ab, abl_charge_maxcool ] = argument[8];
abilities[ this_ab, abl_cooldown ] = 0;
abilities[ this_ab, abl_reload_maxcool ] = argument[9];
abilities[ this_ab, abl_state ] = -1; // -1 means inactive.
abilities[ this_ab, abl_state_durations] = ds_list_create();
abilities[ this_ab, abl_timer ] = 0;
abilities[ this_ab, abl_stop_other_abilities ] = argument[10];
abilities[ this_ab, abl_trigger ] = 0;
abilities[ this_ab, abl_enabled ] = false;
abilities[ this_ab, abl_movement_mod ] = argument[11];
abilities[ this_ab, abl_cast_effect ] = argument[12]; // Special effect object to spawn while ability is active.
abilities[ this_ab, abl_x ] = x;
abilities[ this_ab, abl_y ] = y;
abilities[ this_ab, abl_direction ] = 0;

for ( var i = 13; i < argument_count; i++) {
    ds_list_add( abilities[ this_ab, abl_state_durations], argument[i] );
}
// Note: State duration of <0 means the timer will not progress during that state. The state will have to be advanced manually (like with a hotkey or something)

abilities[ this_ab, abl_total_state_time ] = ds_list_sum( abilities[ this_ab, abl_state_durations] );

abl_enable(this_ab);

return this_ab;
// If an ability has no state durations, it will have to be cancelled manually by other events.
