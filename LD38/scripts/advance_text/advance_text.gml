/// advance_text(current_text,target_text, letters_per_step)

var curtext = argument0;
var targtext = argument1;
var letters_per_step = argument2;

var curlength = string_length(curtext);
var targlength = string_length(targtext);

if curlength == targlength
    return targtext;
else {
    var newtext = string_copy(targtext,1,min(curlength+(letters_per_step),targlength));
    return newtext;
}
