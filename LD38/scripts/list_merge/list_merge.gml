///list_merge(target_list,to_add)
if(ds_exists(argument0,ds_type_list))
{
    if(ds_exists(argument1,ds_type_list))
    {
        if(ds_list_size(argument1)>0)
        {
            var i_bs0;
            for(i_bs0=0;i_bs0<ds_list_size(argument1);i_bs0++)
            {
                ds_list_add(argument0,argument1[|i_bs0]);
            }
        }
    }
}
