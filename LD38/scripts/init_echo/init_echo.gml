///init_echo()
globalvar ECHO_FILE; ECHO_FILE = "echo.txt";
globalvar ECHO_ENABLED; ECHO_ENABLED = true; //Kill switch for all echo_ext()
if file_exists(ECHO_FILE)
    file_delete(ECHO_FILE);
var file = file_text_open_write(ECHO_FILE);
file_text_close(file);