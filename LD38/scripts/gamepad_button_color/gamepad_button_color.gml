/// gamepad_button_color(button)
var bt = get_gpconfig_button(argument0);

switch bt {
    case gp_face1:
        if GAMEPAD_CONFIG == 0 // Green A
            return make_colour_rgb(102,255,0);
        else { // Pink square 
            return make_colour_rgb(248,110,255);
        }
        break;
    case gp_face2:
        if GAMEPAD_CONFIG == 0 // Red B
            return make_colour_rgb(254,31,55);
        else { // Blue X
            return make_colour_rgb(1,184,255);
        }
        break;
    case gp_face3:
        if GAMEPAD_CONFIG == 0 // Blue X
            return make_colour_rgb(81,167,255);
        else { // Orange Circle
            return make_colour_rgb(252,115,80);
        }
        break;
    case gp_face4:
        if GAMEPAD_CONFIG == 0 // Yellow Y
            return make_colour_rgb(255,168,11);
        else { // Teal Triangle
            return make_colour_rgb(1,230,126);
        }
        break;
    default:
        return c_white;
        break;
}
