///file_copy_utf8(source,target)

var source = argument[0];
var target = argument[1];

if (!file_exists(source))
{
    return false;
}

var sourceHandle = file_bin_open(source, 0)
var targetHandle = file_bin_open(target, 1);

var sourceBytes = file_bin_size(sourceHandle);

for (var i = 0; i < sourceBytes; i++)
{
  file_bin_write_byte(targetHandle, file_bin_read_byte(sourceHandle));
}

var failed = true;

if (file_bin_position(targetHandle) >= sourceBytes)
{
  failed = false;
}

file_bin_close(sourceHandle);
file_bin_close(targetHandle);

return failed;
