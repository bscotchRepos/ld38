/// @desc A map is not undefined, and exists
/// @param {real|undefined} map

if(is_undefined(argument[0])){ 
	return false; 
}
if( !is_real(argument[0]) ){ 
	return false;
}
if(ds_exists(argument[0],ds_type_map)){ 
	return true;
}
return false;
