/// abl_is_active(ability_index)

return abilities[argument0, abl_state] >= 0 && abilities[argument0, abl_enabled]
