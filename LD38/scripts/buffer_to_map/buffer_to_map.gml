/// buffer_to_map( map, buffer, key_datatype, value_datatype [, prefix] )
//  Replaces map contents with buffer contents.
//  Will take the buffer reference and assume that the first 4 bytes
//  are a u32 describing the number of ELEMENTS (not bytes) to read.
//  If prefix is not undefined, the second 4 bytes are interpreted
//  as the length and the first 2 are a u16 that should be equal to
//  prefix, else the input map will be unaffected (soft failure).
//  If map is undefined, will return a new map

var map = argument[0];
var buffer = argument[1];
var key_type = argument[2];
var value_type = argument[3];

var prefix = undefined;
if( argument_count>4 ){ prefix = argument[4]; }

if(not is_undefined(prefix)) {
    var actual_prefix = buffer_read(buffer,buffer_u16);
    if(actual_prefix != prefix) {
        echo("Buffer prefix did not match expected. Expected:",
             prefix,"Actual:",actual_prefix);
        return false;
    }
}

if(!ds_exists(map,ds_type_map)){map=ds_map_create();}
else{ds_map_clear(map);}

var items = buffer_read(buffer,buffer_u32);
var item_idx;
for(item_idx=0 ; item_idx<items ; item_idx++){
    var k = buffer_read(buffer,key_type);
    var v = buffer_read(buffer,value_type);
    ds_map_add(map,k,v);
}

return map;
