/// @arg x
/// @arg y
/// @arg margin

var xx = argument0, yy = argument1, margin = argument2;
var curcam = view_camera[0];

if xx < camera_get_view_x(curcam)-margin
   return false;
if yy < camera_get_view_y(curcam)-margin
   return false;
if xx > view_right()+margin
   return false;
if yy > view_bottom()+margin
   return false;

return true;
