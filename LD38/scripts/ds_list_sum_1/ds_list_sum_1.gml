/// ds_list_sum(list)

var i, sum = 0;
for (i = 0; i < ds_list_size(argument0); i++){
    sum += ds_list_find_value(argument0,i);
}
return sum;
