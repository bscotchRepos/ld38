/// @arg sound
/// @arg priority
/// @arg looping

// Plays sound effect with global volume attached.

var sid = audio_play_sound(argument0,argument1,argument2);
audio_sound_gain(sid,SOUND_SETTING_VOLUME,0);
return sid;