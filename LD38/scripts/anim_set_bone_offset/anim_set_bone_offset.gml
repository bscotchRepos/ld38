/// @arg bone_id
/// @arg offset_property
/// @arg amount
/// @arg tween_speed
/// @arg relative_bool

var bone_id = argument0;
var property = argument1;
var amount = argument2;
var tween_speed = argument3;
var relative = argument4;

anim_bones[bone_id, anim_offset_relative] = relative;

if tween_speed != 0 {
    anim_bones[bone_id, property] += anim_tween(anim_bones[bone_id, property], amount, tween_speed);
}
else anim_bones[bone_id, property] = amount;

