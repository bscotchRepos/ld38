/// scalars_to_buffer(list, [, prefix=undefined [, include_length=false]])
//  creates a buffer that must be externally deleted to prevent memory leaks
//  Buffer data structure is: [prefix u16] + [number of values u32] + [value1 datatype1 ] ...
//  list contains alternating value, type pairs, e.g.:
//       list = ( value1, datatype1, value2, datatype2 ... )
//  datatypes must be one of the buffer constants
//  prefix must fit inside a u16, or be undefined

var list  = argument[0];

var prefix         = undefined ;
var include_length = false ;
var prefix_bytes   = 0 ;
var length_bytes   = 4 ;

if(argument_count > 1){ prefix         = argument[1] ;}
if(argument_count > 2){ include_length = argument[2] ;}

if( not include_length )      { length_bytes = 0 ;}
if( not is_undefined(prefix) ){ prefix_bytes = 2 ;}


var datapoints = ds_list_size(list)/2;
var databytes  = 0 ;
var idx ;
for(idx=0;idx<datapoints;idx++){
    var type = list[|2*idx+1];
    if(type==buffer_string){
        databytes += string_length(list[|2*idx])+1;
    }
    else{ databytes += buffer_sizeof(type);}
}
var output_buffer = buffer_create( databytes + 
                                   prefix_bytes + 
                                   length_bytes, buffer_fixed, 1 );

if(not is_undefined(prefix))
{
    buffer_write(output_buffer,buffer_u16,prefix);
}

if(include_length)
{
    buffer_write(output_buffer,buffer_u32,datapoints);
}

for( idx=0; idx<datapoints; idx++)
{
    buffer_write(output_buffer,list[|2*idx+1],list[|2*idx]);
}

ds_list_destroy(list);

return output_buffer ;
