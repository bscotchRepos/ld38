/// abl_state_advance(ability_index)

if abilities[argument0,abl_state] == -1 {
    // Cannot advance the state because the ability is not currently active.
    return false;
}

abilities[argument0, abl_state]++;
abilities[argument0, abl_timer]=0;
if abilities[argument0,abl_state] >= ds_list_size(abilities[argument0,abl_state_durations]) {
    abilities[argument0, abl_state] = -1;
    abilities[argument0, abl_trigger] = 0;
}
return true;
