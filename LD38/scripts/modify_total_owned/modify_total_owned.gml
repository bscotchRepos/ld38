/// @arg itemtype
/// @arg quantity

var itype = argument0, quant = argument1;

if ds_map_exists(TOTAL_OWNED,itype) {
    var curquant = ds_map_find_value(TOTAL_OWNED,itype);
    curquant += quant;
    ds_map_replace(TOTAL_OWNED,itype,curquant);
}   
else ds_map_add(TOTAL_OWNED,itype,quant);
