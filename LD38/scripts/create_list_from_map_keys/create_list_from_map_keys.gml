/// @arg ds_map

var m = argument0;
var k = ds_map_find_first(m);

var ls = ds_list_create();
for ( var i = 0; i < ds_map_size(m); i++){
	ds_list_add(ls, k);
	k = ds_map_find_next(m,k);
}

return ls;