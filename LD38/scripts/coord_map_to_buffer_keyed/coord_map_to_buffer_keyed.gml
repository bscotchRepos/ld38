/// coord_map_to_buffer(map, datatype [, prefix [, include_length=true]])
//  creates a buffer that must be externally deleted to prevent memory leaks
//  Input map is expected to be keyed by u32 encoding of xy coords
//  Buffer data structure is: [prefix u16] + [number of values u32] + [value1 datatype ] ...
//  datatype must be one of the buffer constants
//  prefix must fit inside a u16, or be undefined

var map  = argument[0];
var type  = argument[1];

var prefix         = undefined ;
var include_length = true ;
var prefix_bytes   = 0 ;
var length_bytes   = 4 ;

if(argument_count > 2){ prefix         = argument[2] ;}
if(argument_count > 3){ include_length = argument[3] ;}

if( not include_length )      { length_bytes = 0 ;}
if( not is_undefined(prefix) ){ prefix_bytes = 2 ;}


var datapoints = ds_map_size(map);
var typesize = buffer_sizeof(type) ;
var remapped = ds_map_create();
var typekeys = ds_list_create();
var idx;
var coordkey=ds_map_find_first(map);
var the_types = 0 ;
for(var idx=0; idx<datapoints; idx++)
{
    if(is_undefined(remapped[?map[?coordkey]])){
        remapped[?map[?coordkey]] = ds_list_create();
        the_types += 1;
        ds_list_add(typekeys,map[?coordkey]);
    }
    ds_list_add(remapped[?map[?coordkey]],coordkey)
    coordkey=ds_map_find_next(map,coordkey);
}

// since types are used as keys, they need to also have a u32 size
var output_buffer = buffer_create( the_types*(typesize+4)+datapoints*4 + 
                                   prefix_bytes + 
                                   length_bytes, buffer_fixed, 1 );

if(not is_undefined(prefix))
{
    buffer_write(output_buffer,buffer_u16,prefix);
}

if(include_length)
{
    buffer_write(output_buffer,buffer_u32,the_types);
}

for(var idx=0; idx<ds_list_size(typekeys); idx++)
{
    
    buffer_write(output_buffer,type,typekeys[|idx]);
    var total_coords = ds_list_size(remapped[?typekeys[|idx]]);
    buffer_write(output_buffer,buffer_u32,total_coords);
    for(var j=0; j<total_coords; j++){
        var coordkey_list = remapped[?typekeys[|idx]];
        buffer_write(output_buffer,buffer_s32,coordkey_list[|j]);
    }
    ds_list_destroy(remapped[?typekeys[|idx]]);
}
ds_list_destroy(typekeys);
ds_map_destroy(remapped);

return output_buffer ;
