/// @arg key_2d_array
/// @arg invalid_chars_string
/// @arg height
/// @arg width
/// @arg key_sound
/// @arg font
/// @arg textcolor
/// @arg dropshadow
/// @arg text_flash
/// @arg key_sprite
/// @arg bg_color
/// @arg bg_alpha
/// @arg bg_flashcolor
/// @arg border_color
/// @arg max_characters
/// @arg only_allow_keys

// Escape character: ~
// if you want a tilde, put ~~.

// Backspace: ~b
// Shift: ~s
// Enter: ~r
// Caps: ~c
// Tab: ~t
// Function: ~f

with o_generic_keyboard instance_destroy();

with instance_create(0, 0, o_generic_keyboard) {
    
    if argument_count > 0 {
        keys = argument[0];        
    }
    
    if argument_count > 1   { 
        if argument[1] != "" {
            for ( var s = 1; s <= string_length(argument[1]); s++){
                ds_list_add(invalid_chars, string_char_at(argument[1],s));
            }
        }
    }
    if argument_count > 2   { height = argument[2]; }
    if argument_count > 3   { width = argument[3]; }        
    if argument_count > 4   { sound = argument[4]; }
    if argument_count > 5   { font = argument[5]; }
    if argument_count > 6   { textcolor = argument[6]; }
    if argument_count > 7   { dropshadow = argument[7]; }        
    if argument_count > 8   { text_flashcolor = argument[8]; }
    if argument_count > 9   { key_sprite = argument[9]; }
    if argument_count > 10  { bg_color = argument[10]; }
    if argument_count > 11  { bg_alpha = argument[11]; }
    if argument_count > 12  { bg_flashcolor = argument[12]; } 
    if argument_count > 13  { bordercolor = argument[13]; }
    if argument_count > 14  { max_length  = argument[14]; }
    if argument_count > 15  { only_allow_current_keys = argument[15]; }
    event_user(0);
    return id;
}
