/// @arg dmg_amt

var dmg_amt = round(argument0*random_multiplier(.3));
with o_player {
	if dmg_cooldown <= 0 {
		flash = 1;
		shake = 5;
		play_sound_gv(snd_thud_wet, 10, false);
		hp = max(0, hp-dmg_amt);
		dmg_cooldown = .25;
		create_flying_text(x,y,string(dmg_amt),c_red,.5,.5,150);
	}
}