/// @arg upgrade
/// @arg [level]

var upg = argument[0];
var lev = UPGRADES[upg, upginfo_level];
if argument_count > 1 {
	lev = argument[1];
}

var levelboost = floor(lev/(UPGRADES[upg, upginfo_maxlevel]/BOOST_LEVELS))
lev += levelboost;

var base = UPGRADES[upg, upginfo_baseline];
var amt = 0;
if UPGRADES[upg, upginfo_change_linear] {
	amt = base + + UPGRADES[upg, upginfo_change]*(lev);
}
else {
	amt = base * power(1+UPGRADES[upg, upginfo_change], lev);
}

if UPGRADES[upg, upginfo_rounded] { amt = round(amt); }
return amt;