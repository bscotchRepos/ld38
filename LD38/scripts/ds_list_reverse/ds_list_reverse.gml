/// ds_list_reverse(list)
var newlist = ds_list_create();
var curlist = argument0;
for (var i = ds_list_size(curlist)-1; i >= 0; i--){
    ds_list_add(newlist,curlist[|i]);
}
ds_list_copy(curlist,newlist);
ds_list_destroy(newlist);
