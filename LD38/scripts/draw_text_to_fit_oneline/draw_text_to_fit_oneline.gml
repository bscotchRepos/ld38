/// @arg string
/// @arg pixelwidth
/// @arg pixelheight
/// @arg xdraw
/// @arg ydraw
/// @arg minscale
/// @arg maxscale
/// @arg color
/// @arg drop_pixels
/// @arg [alpha]

var txt = argument[0];
var pixelwidth = argument[1];
var pixelheight = argument[2];
var xdraw = argument[3],
var ydraw = argument[4];
var minscale = argument[5];
var maxscale = argument[6];
var color = argument[7];
var drop_pixels = argument[8];
var alpha = 1;

if argument_count > 9 {
    alpha = argument[9];
}
var tscale = 1;
var theight = max(1,string_height(string_hash_to_newline(txt)));
var twidth = max(1,string_width(string_hash_to_newline(txt)));

tscale = clamp( min(pixelwidth/twidth, pixelheight/theight), minscale, maxscale);

if drop_pixels > 0 {
    shadowtext(xdraw,ydraw,txt,tscale,tscale,0,color,alpha,drop_pixels);
}
else draw_text_transformed_colour(xdraw,ydraw,string_hash_to_newline(txt),tscale,tscale,0,color,color,color,color,alpha)
return tscale;
