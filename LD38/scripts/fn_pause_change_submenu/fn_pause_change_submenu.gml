/// fn_pause_change_submenu(new_submenu)

var new_submenu = argument0;

if current_menu != new_submenu {
    fn_pause_destroy_all_buttons();
    var new_buttonlist = ds_map_find_value(submenus, new_submenu);
    if !is_undefined(new_buttonlist) {
        for ( var i = 0; i < ds_list_size(new_buttonlist); i++) {
            ds_list_add(spawned_buttons, instance_create_layer(-1000, -1000, button_layer, new_buttonlist[|i]));			
        }
    }
    current_menu = new_submenu;
}
