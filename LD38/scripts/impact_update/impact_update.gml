///@desc impact_update()

if impacted{
	if impacted_dur > 0{
		impacted_dur -= SLOMO_SECONDS; 
		impact_speed = lerp(impact_speed, 0, .1);
		x = lerp(x, x + lengthdir_x(impact_speed, impact_dir),.8);
		y = lerp(y, y + lengthdir_y(impact_speed, impact_dir),.8);
	}
	impacted_timer -= SLOMO_SECONDS;
	if impacted_timer <= 0{
		impacted = false;
	}	
}

