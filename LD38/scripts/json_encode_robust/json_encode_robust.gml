/// @desc Return empty map encoding for falsey values, else an actually encoded map.
var encoded = "{}";
if(typeof(argument[0]) != "number"){
	return encoded;
}
if(ds_map_does_exist(argument[0])){
	return json_encode(argument[0]);
}
return encoded;
