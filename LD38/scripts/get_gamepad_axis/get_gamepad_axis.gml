/// get_gamepad_axis(gamepad_index, axis_h, axis_v, include_dpad)

// Returns an array with the horizontal & vertical axis values inside it.

var gp = argument0;
var axis_h = argument1;
var axis_v = argument2;
var include_dpad = argument3;

var xy = 0;

xy[0] = 0;
xy[1] = 0;

if axis_h != -1
    xy[0] = gamepad_axis_value(gp, axis_h);
if axis_v != -1
    xy[1] = gamepad_axis_value(gp, axis_v);

if include_dpad {
    if gamepad_button_check(gp,gp_padd) { xy[1] = 1; }
    if gamepad_button_check(gp,gp_padu) { xy[1] = -1; }
    if gamepad_button_check(gp,gp_padr) { xy[0] = 1; }
    if gamepad_button_check(gp,gp_padl) { xy[0] = -1; }
}

return xy;
