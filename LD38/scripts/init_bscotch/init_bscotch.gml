// Initialize everything BSCOTCHY!

/// Some general variables that all games will need.
globalvar REZ; REZ = 1;
globalvar SECONDS_SINCE_UPDATE; SECONDS_SINCE_UPDATE = 1;
globalvar SLOMO_SECONDS; SLOMO_SECONDS = 1;
globalvar SPD; SPD = 1;
globalvar GRAVITY_PER_FRAME; GRAVITY_PER_FRAME = 1;
globalvar GRAVITY; GRAVITY = 1800;
globalvar FONT_SPACING; FONT_SPACING = 10;
globalvar FONT_SPACING_MAP; FONT_SPACING_MAP = ds_map_create();
globalvar SOUND_SETTING_VOLUME; SOUND_SETTING_VOLUME = 1;
globalvar MUSIC_SETTING_VOLUME; MUSIC_SETTING_VOLUME = 1;
globalvar FULLSCREEN; FULLSCREEN = 0;
globalvar MUSICID; MUSICID = -1;
globalvar GENERAL_OPTIONS_FILE; GENERAL_OPTIONS_FILE = "options.ini";
globalvar ALWAYS_ON_TOP; ALWAYS_ON_TOP = ds_list_create();
globalvar SOUND_GAINS;		SOUND_GAINS = ds_map_create();
globalvar SOUND_PRIORITIES; SOUND_PRIORITIES = ds_map_create();
var temp_map = ds_map_create();
globalvar EMPTY_JSON; EMPTY_JSON = json_encode(temp_map);
ds_map_destroy(temp_map);
// CONTROLS
init_gamepad();
init_key_displays();
