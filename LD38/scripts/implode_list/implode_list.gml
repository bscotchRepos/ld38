///implode_list(separator,list)
var out_z8g = "";
if(ds_exists(argument1,ds_type_list))
{
    var items_z8g = ds_list_size(argument1) ;
    var i_z8g;
    for(i_z8g=0;i_z8g<items_z8g;i_z8g++){
        out_z8g+=argument1[|i_z8g];
        if(i_z8g<items_z8g-1){out_z8g+=argument0;}
    }
}
return out_z8g;
