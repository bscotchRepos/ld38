/// @arg ability
/// @arg [start_state]
/// @arg [end_state]

var abl = argument[0];

if abilities[abl,abl_state] >= 0 {
	var start_state = abilities[abl,abl_state];
	var end_state = abilities[abl,abl_state];
	if (argument_count > 1 ) { start_state = max(0, argument[1]); }
	if (argument_count > 2 ) { end_state = min(argument[2], ds_list_size(abilities[abl,abl_state_durations])-1); }
	if (abilities[abl,abl_state] < start_state ) { return 0; }
	if (abilities[abl,abl_state] > end_state ) { return 0; }
	
	// Get the total time.
	var totaltime = 0;
	var time_passed = 0;
	for ( var i = start_state; i <= end_state; i++){
		var state_maxtime = ds_list_find_value(abilities[abl,abl_state_durations], i);
		if ( abilities[abl,abl_state] > i ) { time_passed += state_maxtime; }
		else if abilities[abl,abl_state] == i { time_passed += abilities[abl, abl_timer] };
		
		totaltime += state_maxtime;
	}
	
    var timer_prop = time_passed/totaltime; //abilities[abl, abl_timer]/ds_list_find_value(abilities[abl,abl_state_durations],abilities[abl,abl_state]);
    //echo("Timer proportion for", abilities[abl, abl_name], "in state", abilities[abl, abl_state],":",timer_prop);
	return timer_prop;
}
else return 0;

