/// grid_coord_s16_to_f32(x,y)
//  converts xy coord to a f32
buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
buffer_write(BUFFER_4BYTE,buffer_s16,argument1);
buffer_write(BUFFER_4BYTE,buffer_s16,argument0);
buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
return buffer_read(BUFFER_4BYTE,buffer_f32);
