/// @arg section
/// @arg key
/// @arg real
/// @arg default
/// @arg write_boolean

// This script can be used to save OR load to an INI file.

var section = argument0;
var key = argument1;
var num = argument2;
var def = argument3;
var write = argument4;

if write {
    ini_write_real(section,key,num);
    return num;
}
else {
    return ini_read_real(section,key,def)    
}
