/// @arg sound_id
/// @arg gain
/// @arg priority

ds_map_add(SOUND_GAINS, argument0, argument1);
ds_map_add(SOUND_PRIORITIES, argument0, argument2);