/// @desc Get a value from a map, allowing a default if map or key do not exist.
/// @param {ds_map} map
/// @param {string} key
/// @param [default_value=undefined]
var map = argument[0];
var key = argument[1];
var default_value = undefined;
if(argument_count>2){ default_value = argument[2]; }

if(ds_map_does_exist(map)){ 
	if(not is_undefined(map[?key])){ 
		return map[?key] ;
	}
}

return default_value;