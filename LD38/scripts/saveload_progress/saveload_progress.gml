/// @arg saving

var saving = argument0;

ini_open("save.ini");

BALLOOMS = ini_readwrite_real("p", "ballooms", BALLOOMS, BALLOOMS, saving);
BEST_DISTANCE = ini_readwrite_real("p", "bs", BEST_DISTANCE, BEST_DISTANCE, saving);
for ( var i = 0; i < array_length_1d(BEAKS); i++) {
	BEAKS[i] = ini_readwrite_real("p", "beaks" + string(i), BEAKS[i], BEAKS[i], saving);
}

for ( var i = 0; i < array_height_2d(UPGRADES); i++){
	UPGRADES[i, upginfo_level] = ini_readwrite_real("u",string(i),UPGRADES[i, upginfo_level],UPGRADES[i, upginfo_level],saving)
}

ini_close();