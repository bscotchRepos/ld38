// Sets the resolution, then goes to the target room.
var resolution_width;
resolution_width = 1920;

window_set_fullscreen(FULLSCREEN)

var dispH = display_get_height();
var dispW = display_get_width();

var aspect_normal		= 0;
var aspect_iphone_3_5	= 1;
var aspect_iphone_4		= 2;
var aspect_iphone_4_7	= 3;
var aspect_iphone_5_5	= 4;
var aspect_ipad_pro		= 5;
var aspect_ipad			= 6;

/*
ASPECTS:
    aspect_normal (normal)
    aspect_iphone_3_5 (3.5 inch)
    aspect_iphone_4 (4 inch)
    aspect_iphone_4_7 (4.7 inch)
    aspect_iphone_5_5 (5.5 inch)
    aspect_ipad (ipad)
    aspect_ipad_pro (ipad pro)
*/

var screenshot_mode = aspect_normal;
var wh_ratio = dispW/dispH;

switch screenshot_mode {
    case aspect_iphone_3_5: // (960x640 -- 1.5x1)
        resolution_width = 1024;
        wh_ratio = 1.5;
        break;
    case aspect_iphone_4: // (1136x640 -- 1.775) 
    case aspect_iphone_4_7: // (1334x750 -- 1.779)
    case aspect_iphone_5_5: // (2208x1242 -- 1.778)
        resolution_width = 1366;
        wh_ratio = 1.777;
        break;
    case aspect_ipad_pro: // (2732x2048 -- 1.333)
    case aspect_ipad: // (2048x1536 -- 1.333)
        resolution_width = 1600;        
        wh_ratio = 1.333;
        break;
}

var view_width = resolution_width;

if !on_mobile() && !FULLSCREEN
    wh_ratio = min(1.77777,wh_ratio);

var view_height = ceil(view_width/wh_ratio);

globalvar GUIWIDTH, GUIHEIGHT;
GUIWIDTH = view_width;
GUIHEIGHT = view_height;
display_set_gui_size(view_width,view_height)

var i;
for (i = 0; i < 500; i += 1){    
    if (room_exists(i)){  
        for ( var v = 0; v < 5; v++){      
            room_set_view(i, v, (v == 0), 0, 0, view_width, view_height, 0, 0, view_width, view_height, -1, -1, -1, -1, noone)                       
        }
    }
}

if FULLSCREEN {
    window_set_size(display_get_width(), display_get_height())
    window_set_position(0, 0)
    surface_resize(application_surface, view_width, view_height);    
}
else {
    var window_width = min(dispW*.9,view_width);
    var window_height = min(dispH*.9,view_height);
    
    window_set_size(window_width, window_height)
    window_set_position((dispW-window_width)/2,(dispH-window_height)/2)
    surface_resize(application_surface, view_width, view_height); 
}
