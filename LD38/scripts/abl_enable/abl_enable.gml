//// abl_enable(ability_id_or_name)

var abl_to_enable = argument0;

if is_string(abl_to_enable) {
    var namecheck = string_lower(abl_to_enable);
    var found = false;
    for ( var i = 0; i < array_height_2d(abilities) && !found; i++) {
        if string_lower(abilities[i, abl_name]) == namecheck {
            found = true;
            abl_to_enable = i;
        }
    }
}

ds_list_add_nodupe(abilities_enabled, abl_to_enable);
abilities[abl_to_enable, abl_enabled] = true;
