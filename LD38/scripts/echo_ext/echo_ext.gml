/// echo_ext(local_debug_switch,item1 [,item2 [,..]])
/// @arg local_debug_switch
/// @arg item1
/// @arg [item2..]
if ECHO_ENABLED{
    var local_debug_switch = argument[0];
    if local_debug_switch == false{
        exit;
    }
    else{
        var echo_item;
        var echo_owner = object_get_name(object_index);      
        var echo_string= echo_owner+" :: ";
        for(echo_item = 1;echo_item<argument_count;echo_item++){
            echo_string+=string(argument[echo_item])+" ";
        }                 
        var file = file_text_open_append(ECHO_FILE);
        file_text_write_string(file,echo_string);
        file_text_writeln(file);
        file_text_close(file);      
        show_debug_message(echo_string);
    }
}
