/// bin_file_to_coord_map_keyed( map, file_handle, datatype [, prefix] )
//  Replaces list contents with buffer contents.
//  Will take the buffer reference and assume that the first 4 bytes
//  are a u32 describing the number of ELEMENTS (not bytes) to read.
//  If prefix is not undefined, the second 4 bytes are interpreted
//  as the length and the first 2 are a u16 that should be equal to
//  prefix, else the input list will be unaffected (soft failure).
//  If list is undefined, will return a new list

var map    = argument[0];
var file_handle = argument[1];
var type   = argument[2];
var prefix = undefined;
if( argument_count>3 ){ prefix = argument[3]; }

if(not is_undefined(prefix)){
    var actual_prefix = file_bin_read(file_handle,buffer_u16);
    if(actual_prefix != prefix){
        echo("Buffer prefix did not match expected. Expected:",
             prefix,"Actual:",actual_prefix);
        return false;
    }
}

if(is_undefined(map)){map=ds_map_create();}
else{ds_map_clear(map);}


var items = file_bin_read(file_handle,buffer_u32);
var item_idx;
for(item_idx=0 ; item_idx<items ; item_idx++){
    var key   = file_bin_read(file_handle,type);
    var count = file_bin_read(file_handle,buffer_u32);
    for(var i=0; i<count; i++){
        ds_map_add(map,file_bin_read(file_handle,buffer_s32),key);
    }
}

return map;
