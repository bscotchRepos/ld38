/// pn_get_base_noise(grid_x,grid_y,wavelength,seed_modifier)
// SCRIPT REPLACEMENT FOR NOISE using cross-platform random NUMBAHS

/// get_base_noise(grid_x,grid_y,wavelength,seed_modifier)
var xg = argument0, yg = argument1, wl = argument2, seedmod = argument3;
var xwave = floor(xg/wl);
var ywave = floor(yg/wl);

buffer_seek(RANDOM_X, buffer_seek_start,2*(abs(xwave*XMOD_RAND-ywave*YMOD_RAND+seedmod+RANDBUFFER_LENGTH) mod RANDBUFFER_LENGTH));
buffer_seek(RANDOM_Y, buffer_seek_start,2*(abs(ywave*XMOD_RAND+xwave*YMOD_RAND+seedmod+RANDBUFFER_LENGTH) mod RANDBUFFER_LENGTH));
    
var rand_x = buffer_read(RANDOM_X,buffer_u16);
var rand_y = buffer_read(RANDOM_Y,buffer_u16);

var tile = (rand_x+rand_y) mod MAX_RAND_NUMBER ;

return tile;
