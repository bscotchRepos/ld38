/// @arg the_key
var the_key = argument0;

if ds_map_exists(KEY_DISPLAYS, the_key) {
    return KEY_DISPLAYS[?the_key];
}
else return "--";
