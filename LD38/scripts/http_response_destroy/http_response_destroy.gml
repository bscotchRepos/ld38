/// @desc Destroy the data structure created by http_response, if it exists
/// @param response

if(ds_map_does_exist(argument[0])){
	ds_map_destroy(argument[0]);
}
