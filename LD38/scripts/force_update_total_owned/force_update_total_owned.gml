// Dig through the inventory/equipment.
var totalowned = 0;
ds_map_clear(TOTAL_OWNED);
    
for (i = 0; i < ds_list_size(ITEMS_OWNED); i++){       
    var it = ds_list_find_value(ITEMS_OWNED, i);        
    var quant = ds_list_find_value(ITEM_STACKS,i);     
    modify_total_owned(it, quant);  
}    

