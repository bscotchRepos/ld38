for ( var i = 0; i < array_height_2d(anim_bones); i++){
	anim_set_bone_direction(i, anim_targs[i,anim_rotation_targ], anim_targs[i,anim_rotation_speed], anim_bones[i, anim_rotation_relative]);
	anim_set_bone_offset(i, anim_xoffset, anim_targs[i,anim_xoffset_targ], anim_targs[i,anim_xoffset_speed], anim_bones[i, anim_offset_relative]);
	anim_set_bone_offset(i, anim_yoffset, anim_targs[i,anim_yoffset_targ], anim_targs[i,anim_yoffset_speed], anim_bones[i, anim_offset_relative]);
}
