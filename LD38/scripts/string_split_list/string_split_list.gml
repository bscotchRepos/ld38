/// string_split_list(string,[separator=','])
//  returns list of strings, split on the separator
var separator = ",";
if(argument_count>1)
{
    separator = argument[1];
}
var to_split  = argument[0];
var exploded  = ds_list_create();
var si = 1 ;
var exploded_idx = 0 ;
var current_word = "" ;
while(true)
{
    // Get the next character in the string
    if(si>string_length(to_split))
    {
        if(current_word != ""){ds_list_add(exploded,current_word);}
        break;
    }
    var char = string_char_at(to_split,si);
    if(char==separator)
    {
        ds_list_add(exploded,current_word);
        exploded_idx++;
        current_word = "";
    }
    else
    {
        current_word += char ;
    }
    si++;
}
return exploded ;
