/// psys_destroy_ps(part_system_id, seconds_delay)
var ps_to_kill = argument0;
var ps_timer = argument1;

with o_particle_manager {
    if !ds_list_contains(ps_death, ps_to_kill) && ds_list_contains(particle_systems, ps_to_kill) {            
        ds_list_add(ps_death, ps_to_kill);
        ds_list_add(ps_death_timers, ps_timer);
    }
}

