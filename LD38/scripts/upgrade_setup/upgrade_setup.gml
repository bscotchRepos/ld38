/// @arg id
/// @arg name
/// @arg description
/// @arg base_value
/// @arg max_level
/// @arg change_amt
/// @arg change_linear_bool
/// @arg cost_modifier
/// @arg label
/// @arg rounded

var upg_id = argument0;

UPGRADES[upg_id, upginfo_name] = argument1;
UPGRADES[upg_id, upginfo_description] = argument2;
UPGRADES[upg_id, upginfo_baseline] = argument3;
UPGRADES[upg_id, upginfo_maxlevel] = argument4;
UPGRADES[upg_id, upginfo_change] = argument5;
UPGRADES[upg_id, upginfo_change_linear] = argument6;
UPGRADES[upg_id, upginfo_cost_mod] = argument7;
UPGRADES[upg_id, upginfo_label] = argument8;
UPGRADES[upg_id, upginfo_rounded] = argument9;

UPGRADES[upg_id, upginfo_level] = 0;