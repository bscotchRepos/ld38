/// deactivate_all_but_persistent([self_also=false])
var self_also = false ;
if( argument_count == 1 ){ self_also = argument[0] ; }
var me = id ;
with all {
    if !persistent {
        if (!self_also && id != me) || self_also {
            instance_deactivate_object(id);
        }
    }
}
