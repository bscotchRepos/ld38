/// @arg soundid
/// @arg priority
/// @arg loops
/// @arg pitch

var s = play_sound_gv(argument0,argument1,argument2);
audio_sound_pitch(s,argument3);
return s;
