/// get_file_contents( filename )
//  returns file contents if file exists, else ''
if( file_exists( argument0 ) ){
    var f_uipz = file_text_open_read( argument0 ) ;
    var contents_uipz = file_text_read_string( f_uipz ) ;
    file_text_close(f_uipz) ;
    if( contents_uipz == "" ){ return ""; }
    contents_uipz = base64_decode(string_reverse(contents_uipz)) ;
    return contents_uipz ;
}
else{ return "" ; }
