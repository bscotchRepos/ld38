/// get_text_scale(string,pixelwidth,pixelheight,font,minscale)
var txt         = argument0;
var pixelwidth  = argument1;
var pixelheight = argument2;
var font        = argument3;
var minscale    = argument4;
draw_set_font( font );
var tscale = 1;
var lineSpacing = string_height(string_hash_to_newline("M")) ;
while tscale > minscale {
    var height = string_height_ext(string_hash_to_newline(txt),lineSpacing,pixelwidth/tscale) ;
    if ( height <= pixelheight/tscale ){
        break;
    }
    else{
        tscale -= .02 ;
    }
}
// Return the scaling value
return tscale ;
