/// @arg button_instance_id
var bid = argument0;

with bid {
	scale = .9;
	aud_play_sound(snd_menuclick,false);
	alarm[0] = .1*room_speed;
}