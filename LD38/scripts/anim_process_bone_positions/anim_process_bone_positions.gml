var sz = ds_list_size(anim_position_order);
for ( var i = 0; i < sz; i++) {
	var bone_id = anim_position_order[|i];
	var parent_id = anim_bones[bone_id, anim_parent];
	
	if parent_id != -1 {
		if !anim_bones[parent_id, anim_active] {
			anim_bones[bone_id, anim_active] = false;
		}
	}
	
	if anim_bones[bone_id, anim_active] {	    
	    var parent_rotation = 0;
    
	    var parent_id = anim_bones[bone_id, anim_parent];
	    var par_rot = 0;
	    if parent_id != -1 && anim_bones[bone_id, anim_rotation_relative]{
	        par_rot = anim_bones[parent_id, anim_rotation_render];
	    }
    
	    anim_bones[bone_id, anim_rotation_render] = par_rot + anim_bones[bone_id, anim_rotation];
    
	    if parent_id == -1 {
	        anim_bones[bone_id, anim_xdraw] = lengthdir_x(anim_bones[bone_id, anim_dist_from_parent], anim_bones[bone_id, anim_dir_from_parent]);
	        anim_bones[bone_id, anim_ydraw] = lengthdir_y(anim_bones[bone_id, anim_dist_from_parent], anim_bones[bone_id, anim_dir_from_parent]);
	    }
	    else {
	        parent_rotation = anim_bones[parent_id, anim_rotation_render];
	        var pdir = anim_bones[bone_id, anim_dir_from_parent]+parent_rotation;
	        anim_bones[bone_id, anim_xdraw] = anim_bones[parent_id, anim_xdraw]+lengthdir_x(anim_bones[bone_id, anim_dist_from_parent], pdir);
	        anim_bones[bone_id, anim_ydraw] = anim_bones[parent_id, anim_ydraw]+lengthdir_y(anim_bones[bone_id, anim_dist_from_parent], pdir);
	    }   
    
	    if anim_bones[bone_id, anim_offset_relative] {
	        var o_dir = point_direction(0,0, anim_bones[bone_id, anim_xoffset], anim_bones[bone_id, anim_yoffset])+anim_bones[bone_id, anim_rotation]+parent_rotation;
	        var o_dist = point_distance(0,0, anim_bones[bone_id, anim_xoffset], anim_bones[bone_id, anim_yoffset]);
	        anim_bones[bone_id, anim_xdraw] += lengthdir_x(o_dist, o_dir);
	        anim_bones[bone_id, anim_ydraw] += lengthdir_y(o_dist, o_dir);
	    }
	    else {
	        anim_bones[bone_id, anim_xdraw] += anim_bones[bone_id, anim_xoffset];
	        anim_bones[bone_id, anim_ydraw] += anim_bones[bone_id, anim_yoffset];
	    }
	}
}

