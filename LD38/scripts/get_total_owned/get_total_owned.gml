/// @arg item_type

var itemtype = argument0;
if ds_map_exists(TOTAL_OWNED, itemtype) {
    return ds_map_find_value(TOTAL_OWNED, itemtype);
}
return 0;
