/// @desc Return a value from an arbitrary nesting of maps of maps
/// @param {ds_map} map_of_maps
/// @param {string} key1
/// @param [key2,...]
var mapmap = argument[0];
var out = undefined; 

for( var k=1; k<argument_count; k++){
	out = mapmap[?argument[k]];
	if(k+1==argument_count){ return out ;}
	if(not ds_map_does_exist(out)){ return undefined; }
	else{
		mapmap = out;
	}
}

return out;
