/// abl_can_use(ability_channel)

var ab = argument0;
if ab < 0  
    return false;
    
if !abilities[ab, abl_enabled]
    return false;
    
if ability_global_cooldown > 0 && !abilities[ab, abl_ignore_gcd] {
    //echo(abilities[ab, abl_name],'cannot be used because the global cooldown is up.');
    return false;
}
else if abilities[ab, abl_charges] <= 0 && abilities[ab, abl_max_charges] > 0{
    //echo(abilities[ab, abl_name],'cannot be used because it has no charges.');
    return false;
}
else if abilities[ab, abl_state] >= 0 {
    // Ability is already in use.
    //echo(abilities[ab, abl_name],'cannot be used because it is already in use.');
    return false;
}
else {
    for ( var i = 0; i < array_height_2d(abilities); i++){
        if i != ab {
            if abl_is_active(i) && abilities[i, abl_stop_other_abilities]
                return false;
        }
    }

    var ab_resource = abilities[ab, abl_resource_channel];
    
    if ab_resource == -1 { return true; } // Doesn't use a resource.
    else {
        var have_resources = ( abilities[ab, abl_resource_cost] <= resources[ab_resource, rscinfo_current])
        //if !have_resources { echo(abilities[ab, abl_name],'cannot be used because there is not enough',resources[ ab_resource, rscinfo_name ]) };
        return have_resources;
    }
}
