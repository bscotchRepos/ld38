/// @arg text
/// @arg time
/// @arg color
/// @arg [persistent]

with o_onscreen_message instance_destroy();

with instance_create(0,0,o_onscreen_message) {    
    curtext = argument[0];
    vis = argument[1];
    c = argument[2];
	if argument_count > 3 {
		persistent = argument[3];
	}
}
