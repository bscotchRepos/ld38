/// @desc converts xy coord to a s32
/// @arg x
/// @arg y

buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
buffer_write(BUFFER_4BYTE,buffer_s16,argument1);
buffer_write(BUFFER_4BYTE,buffer_s16,argument0);
buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
return buffer_read(BUFFER_4BYTE,buffer_s32);
