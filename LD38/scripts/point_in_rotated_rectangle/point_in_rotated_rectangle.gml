/// point_in_rotated_rectangle(point_x,point_y,rect_origin_x,rect_origin_y,rect_rotation,rect_width,rect_height)

var pch_x = argument0,
    pch_y = argument1,
    rect_origin_x = argument2,
    rect_origin_y = argument3,
    rect_rotation = argument4,
    rect_width = argument5,
    rect_height = argument6;
 
var x1 = rect_origin_x+lengthdir_x(rect_height*.5,rect_rotation+90);
var y1 = rect_origin_y+lengthdir_y(rect_height*.5,rect_rotation+90);

var x2 = rect_origin_x+lengthdir_x(rect_height*.5,rect_rotation-90);
var y2 = rect_origin_y+lengthdir_y(rect_height*.5,rect_rotation-90);

var x3 = x1+lengthdir_x(rect_width,rect_rotation);
var y3 = y1+lengthdir_y(rect_width,rect_rotation);
   
var x4 = x2+lengthdir_x(rect_width,rect_rotation);
var y4 = y2+lengthdir_y(rect_width,rect_rotation);

/*
    1       3 
  origin
    2       4
*/

if point_in_triangle(pch_x,pch_y,x1,y1,x2,y2,x3,y3)
    return true;
if point_in_triangle(pch_x,pch_y,x2,y2,x3,y3,x4,y4)
    return true;
    
return false;
