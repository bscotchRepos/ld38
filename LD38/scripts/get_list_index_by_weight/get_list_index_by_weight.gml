/// get_list_index_by_weight(list_of_weights)

var wtlist = argument0; // GIVE IT A LIST OF WEIGHTS!

var wt = 0;
var totalweights = ds_list_sum(wtlist);
var standard = 10000;
if totalweights == 0
    return 0;
var modifier = standard/totalweights;
var chosen_item = -1;
var roll = irandom(standard);

var i, done = false;
for (i = 0; i < ds_list_size(wtlist) && !done; i++){
    var new_wt = modifier*ds_list_find_value(wtlist,i);
    if roll >= wt {
        chosen_item = i;
    }
    else done = true;
    wt += new_wt;    
}

return chosen_item;



// This will return
