///mv_move_ext(movedir,move_amt,ignore_collision)
var mvdir, move_amt, ignore_collision;

mvdir = argument[0];
move_amt = argument[1];
ignore_collision = argument[2];

if (ignore_collision || place_free(x+lengthdir_x(move_amt, mvdir), y+lengthdir_y(move_amt, mvdir)) || !place_free(x,y)){
    x += lengthdir_x(move_amt, mvdir);
    y += lengthdir_y(move_amt, mvdir);
}
else {
    var place_found = false;
    var angle_max = 90;
    var increment = 10;
    for ( var a = increment; a <= angle_max && !place_found; a += increment) {
        for ( var dir = -1; dir <= 1 && !place_found; dir += 2) {
            var md = mvdir + a*dir;
            var xtarg = x+lengthdir_x(move_amt, md);
            var ytarg = y+lengthdir_y(move_amt, md)
            if place_free(xtarg, ytarg) {
                place_found = true;
                x += lengthdir_x(move_amt, md);
                y += lengthdir_y(move_amt, md);
                break;
            }
        }
    }
    
    if !place_found {
        move_contact_solid(mvdir, move_amt)
    }
}

