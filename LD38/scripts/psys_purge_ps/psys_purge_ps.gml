/// psys_purge_ps(particle_system_index)

var ps = argument0;

if ds_list_contains(particle_systems, ps) {
    var ps_index = ds_list_find_index(particle_systems, ps);
    ds_list_delete(particle_systems, ps_index);
    
    if ds_map_exists(particle_types, ps) {
        var ptypes = ds_map_find_value(particle_types, ps);
        for ( var i = 0; i < ds_list_size(ptypes); i++){
            part_type_destroy(ptypes[|i]);
        }
        ds_list_destroy(ptypes);
        ds_map_delete(particle_types, ps);
    }
    
    if ds_map_exists(particle_instances, ps) {
        ds_map_delete(particle_instances, ps);
    }
        
    if ds_list_contains(ps_death, ps) {
        var pindex = ds_list_find_index(ps_death, ps);
        ds_list_delete(ps_death, pindex);
        ds_list_delete(ps_death_timers, pindex);
    }
    
    part_system_destroy(ps);
}


