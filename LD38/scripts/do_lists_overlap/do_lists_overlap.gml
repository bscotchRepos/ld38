/// do_lists_overlap(list1,list2)
// Returns true if two lists have one or more of the same components.
var i;
var j;
var list1 = argument0;
var list2 = argument1;

if ds_list_empty(list1) || ds_list_empty(list2)
    return false;

for (i = 0; i < ds_list_size(list1); i++){
    var item1 = ds_list_find_value(list1,i);
    for (j = 0; j < ds_list_size(list2); j++) {
        var item2 = ds_list_find_value(list2,j);
        if (item1 == item2)
            return true;
    }
}
return false;
