/// blink(blink_variable)

var blnk = argument0;
var persec = delta_time/1000000
if blnk == 0 && random(1) <= SPD*persec
   blnk = persec;
if blnk > 0{
   blnk += SPD*persec;
   if blnk >= 1
      blnk = 0
}
return blnk;
