/// @arg target_instance

with o_camera {
	var targ_index = ds_list_find_index(my_targets, argument0);
	if targ_index != -1 {
		ds_list_delete(my_targets, targ_index);
		ds_list_delete(target_weights, targ_index);
	}  
}
