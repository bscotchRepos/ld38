/// @arg bone
/// @arg direction
/// @arg xoffset
/// @arg yoffset
/// @arg [speed]

var bone = argument[0];
var dir = argument[1];
var xo = argument[2];
var yo = argument[3];

if argument_count > 4 {
	anim_targs[bone, anim_rotation_speed] = argument[4];
	anim_targs[bone,  anim_xoffset_speed] = argument[4];
	anim_targs[bone,  anim_yoffset_speed] = argument[4];
}

anim_targs[bone, anim_rotation_targ] = dir;
anim_targs[bone, anim_xoffset_targ] = xo;
anim_targs[bone, anim_yoffset_targ] = yo;

