/// @arg current
/// @arg target
/// @arg amount

var current, target, spd;
current = argument0;
target = argument1;
spd = argument2*SLOMO_SECONDS;

var diff = (target-current);
var newval = clamp(diff*spd,-abs(diff),abs(diff));
return newval;

