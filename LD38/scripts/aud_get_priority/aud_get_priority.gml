/// @arg sound_id

if ds_map_exists(SOUND_PRIORITIES, argument0)
	return SOUND_PRIORITIES[?argument0];
return 0;