/// @arg music_id
/// @arg looping

var newmusic = argument0, looping = argument1;

if newmusic != MUSICID {
    audio_stop_sound(MUSICID);
    if newmusic != -1 {
        var s = audio_play_sound(newmusic,50,looping);
        audio_sound_gain(s,MUSIC_SETTING_VOLUME,0);
    }
    MUSICID = newmusic;
}

   
