with o_particle_manager {
    for ( var i = ds_list_size(particle_systems)-1; i >= 0 ; i--){
        psys_purge_ps( particle_systems[|i] );
    }
}
