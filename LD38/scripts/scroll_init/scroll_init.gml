/// @arg channel_id
/// @arg allow_horiz
/// @arg allow_vert
/// @arg gp_left_stick
/// @arg gp_right_stick
/// @arg gp_dpad
/// @arg hotkey_up
/// @arg hotkey_down
/// @arg hotkey_left
/// @arg hotkey_right
/// @arg mousewheel_vertical
/// @arg min_x
/// @arg max_x
/// @arg min_y
/// @arg max_y

// Script to allow for scrolling of everything in the universe.
var channel_id     = argument[ 0 ];

sc_gplaxis[channel_id]     = argument[ 3 ]; // Enable scrolling with left gamepad axis
sc_gpraxis[channel_id]     = argument[ 4 ]; // Enable scrolling with right gamepad axis
sc_gpdpad[channel_id]      = argument[ 5 ]; // Enable scrolling with the dpad

sc_hk_up[channel_id]       = argument[ 6 ]; // Hotkey for scrolling up 
sc_hk_down[channel_id]     = argument[ 7 ]; // Hotkey for scrolling down
sc_hk_left[channel_id]     = argument[ 8 ]; // Hotkey for scrolling left
sc_hk_right[channel_id]    = argument[ 9 ]; // Hotkey for scrolling right

sc_mwheel_vert[channel_id] = argument[ 10 ];

sc_min[channel_id,0]       = argument[ 11];
sc_max[channel_id,0]       = argument[ 12];

sc_min[channel_id,1]       = argument[ 13];
sc_max[channel_id,1]       = argument[ 14];

sc_click_cooldown[channel_id]      = .1;
sc_clicktimer[channel_id]          = 0;
sc_device[channel_id]              = -1;

for ( var i = 0; i <= 1; i++){
    var chid = channel_id+i;
    sc_allow[channel_id,i]         = argument[i+1];
    sc_offset[channel_id,i]        = 0;
    sc_targ[channel_id,i]          = 0;
    sc_last[channel_id,i]          = 0;
    sc_anchor[channel_id,i]        = 0;
    sc_speed[channel_id,i]         = 0;
    sc_lastpositions[channel_id,i] = ds_list_create();
}


