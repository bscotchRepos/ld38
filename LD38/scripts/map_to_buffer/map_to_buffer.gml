/// map_to_buffer(map,key_variable_type,value_variable_type,prefix[,list_of_keys])
var the_map = argument[0];
var key_variable_type = argument[1];
var value_variable_type = argument[2];
var prefix = argument[3];

echo("Saving map to buffer.");
if key_variable_type == buffer_string || value_variable_type == buffer_string
    show_error("Cannot save strings in map_to_buffer. Only numbers.",true);

var length = ds_map_size(the_map);
var the_buffer = buffer_create(
     buffer_sizeof(buffer_u16)+buffer_sizeof(buffer_u32)+(buffer_sizeof(key_variable_type)+buffer_sizeof(value_variable_type))*length,buffer_fixed,1);
buffer_write( the_buffer, buffer_u16, prefix );
buffer_write( the_buffer, buffer_u32, length );

var list_iterate = false;

if argument_count == 5
    if ds_list_size(argument[4]) == length
        list_iterate = true;

if !list_iterate {
    var k = ds_map_find_first(the_map), i;
    for (var i = 0; i < length; i++) {
        buffer_write( the_buffer, key_variable_type, k);
        buffer_write( the_buffer, value_variable_type, ds_map_find_value(the_map,k));
        k = ds_map_find_next(the_map,k);
    }
}
else {
    var list_of_keys = argument[4];
    for (var i = 0; i < length; i++){
        var k = list_of_keys[|i];
        var v = the_map[?k];
        buffer_write(the_buffer, key_variable_type, k);
        buffer_write(the_buffer, value_variable_type, v);
    }
}

return the_buffer;
