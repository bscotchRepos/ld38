/// @arg sound
/// @arg looping

// Plays sound effect with global volume attached, pulling from sound-specific gain and sound-specific priority.

var sid = audio_play_sound(argument0,aud_get_priority(argument0),argument1);
audio_sound_gain(sid,SOUND_SETTING_VOLUME*aud_get_gain(argument0),0);
return sid;