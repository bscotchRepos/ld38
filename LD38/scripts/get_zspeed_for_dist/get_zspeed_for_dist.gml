/// get_zspeed_for_dist(distance,movespeed)
var dist_to_travel = argument0, movspd = argument1, zsp;

zsp = (GRAVITY*(dist_to_travel/movspd))/2;
return zsp;
