/// set_depth_to_top([always_on_top])
var ontop = false;
if argument_count > 0 {
    ontop = argument[0];
    if ontop {
        if !ds_list_contains(ALWAYS_ON_TOP,id) {
            ds_list_insert(ALWAYS_ON_TOP,0,id);
        }
    }
}

var d = depth;
with all {
    if !ds_list_contains(ALWAYS_ON_TOP,id) {
        d = min(d,depth-1);
    }
}
depth = d;

// Now, put the ALWAYS ON TOP things higher up. DRAMATICALLY.
d -= 10000;
for ( var i = ds_list_size(ALWAYS_ON_TOP)-1; i >= 0; i--) {
    var inst = ALWAYS_ON_TOP[|i];
    if !instance_exists(inst) {
        ds_list_delete(ALWAYS_ON_TOP,i);
    }
    else {
        d -= 1;
        with inst { depth = d; }
        // Since we are iterating from the top of the list down,
        // and since newer items are inserted at position 0,
        // The newest things added to the always on top section
        // Will still go on top of older things.
    }
}




