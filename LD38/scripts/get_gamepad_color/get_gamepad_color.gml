/// @arg gamepad_key

var gpkey = argument0;

if ds_map_exists(GAMEPAD_COLORS, gpkey) {
	return GAMEPAD_COLORS[?gpkey];
}
return c_white;


