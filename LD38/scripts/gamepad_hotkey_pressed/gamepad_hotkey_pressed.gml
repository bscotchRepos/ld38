/// @arg gamepad_button
/// @arg [hold=false]

var hold = false;
if CURRENT_GAMEPAD < 0
    return false;
if argument_count > 1
    hold = argument[1];
if !hold 
    return gamepad_button_check_pressed(CURRENT_GAMEPAD,get_gpconfig_button(argument[0]));
else return gamepad_button_check(CURRENT_GAMEPAD,get_gpconfig_button(argument[0]));
