/// draw_sprite_centered_ext(sprite,img,xdraw,ydraw,xscale,yscale,rotation,color,alpha)

var sp = argument0, img = argument1, xdraw = argument2, ydraw = argument3, xscale = argument4, yscale = argument5, rotation = argument6, color = argument7, alpha = argument8;

var w = xscale*sprite_get_width(sp)*.5;
var h = yscale*sprite_get_height(sp)*.5;
var xo = xscale*sprite_get_xoffset(sp);
var yo = yscale*sprite_get_yoffset(sp);

var base_rot = point_direction(w,h,xo,yo);
var drawdist = point_distance(w,h,xo,yo);

draw_sprite_ext(sp,img,
    xdraw+lengthdir_x(drawdist,base_rot+rotation),
    ydraw+lengthdir_y(drawdist,base_rot+rotation),
    xscale,yscale,rotation,color,alpha)
