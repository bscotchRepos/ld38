/// xy_perspective(base_x, base_y, point_dir, squish_amt)

// Squishes lengthdir x and y based on perspective.
// Straight top-down perspective requires no squishing.

// How to determine squish amount:
// If your perspective renders what would ordinarily be a square
//      as a 2:1 rectangle, then the squish amount is 50%. (height/width).

var xx = argument0;
var yy = argument1;
var dir_point = argument2;
var squish_amt = argument3;

var xy = 0;
if squish_amt == 0 {
    xy[0] = xx;
    xy[1] = yy;
}
else {
    var base_length = point_distance(0,0,xx,yy);
    var base_dir = point_direction(0,0,xx,yy);
    
    var rot_x = lengthdir_x(1,dir_point);
    var rot_y = lengthdir_y(1,dir_point)/squish_amt;   
    var new_rotation = point_direction(0,0,rot_x,rot_y);
    
    var new_x = lengthdir_x(base_length, base_dir+new_rotation);
    var new_y = lengthdir_y(base_length*squish_amt, base_dir+new_rotation);
    
    xy[0] = new_x;
    xy[1] = new_y;
}
return xy;
