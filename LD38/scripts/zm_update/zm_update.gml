/// zm_update(z_floor)
var zf = argument0;
z = max(zf, z+zspeed*SLOMO_SECONDS);
if z <= zf && zspeed < 0 {
    zspeed = 0;
}
