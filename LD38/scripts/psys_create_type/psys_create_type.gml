/// @arg ps_owner
/// @arg [lifetime_seconds]
/// @arg [color_start]
/// @arg [color_end]
/// @arg [alpha_start]
/// @arg [alpha_end]
/// @arg [speed_per_sec]
/// @arg [direction_min]
/// @arg [direction_max]
/// @arg [size]
/// @arg [blend]
/// @arg [shape]

var ps_owner = argument[0];
var ptype = part_type_create();

var p_color_start = c_white;
var p_color_end = c_white;
var p_alpha_start = 1;
var p_alpha_end = 0;
var p_blend = 1;
var p_life = 1;
var p_spd = 100;
var p_dir_min = 0;
var p_dir_max = 359;
var p_shape = pt_shape_flare;
var p_size = .5;

if argument_count > 1 { if argument[1] != -1 { p_life = argument[1]; }}
if argument_count > 2 { if argument[2] != -1 { p_color_start = argument[2]; }}
if argument_count > 3 { if argument[3] != -1 { p_color_end = argument[3]; }}
if argument_count > 4 { if argument[4] != -1 { p_alpha_start = argument[4]; }}
if argument_count > 5 { if argument[5] != -1 { p_alpha_end = argument[5]; }}
if argument_count > 6 { if argument[6] != -1 { p_spd = argument[6]; }}
if argument_count > 7 { if argument[7] != -1 { p_dir_min = argument[7]; }}
if argument_count > 8 { if argument[8] != -1 { p_dir_max = argument[8]; }}
if argument_count > 9 { if argument[9] != -1 { p_size = argument[9]; }}
if argument_count > 10 { if argument[10] != -1 { p_blend = argument[10]; }}
if argument_count > 11 { if argument[11] != -1 { p_shape = argument[11]; }}

part_type_life(ptype, .75*p_life*room_speed, p_life*room_speed);
part_type_alpha2(ptype, p_alpha_start, p_alpha_end);
part_type_colour2(ptype, p_color_start, p_color_end);
part_type_blend(ptype, p_blend);
part_type_shape(ptype, p_shape);
part_type_speed(ptype, p_spd*.75/room_speed, p_spd/room_speed, 0, 0);
part_type_direction(ptype, p_dir_min, p_dir_max, 0, 0);
part_type_size(ptype, p_size*.7, p_size, 0, 0);

psys_assign_type_owner(ptype, ps_owner);

return ptype;
