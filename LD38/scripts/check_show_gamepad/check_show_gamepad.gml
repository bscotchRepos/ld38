/// Flips variables on or off depending on whether you are using a gamepad or a mouse.

var cursor_moved = false;

for ( var c = 0; c < 5; c++) {
    if device_mouse_check_button(c,mb_left) {
    
        var dx = device_mouse_x_to_gui(c);
        var dy = device_mouse_y_to_gui(c);
        
        if dx != CURSOR_LAST_X || dy != CURSOR_LAST_Y {
            CURSOR_LAST_X = dx;
            CURSOR_LAST_Y = dy;
            CURRENT_GAMEPAD = -1;
            cursor_moved = true;
        }            
    }
}

if !cursor_moved {
    var gamepad_detected = false;
    for ( var c = 0; c < gamepad_get_device_count() && !gamepad_detected; c++) {
        var gp_lh = gamepad_axis_value(c,gp_axislh);   
        var gp_rh = gamepad_axis_value(c,gp_axisrh);
        var gp_lv = gamepad_axis_value(c,gp_axislv);
        var gp_rv = gamepad_axis_value(c,gp_axisrv);
        
        if (point_distance(0,0,gp_lh,gp_lv) >= GAMEPAD_DEADZONE)
            gamepad_detected = true;
        else if point_distance(0,0,gp_rh,gp_rv) >= GAMEPAD_DEADZONE
            gamepad_detected = true;
        else if( gamepad_button_check_pressed(c,gp_face1) ||
            gamepad_button_check_pressed(c,gp_face2) ||
            gamepad_button_check_pressed(c,gp_face3) ||
            gamepad_button_check_pressed(c,gp_face4) ||
            gamepad_button_check_pressed(c,gp_shoulderrb) ||
            gamepad_button_check_pressed(c,gp_shoulderr) ||
            gamepad_button_check_pressed(c,gp_shoulderlb) ||
            gamepad_button_check_pressed(c,gp_shoulderl) ||
            gamepad_button_check_pressed(c,gp_start) ||
            gamepad_button_check_pressed(c,gp_select) ||
            gamepad_button_check_pressed(c,gp_padu) ||
            gamepad_button_check_pressed(c,gp_padd) ||
            gamepad_button_check_pressed(c,gp_padr) ||
            gamepad_button_check_pressed(c,gp_padl) )
        {
            gamepad_detected = true;
        }
        
        if(gamepad_detected){
            CURRENT_GAMEPAD = c; 
        }
    }
    
    var mx = round(device_mouse_x_to_gui(0));
    var my = round(device_mouse_y_to_gui(0));
    
    if mx != CURSOR_LAST_X || my != CURSOR_LAST_Y {
        CURRENT_GAMEPAD = -1;
        CURSOR_LAST_X = mx;
        CURSOR_LAST_Y = my;
    }
}
