/// slide_modify(speed, direction)

var spd = argument0;
var dir = argument1;

var slide_xspd = lengthdir_x(slide_speed,slide_dir);
var slide_yspd = lengthdir_y(slide_speed,slide_dir);

slide_xspd += lengthdir_x(spd, dir);
slide_yspd += lengthdir_y(spd, dir);

slide_speed = point_distance(0,0,slide_xspd,slide_yspd);
slide_dir = point_direction(0,0,slide_xspd,slide_yspd);            
