/// @arg speedmax_proportion_target
var targspeed = mv_speed_base*argument0;

if targspeed > mv_movespeed {
    // Accelerate
    if mv_accel < 0 {
        mv_movespeed = targspeed;
    }
    else mv_movespeed = min(targspeed, mv_movespeed + mv_accel*SLOMO_SECONDS);
}
else {
    // Decelerate
    if mv_decel < 0 {
        mv_movespeed = targspeed;
    }
    else mv_movespeed = max(targspeed, mv_movespeed - mv_decel*SLOMO_SECONDS);
}


