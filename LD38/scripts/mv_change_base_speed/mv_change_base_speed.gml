/// @arg base_speed
/// @arg accel_prop_per_second
/// @arg decel_prop_per_second
mv_speed_base = argument[0];
mv_accel = argument[1]*mv_speed_base;
mv_decel = argument[2]*mv_speed_base;