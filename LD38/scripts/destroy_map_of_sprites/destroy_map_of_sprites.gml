/// @arg map

/// For destroying a map of dynamic sprites where the values are the sprites.
var the_map = argument0;
var key = ds_map_find_first(the_map);
for ( var i = 0; i < ds_map_size(the_map); i++){
	if sprite_exists(the_map[?key]) {	
		sprite_delete(the_map[?key]);
	}
	key = ds_map_find_next(the_map, key);
}
ds_map_destroy(the_map);