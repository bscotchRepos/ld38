/// @arg map
/// @arg key

// Retreives a map from within a map. Creates the map if it doesn't exist.
var the_map = argument0;
var the_key = argument1;

if !ds_map_exists(the_map, the_key) {
	var new_map = ds_map_create();
	ds_map_add_map(the_map, the_key, new_map);
	return new_map;
}
else return the_map[?the_key];