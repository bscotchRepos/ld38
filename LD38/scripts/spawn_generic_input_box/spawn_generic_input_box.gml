/// @arg x
/// @arg y
/// @arg width
/// @arg height
/// @arg default_text
/// @arg text_color
/// @arg bg_color
/// @arg bg_alpha
/// @arg max_characters
/// @arg [gamepad_key=-1]
/// @arg [keyboard_layout]

with instance_create(argument[0], argument[1], o_generic_text_input) {

	width = argument[2];
	height = argument[3];	
	my_text = argument[4];
	text_color = argument[5];
	bg_color = argument[6];
	bg_alpha = argument[7];
	max_characters = argument[8];
	
	if argument_count > 9
		gamepad_key = argument[9];
		
	if argument_count > 10
		keyboard_layout = argument[10];
	
	return id;
}