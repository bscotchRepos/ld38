/// draw_gamepad_button(button,scale,x,y[,alpha])

// Converts the input button into whatever button matches in the configuration.

var b = argument[0];
var sc = argument[1];
var xx = argument[2];
var yy = argument[3];
var alpha = 1;

if argument_count > 4 {
    alpha = argument[4];
}
var font_force = fnt_smallheader;
if argument_count > 5 { font_force = argument[5]; }

if CURRENT_GAMEPAD >= 0 {
    var original_b = b;
    b = get_gpconfig_button(b);
    font_set(font_force,fa_center,fa_middle);
    
    /*
        A: 102,255,0
        X: 81,167,255
        B: 254, 31, 55
        Y: 255, 168, 11
    */
    
    var shapescale = .7;
    
    switch b {
        case gp_face1:
            draw_sprite_ext(sp_gamepad_button,0,xx,yy,sc,sc,0,gamepad_button_color(original_b),alpha);
            if GAMEPAD_CONFIG == 0 {
                shadowtext(xx,yy,"A",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            }
            else {
                draw_sprite_ext(sp_gp_square,0,xx,yy,sc*shapescale,sc*shapescale,0,c_white,alpha);
            }
            break; 
        case gp_face2:
            draw_sprite_ext(sp_gamepad_button,0,xx,yy,sc,sc,0,gamepad_button_color(original_b),alpha);
            if GAMEPAD_CONFIG == 0 {
                shadowtext(xx,yy,"B",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            }
            else {
                draw_sprite_ext(sp_gp_x,0,xx,yy,sc*shapescale,sc*shapescale,0,c_white,alpha);
            }
            break; 
        case gp_face4:
            draw_sprite_ext(sp_gamepad_button,0,xx,yy,sc,sc,0,gamepad_button_color(original_b),alpha);
            if GAMEPAD_CONFIG == 0 {
                shadowtext(xx,yy,"Y",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            }
            else {
                draw_sprite_ext(sp_gp_triangle,0,xx,yy,sc*shapescale,sc*shapescale,0,c_white,alpha);
            }
            break;
        case gp_face3:
            draw_sprite_ext(sp_gamepad_button,0,xx,yy,sc,sc,0,gamepad_button_color(original_b),alpha);
            if GAMEPAD_CONFIG == 0 {
                shadowtext(xx,yy,"X",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            }
            else {
                draw_sprite_ext(sp_gp_circle,0,xx,yy,sc*shapescale,sc*shapescale,0,c_white,alpha);
            }
            break;
        case gp_start:
            draw_sprite_ext(sp_gamepad_start,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"Start",sc,sc,0,c_white,alpha,3*sc);
            break;
        case gp_select:
            draw_sprite_ext(sp_gamepad_start,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"Select",sc,sc,0,c_white,alpha,3*sc);
            break;
        case gp_shoulderr:
            draw_sprite_ext(sp_gamepad_bumper,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"RB",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            break;
        case gp_shoulderrb:
            draw_sprite_ext(sp_gamepad_bumper,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"RT",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            break;
        case gp_shoulderl:
            draw_sprite_ext(sp_gamepad_bumper,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"LB",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            break;
        case gp_shoulderlb:
            draw_sprite_ext(sp_gamepad_bumper,0,xx,yy,sc,sc,0,c_gray,alpha);
            shadowtext(xx,yy,"LT",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            break;
        case gp_padu:
            draw_sprite_ext(sp_gamepad_dpad,0,xx,yy,sc,sc,90,c_white,alpha);            
            break; 
        case gp_padd:
            draw_sprite_ext(sp_gamepad_dpad,0,xx,yy,sc,sc,270,c_white,alpha);            
            break; 
        case gp_padl:
            draw_sprite_ext(sp_gamepad_dpad,0,xx,yy,sc,sc,180,c_white,alpha);            
            break;
        case gp_padr:
            draw_sprite_ext(sp_gamepad_dpad,0,xx,yy,sc,sc,0,c_white,alpha);            
            break;            
        case -1:
            draw_sprite_ext(sp_gamepad_dpad_all,0,xx,yy,sc,sc,0,c_white,alpha);            
            break;
        case gp_stickl:
            draw_sprite_ext(sp_gamepad_joystick,0,xx,yy,sc,sc,0,c_gray,alpha);      
            shadowtext(xx,yy,"LS",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);      
            break;
        case gp_stickr:
            draw_sprite_ext(sp_gamepad_joystick,0,xx,yy,sc,sc,0,c_gray,alpha);  
            shadowtext(xx,yy,"RS",sc*1.25,sc*1.25,0,c_white,alpha,3*sc);
            break;        
    }
}
