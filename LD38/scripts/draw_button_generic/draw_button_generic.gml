/// @arg xx
/// @arg yy
/// @arg width
/// @arg height
/// @arg radius
/// @arg text
/// @arg text_scale
/// @arg scale
/// @arg color
/// @arg [gamepad_key]

var xx = argument[0]; // X draw position
var yy = argument[1]; // Y draw position
var wd = argument[2]; // Width
var ht = argument[3]; // Height
var rad = argument[4]; // Radius
var txt = argument[5]; // Text to draw
var tscale = argument[6]; // Text scale
var sc = argument[7]; // Scale
var col = argument[8]; // Color
var gpk = -1;

var a_mod = .5+.5*active;

if argument_count > 9 {
	gpk = argument[9];
	if CURRENT_GAMEPAD >= 0 {
		col = get_gamepad_color( gpk );
	}
}

var dark_c = color_change(col,.6);

if wd > 0 && ht > 0 {
    wd *= sc;
    ht *= sc;
    var corner_radius = min(ht*.5,wd*.5);
    draw_set_alpha(.8*a_mod);
    draw_roundrect_color_ext(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5, corner_radius,corner_radius, c_black,c_black,0)
	//draw_rectangle_colour(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5,
    //    c_black,c_black,c_black,c_black,0);
    draw_set_alpha(a_mod);	
    draw_roundrect_color_ext(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5, corner_radius,corner_radius, dark_c,dark_c,1)
	//draw_rectangle_colour(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5,
    //    dark_c,dark_c,dark_c,dark_c,1);
}    
if rad > 0 {
    rad *= sc;
    draw_set_alpha(.8*a_mod);
    draw_circle_colour(xx, yy, rad, c_black, c_black, 0);
    draw_set_alpha(a_mod);
    draw_circle_colour(xx, yy, rad, dark_c, dark_c, 1);
}

if txt != "" {
	var txt_display = txt;
	if CURRENT_GAMEPAD >= 0 && gpk != -1 {
		txt_display += " (" + get_gamepad_display( gpk ) + ")";
	}
    font_set(fnt_header,fa_center,fa_middle);
    var tfit = max(width,radius*2);
    var theight = max(height,radius*2);
    draw_text_to_fit_oneline(txt_display,(tfit*.9)*sc,(theight*.9)*sc,xx,yy,.01*sc,tscale*sc,col,0,a_mod);
    //shadowtext(xx,yy,txt,tscale*sc,tscale*sc,0,col,1,2);
}
