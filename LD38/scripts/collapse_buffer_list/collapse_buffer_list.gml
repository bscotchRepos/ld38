/// collapse_buffer_list( buffer_list [,prefix=undefined [,include_hash=false]] )
//  concatenate all buffers into one and then DELETE all buffers and the list itself
//  prefix has to fit into buffer_u16 if defined
var list         = argument[0]
var databytes    = 0;
var prefix       = undefined ;
var prefix_bytes = 0;
var include_length = false;
var include_hash   = false;
var hash_bytes     = 0;


if(     argument_count > 1){ prefix = argument[1];}
if(argument_count > 2){ include_hash=argument[2]; }

if( not is_undefined(prefix) ){ prefix_bytes = 2 ;}
if( include_hash ){ hash_bytes = 33; }

var list_items = ds_list_size(list) ;

// tally the size of the buffer
var idx;
for( idx=0 ; idx<list_items ; idx++){
    databytes+= buffer_get_size(list[|idx])
}

var pos = 0 ;
var buffer_size = databytes+prefix_bytes+hash_bytes ;

var out_buffer = buffer_create( buffer_size, buffer_fixed, 1);
if( not is_undefined(prefix) ){ buffer_write(out_buffer,buffer_u16,prefix); pos=2;}


for(idx=0; idx<list_items; idx++){
    var src_size = buffer_get_size(list[|idx]);
    buffer_copy( list[|idx],0,src_size,out_buffer,pos);
    pos += src_size ;
    buffer_delete(list[|idx]);
}

if(include_hash){
    var the_hash = md5_string_unicode( buffer_base64_encode(out_buffer,0,pos) )
    pos = buffer_seek(out_buffer,buffer_seek_start,buffer_size-33)
    buffer_write(out_buffer,buffer_string,the_hash)
}

ds_list_destroy(list);
buffer_seek(out_buffer,buffer_seek_start,0)

return out_buffer;
