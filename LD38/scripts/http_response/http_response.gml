/// @desc Ask an http_request object for its status and data
/// @param http_request_id
/// @param [kill_if_done=true]
// Note that one must take EXTRA CARE to avoid memory leaks when seeking a "map" response --
//   the request object and scripts will not destroy it for you!
var poll_http_id = argument[0];
var kill_if_done = true;
if(argument_count>1){
	kill_if_done = argument[1];
}
var poll_response = undefined;

if( not instance_exists(poll_http_id) ){
	echo("O_HTTP instance does not exist!");
	return poll_response;
}
else if(http_done(poll_http_id)){
	with( poll_http_id ){
		poll_response = new_map_from_args(
			"done",done,
			"failed",failed,
			"status",status,
			"response_type",response_type,
			"response_raw",response_raw,
			"response",response
		);
		if(poll_response[?"response_type"]=="map"){
			poll_response[?"response"]=json_decode(poll_response[?"response_raw"]);
		}
		if(kill_if_done){
			instance_destroy();
		}
	}
}

return poll_response;
