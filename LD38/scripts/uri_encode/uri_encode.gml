///uri_encode(string)
// return string as URI-encoded
var input_string = string(argument0);
var encoded = "";
var uri_code = ds_map_create();
ds_map_add(uri_code," ","%20");
ds_map_add(uri_code,"!","%21");
//ds_map_add(uri_code,""","%22");
ds_map_add(uri_code,"#","%23");
ds_map_add(uri_code,"$","%24");
ds_map_add(uri_code,"%","%25");
ds_map_add(uri_code,"&","%26");
ds_map_add(uri_code,"'","%27");
ds_map_add(uri_code,"(","%28");
ds_map_add(uri_code,")","%29");
ds_map_add(uri_code,"*","%2A");
ds_map_add(uri_code,"+","%2B");
//ds_map_add(uri_code,",","%2C");
//ds_map_add(uri_code,"-","%2D");
//ds_map_add(uri_code,".","%2E");
ds_map_add(uri_code,"/","%2F");
//ds_map_add(uri_code,":","%3A");
ds_map_add(uri_code,";","%3B");
ds_map_add(uri_code,"<","%3C");
ds_map_add(uri_code,"=","%3D");
ds_map_add(uri_code,">","%3E");
ds_map_add(uri_code,"?","%3F");
ds_map_add(uri_code,"@","%40");
ds_map_add(uri_code,"[","%5B");
ds_map_add(uri_code,"\\","%5C");
ds_map_add(uri_code,"]","%5D");
ds_map_add(uri_code,"^","%5E");
//ds_map_add(uri_code,"_","%5F");
//ds_map_add(uri_code,"`","%60");
ds_map_add(uri_code,"{","%7B");
ds_map_add(uri_code,"|","%7C");
ds_map_add(uri_code,"}","%7D");
ds_map_add(uri_code,"~","%7E");
ds_map_add(uri_code,chr(10),"%0A");
ds_map_add(uri_code,chr(13),"%0D");

var i;
for(i=1;i<=string_length(input_string);i++){
    var char = string_char_at(input_string,i);
    var encodedChar = ds_map_find_value(uri_code,char);
    if(is_undefined(encodedChar)){encoded+=char;}
    else{encoded+=encodedChar;}
}
ds_map_destroy(uri_code);
return encoded;
