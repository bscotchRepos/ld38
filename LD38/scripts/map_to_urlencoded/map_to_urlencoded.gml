/// @desc Convert a map of param:value pairs into a URL query/body url/form-encoded string
/// @param data_map
var encoded = "" ;
var data_map = argument[0];

if( ds_map_does_exist(data_map) ){
	if(ds_map_size(data_map)>0){
		var key = ds_map_find_first(data_map);
		for( var i=0; i< ds_map_size(data_map); i++){
			encoded += uri_encode(key)+"="+uri_encode(data_map[?key]);
			key = ds_map_find_next(data_map,key);
		}
	}
}

return encoded ;
