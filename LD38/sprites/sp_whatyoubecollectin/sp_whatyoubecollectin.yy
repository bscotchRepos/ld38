{
    "id": "8977689d-b71f-4f24-994f-5ff5bef88c67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_whatyoubecollectin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 190,
    "bbox_left": 0,
    "bbox_right": 349,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d603b5fd-7d2b-4198-9bbe-e3ec5b70c6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8977689d-b71f-4f24-994f-5ff5bef88c67",
            "compositeImage": {
                "id": "476602ce-a814-4bce-9171-89a9c6c137f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d603b5fd-7d2b-4198-9bbe-e3ec5b70c6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845aa03d-71a1-44d3-9d1b-5359e42871b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d603b5fd-7d2b-4198-9bbe-e3ec5b70c6b7",
                    "LayerId": "46d715c6-5e61-4328-a2a7-6f83a33c60f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 191,
    "layers": [
        {
            "id": "46d715c6-5e61-4328-a2a7-6f83a33c60f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8977689d-b71f-4f24-994f-5ff5bef88c67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 350,
    "xorig": 276,
    "yorig": 40
}