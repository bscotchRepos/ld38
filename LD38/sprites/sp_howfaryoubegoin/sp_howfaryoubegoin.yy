{
    "id": "36512856-6bda-4bcc-a589-c8b9a5f0bc86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_howfaryoubegoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 0,
    "bbox_right": 501,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a97d43da-381f-4402-aa5b-6e8c87c3f6c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36512856-6bda-4bcc-a589-c8b9a5f0bc86",
            "compositeImage": {
                "id": "7de5dc20-ba1b-4bd0-b11e-0435424e6ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a97d43da-381f-4402-aa5b-6e8c87c3f6c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "631016b8-5f25-4625-9c49-edf67218da47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a97d43da-381f-4402-aa5b-6e8c87c3f6c8",
                    "LayerId": "caa56ad9-4e86-48dc-aa28-e330f3e1a461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 116,
    "layers": [
        {
            "id": "caa56ad9-4e86-48dc-aa28-e330f3e1a461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36512856-6bda-4bcc-a589-c8b9a5f0bc86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 502,
    "xorig": 61,
    "yorig": 56
}