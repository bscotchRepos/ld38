{
    "id": "579ad86b-8a88-42cd-90bd-070fb03ec349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_button_main_loins",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 278,
    "bbox_left": 0,
    "bbox_right": 524,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fa59371f-bf14-4bad-9519-4bbbaeedf9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "579ad86b-8a88-42cd-90bd-070fb03ec349",
            "compositeImage": {
                "id": "98ba7f9b-21ad-4831-a350-2f730a6ada3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa59371f-bf14-4bad-9519-4bbbaeedf9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef1c884-3b4e-4045-9554-f97bc8a495af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa59371f-bf14-4bad-9519-4bbbaeedf9e9",
                    "LayerId": "8d68efe7-e169-45d0-8b29-19c7cc3a850a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 279,
    "layers": [
        {
            "id": "8d68efe7-e169-45d0-8b29-19c7cc3a850a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "579ad86b-8a88-42cd-90bd-070fb03ec349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 525,
    "xorig": 262,
    "yorig": 139
}