{
    "id": "1073d56c-2ecd-332a-cbfe-dc50d47c4be5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dirb_wing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0bc3b645-dc34-4d5c-a699-2702e8f1e42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1073d56c-2ecd-332a-cbfe-dc50d47c4be5",
            "compositeImage": {
                "id": "4742b759-faca-47c6-b1c6-b62aa36e71c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc3b645-dc34-4d5c-a699-2702e8f1e42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd87006-9ab0-4745-a390-5d4e7ae61a59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc3b645-dc34-4d5c-a699-2702e8f1e42c",
                    "LayerId": "eb237529-a5fd-e81a-4aed-7add4817c9b9"
                }
            ]
        },
        {
            "id": "ea6f17a6-93ad-4053-b111-1fff79195fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1073d56c-2ecd-332a-cbfe-dc50d47c4be5",
            "compositeImage": {
                "id": "f5092fcc-cf86-4bd3-97ff-fdd7b337d12f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6f17a6-93ad-4053-b111-1fff79195fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "111dad57-d57f-4584-8755-9e3e3e2db078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6f17a6-93ad-4053-b111-1fff79195fe1",
                    "LayerId": "eb237529-a5fd-e81a-4aed-7add4817c9b9"
                }
            ]
        },
        {
            "id": "314d5449-652a-4beb-b840-8a52e91ba892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1073d56c-2ecd-332a-cbfe-dc50d47c4be5",
            "compositeImage": {
                "id": "50c83264-a5dd-47b9-baf3-24450a97a78a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "314d5449-652a-4beb-b840-8a52e91ba892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea71fb4-d73c-4233-b4db-a97f717d4540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "314d5449-652a-4beb-b840-8a52e91ba892",
                    "LayerId": "eb237529-a5fd-e81a-4aed-7add4817c9b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "eb237529-a5fd-e81a-4aed-7add4817c9b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1073d56c-2ecd-332a-cbfe-dc50d47c4be5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 44,
    "xorig": 37,
    "yorig": 26
}