{
    "id": "bfd74c20-6793-4c53-8f24-58d13eee8104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_flex",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 0,
    "bbox_right": 1611,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6dd4c27d-a33d-4082-9b6b-566db3428df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd74c20-6793-4c53-8f24-58d13eee8104",
            "compositeImage": {
                "id": "182769d4-86df-425e-9ecb-b1794f44a24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd4c27d-a33d-4082-9b6b-566db3428df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ec88a2-2da9-43ec-b674-a7be39dfa25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd4c27d-a33d-4082-9b6b-566db3428df2",
                    "LayerId": "d6b93988-6cac-41c6-af48-6bb648f709fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 246,
    "layers": [
        {
            "id": "d6b93988-6cac-41c6-af48-6bb648f709fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd74c20-6793-4c53-8f24-58d13eee8104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1612,
    "xorig": 806,
    "yorig": 123
}