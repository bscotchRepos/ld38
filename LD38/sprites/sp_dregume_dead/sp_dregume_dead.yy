{
    "id": "0568ea62-5a27-42b3-9754-93dc3123bfd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dregume_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 524,
    "bbox_left": 0,
    "bbox_right": 516,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c9b39d8e-ae16-4245-909f-848342f18fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0568ea62-5a27-42b3-9754-93dc3123bfd9",
            "compositeImage": {
                "id": "a5447c54-62e9-4d85-8630-c17061843bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b39d8e-ae16-4245-909f-848342f18fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec911a9-7c6d-426f-a180-f9ca8f83cd73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b39d8e-ae16-4245-909f-848342f18fc1",
                    "LayerId": "3d39a3ab-f76c-4d9b-9406-c48325186d5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 525,
    "layers": [
        {
            "id": "3d39a3ab-f76c-4d9b-9406-c48325186d5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0568ea62-5a27-42b3-9754-93dc3123bfd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 517,
    "xorig": 258,
    "yorig": 262
}