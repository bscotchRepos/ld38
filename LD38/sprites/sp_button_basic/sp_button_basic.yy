{
    "id": "361c1f77-81a9-42d4-a10d-4ee5f0dec0f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_button_basic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 504,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ac87c96c-ed7d-48cf-bc09-ace806aa4d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "361c1f77-81a9-42d4-a10d-4ee5f0dec0f9",
            "compositeImage": {
                "id": "6a7300c6-51cc-4a76-bcf5-fcd62307dc8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac87c96c-ed7d-48cf-bc09-ace806aa4d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869b8a28-b88f-4a09-b08b-706af4c5bec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac87c96c-ed7d-48cf-bc09-ace806aa4d3a",
                    "LayerId": "ce65e2eb-d895-45d9-a9ff-1378f38ee389"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "ce65e2eb-d895-45d9-a9ff-1378f38ee389",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "361c1f77-81a9-42d4-a10d-4ee5f0dec0f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 505,
    "xorig": 252,
    "yorig": 52
}