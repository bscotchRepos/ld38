{
		"name": "sp_dirb1_blink",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 123,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 47,
		"swatchColours": null,
		"id": "a790a87f-6256-9abc-35b3-27100469bf88",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "a790a87f-6256-9abc-35b3-27100469bf88",
						"id": "c049db31-27b1-f15a-20fd-ba3656d86df9"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "896f5e8d-0b22-11c4-31c1-ed211c499e17",
								"id": "239ad240-a45d-50bd-aa13-3157f3e20d5c"
						},
						"id": "896f5e8d-0b22-11c4-31c1-ed211c499e17",
						"modelName": "GMSpriteFrame",
						"SpriteId": "a790a87f-6256-9abc-35b3-27100469bf88",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "c049db31-27b1-f15a-20fd-ba3656d86df9",
										"FrameId": "896f5e8d-0b22-11c4-31c1-ed211c499e17",
										"id": "8ef5a39a-b5c5-fdb2-6298-181f1e0dd9c2"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 122,
		"height": 95,
		"bbox_bottom": 94,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 61,
		"origin": 4,
		"coltolerance": 0
}