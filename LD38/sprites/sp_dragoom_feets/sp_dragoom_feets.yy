{
    "id": "07a93606-7f4a-3116-af9d-28251870663c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dragoom_feets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b3d18900-009d-8481-06ec-89cf2c5679f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a93606-7f4a-3116-af9d-28251870663c",
            "compositeImage": {
                "id": "d41623ee-fa7d-acda-9fe7-4968d27eb928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d18900-009d-8481-06ec-89cf2c5679f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054bcbaa-dd45-a3cc-c5b6-dd7f8b2ed3fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d18900-009d-8481-06ec-89cf2c5679f3",
                    "LayerId": "5f04f966-966c-d0f8-b036-10ed94cdbfca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "5f04f966-966c-d0f8-b036-10ed94cdbfca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a93606-7f4a-3116-af9d-28251870663c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 17,
    "xorig": 11,
    "yorig": 4
}