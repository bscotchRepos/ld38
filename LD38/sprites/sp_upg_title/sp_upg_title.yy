{
    "id": "d79053cc-fb3d-4753-9f70-0fbdb5146022",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_upg_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 0,
    "bbox_right": 1611,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9cd8fdf5-0de5-4f7e-8f3c-5ab070170b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d79053cc-fb3d-4753-9f70-0fbdb5146022",
            "compositeImage": {
                "id": "fcd1b285-d5b8-478f-bbdc-7083d8ba09c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd8fdf5-0de5-4f7e-8f3c-5ab070170b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146bb4a0-8347-4c6a-ba23-a168114ac9e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd8fdf5-0de5-4f7e-8f3c-5ab070170b22",
                    "LayerId": "3ed685ff-235a-421e-8a88-0d7b6f7a6fa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 246,
    "layers": [
        {
            "id": "3ed685ff-235a-421e-8a88-0d7b6f7a6fa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d79053cc-fb3d-4753-9f70-0fbdb5146022",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1612,
    "xorig": 806,
    "yorig": 123
}