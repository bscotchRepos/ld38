{
    "id": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_placeholder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5094ab0a-66af-4821-9df1-180047dd083c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
            "compositeImage": {
                "id": "bbbda90e-e78d-4f5d-a1c7-0f73c8f84e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5094ab0a-66af-4821-9df1-180047dd083c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee4b381-8c49-4499-90f4-fb0323553000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5094ab0a-66af-4821-9df1-180047dd083c",
                    "LayerId": "224147e8-f3f6-4019-bc8f-acb757c42d1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "224147e8-f3f6-4019-bc8f-acb757c42d1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}