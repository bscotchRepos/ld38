{
		"name": "sp_wing1_down",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 43,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 21,
		"swatchColours": null,
		"id": "8112e2db-7594-97fa-f4d0-ef131f572cc8",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "8112e2db-7594-97fa-f4d0-ef131f572cc8",
						"id": "18551568-53e3-9a83-7a24-8ad806dfa135"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "a39faa6e-9cd1-a673-24d7-da0dde7cd363",
								"id": "42ddbc3b-ac8e-5d44-578b-564738e1348b"
						},
						"id": "a39faa6e-9cd1-a673-24d7-da0dde7cd363",
						"modelName": "GMSpriteFrame",
						"SpriteId": "8112e2db-7594-97fa-f4d0-ef131f572cc8",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "18551568-53e3-9a83-7a24-8ad806dfa135",
										"FrameId": "a39faa6e-9cd1-a673-24d7-da0dde7cd363",
										"id": "1688f0a4-630a-23c9-5ef0-c56ac10efced"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 42,
		"height": 44,
		"bbox_bottom": 43,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 21,
		"origin": 4,
		"coltolerance": 0
}