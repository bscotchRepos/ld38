{
    "id": "9073e54e-57aa-06c0-dd3b-98e5ba9e80e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_malloom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "26f75270-b3ae-4841-8d67-5e62d16f19c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9073e54e-57aa-06c0-dd3b-98e5ba9e80e4",
            "compositeImage": {
                "id": "95bc3dc0-d9a1-4978-9a9a-a2c34a6db1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f75270-b3ae-4841-8d67-5e62d16f19c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60eb5df2-b573-4746-8dd6-a258004c4815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f75270-b3ae-4841-8d67-5e62d16f19c7",
                    "LayerId": "8a6e820c-1825-e868-3129-77d70d977e57"
                }
            ]
        },
        {
            "id": "aaaffa69-5a0f-432d-8841-8f038a367ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9073e54e-57aa-06c0-dd3b-98e5ba9e80e4",
            "compositeImage": {
                "id": "268eba88-0737-44fb-b6b5-59966e3a8b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaaffa69-5a0f-432d-8841-8f038a367ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bb840e-8e65-4029-ae15-7be5da55a967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaaffa69-5a0f-432d-8841-8f038a367ea8",
                    "LayerId": "8a6e820c-1825-e868-3129-77d70d977e57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "8a6e820c-1825-e868-3129-77d70d977e57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9073e54e-57aa-06c0-dd3b-98e5ba9e80e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 47,
    "xorig": 23,
    "yorig": 23
}