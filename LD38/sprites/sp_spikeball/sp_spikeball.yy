{
    "id": "050db3ec-1215-40e6-a412-233a995c854f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_spikeball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 344,
    "bbox_left": 0,
    "bbox_right": 337,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5c8bc430-8a10-439d-860d-ebe9daca93f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050db3ec-1215-40e6-a412-233a995c854f",
            "compositeImage": {
                "id": "7f7a409b-a041-424c-8cb3-f9a18ba9342d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c8bc430-8a10-439d-860d-ebe9daca93f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e58662f-5d21-4dc0-a68a-643d022299dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c8bc430-8a10-439d-860d-ebe9daca93f3",
                    "LayerId": "756e5d4f-223f-4812-85a0-906ee31a1214"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 345,
    "layers": [
        {
            "id": "756e5d4f-223f-4812-85a0-906ee31a1214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "050db3ec-1215-40e6-a412-233a995c854f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 338,
    "xorig": 169,
    "yorig": 172
}