{
    "id": "1691868d-8db5-480d-b96b-9b67952d03f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_instructions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1036,
    "bbox_left": 0,
    "bbox_right": 1562,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8649fd80-1816-4e05-b971-a2964465705f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1691868d-8db5-480d-b96b-9b67952d03f0",
            "compositeImage": {
                "id": "c08726fb-c671-4822-a503-7f79283a4fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8649fd80-1816-4e05-b971-a2964465705f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cefafc13-b37c-48ea-b13d-928319f172ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8649fd80-1816-4e05-b971-a2964465705f",
                    "LayerId": "a4902ff4-92ad-4e6a-b487-20d3c954123c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1037,
    "layers": [
        {
            "id": "a4902ff4-92ad-4e6a-b487-20d3c954123c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1691868d-8db5-480d-b96b-9b67952d03f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1563,
    "xorig": 781,
    "yorig": 518
}