{
    "id": "2ef418d7-f960-4104-b10e-4b574700c9a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_paused",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 194,
    "bbox_left": 0,
    "bbox_right": 551,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c140ab64-8cae-45a4-933c-ad35603d6fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ef418d7-f960-4104-b10e-4b574700c9a6",
            "compositeImage": {
                "id": "9aa7fde3-1761-44f1-a7d0-1dcdf276231a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c140ab64-8cae-45a4-933c-ad35603d6fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151734c6-8477-44a0-b644-1e7be296d9af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c140ab64-8cae-45a4-933c-ad35603d6fed",
                    "LayerId": "878f45a5-9177-4c4c-9fc9-8aba99841604"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 195,
    "layers": [
        {
            "id": "878f45a5-9177-4c4c-9fc9-8aba99841604",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ef418d7-f960-4104-b10e-4b574700c9a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 552,
    "xorig": 276,
    "yorig": 97
}