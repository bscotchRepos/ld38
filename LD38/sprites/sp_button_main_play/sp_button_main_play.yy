{
    "id": "cd7ee442-4e9e-431b-aff3-34a33d1dea7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_button_main_play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 317,
    "bbox_left": 0,
    "bbox_right": 413,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "34ba32d7-b0e9-4146-942a-4cfa6d9402a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd7ee442-4e9e-431b-aff3-34a33d1dea7f",
            "compositeImage": {
                "id": "a1d75d41-824a-4a7c-a179-c430befc1248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ba32d7-b0e9-4146-942a-4cfa6d9402a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0faf8b83-c675-45b9-8f0f-8fd95d6642d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ba32d7-b0e9-4146-942a-4cfa6d9402a0",
                    "LayerId": "e4309c6e-356c-48bd-a1fc-168cfd1c6bf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 318,
    "layers": [
        {
            "id": "e4309c6e-356c-48bd-a1fc-168cfd1c6bf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd7ee442-4e9e-431b-aff3-34a33d1dea7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 414,
    "xorig": 207,
    "yorig": 159
}