{
		"VTile": false,
		"swfPrecision": 2.525,
		"gridY": 0,
		"name": "sp_dragoom_earwings",
		"frames": [
				{
						"images": [
								{
										"LayerId": "00e23b4c-41bc-acc5-be21-ef64e881e194",
										"mvc": "1.0",
										"id": "389137a1-a6da-4e02-96ef-c93eae3f134f",
										"modelName": "GMSpriteImage",
										"FrameId": "a5701f4f-69af-6eb8-8269-4d0fa7fd8e18"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "a5701f4f-69af-6eb8-8269-4d0fa7fd8e18",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "a7c0f185-8bfc-05de-1e54-0c2e2ff8dd24",
								"modelName": "GMSpriteImage",
								"FrameId": "a5701f4f-69af-6eb8-8269-4d0fa7fd8e18"
						},
						"SpriteId": "408758f8-0f1c-38a9-13a1-028ac4ebf6a4"
				},
				{
						"images": [
								{
										"LayerId": "00e23b4c-41bc-acc5-be21-ef64e881e194",
										"mvc": "1.0",
										"id": "751a9ebe-9e13-f927-069d-587602ab8715",
										"modelName": "GMSpriteImage",
										"FrameId": "143500c5-f070-ecc9-55cc-cfd1a0d22c3a"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "143500c5-f070-ecc9-55cc-cfd1a0d22c3a",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "c95e28a0-b06e-1ce5-c51f-c77a2e109f47",
								"modelName": "GMSpriteImage",
								"FrameId": "143500c5-f070-ecc9-55cc-cfd1a0d22c3a"
						},
						"SpriteId": "408758f8-0f1c-38a9-13a1-028ac4ebf6a4"
				},
				{
						"images": [
								{
										"LayerId": "00e23b4c-41bc-acc5-be21-ef64e881e194",
										"mvc": "1.0",
										"id": "4b732552-2cfc-48e5-b508-c19eb1476f23",
										"modelName": "GMSpriteImage",
										"FrameId": "5bac578a-7280-9c4f-0d60-6ff46921b718"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "5bac578a-7280-9c4f-0d60-6ff46921b718",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "56e96be6-a3f8-3320-fe0e-af1381d5b91a",
								"modelName": "GMSpriteImage",
								"FrameId": "5bac578a-7280-9c4f-0d60-6ff46921b718"
						},
						"SpriteId": "408758f8-0f1c-38a9-13a1-028ac4ebf6a4"
				}
		],
		"id": "408758f8-0f1c-38a9-13a1-028ac4ebf6a4",
		"type": 0,
		"HTile": false,
		"bboxmode": 0,
		"bbox_bottom": 67,
		"modelName": "GMSprite",
		"gridX": 0,
		"playbackSpeedType": 0,
		"textureGroup": 0,
		"bbox_top": 0,
		"height": 68,
		"colkind": 1,
		"bbox_left": 0,
		"yorig": 42,
		"layers": [
				{
						"isLocked": false,
						"mvc": "1.0",
						"blendMode": 0,
						"name": "default",
						"opacity": 100,
						"modelName": "GMImageLayer",
						"id": "00e23b4c-41bc-acc5-be21-ef64e881e194",
						"visible": true,
						"SpriteId": "408758f8-0f1c-38a9-13a1-028ac4ebf6a4"
				}
		],
		"origin": 9,
		"xorig": 41,
		"coltolerance": 0,
		"mvc": "1.12",
		"bbox_right": 53,
		"playbackSpeed": 15,
		"width": 54,
		"sepmasks": false,
		"For3D": false,
		"swatchColours": null
}