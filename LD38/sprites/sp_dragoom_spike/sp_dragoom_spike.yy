{
		"name": "sp_dragoom_spike",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 390,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 183,
		"swatchColours": null,
		"id": "1633c530-98c5-b898-8636-16636a6d6651",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "1633c530-98c5-b898-8636-16636a6d6651",
						"id": "42b13aba-a175-8489-5601-7fa4c4363e5b"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "dd6c5dbc-9757-c8a9-5645-d7e8aef72fb5",
								"id": "a07ec7bc-584c-163a-29fc-25e87d0c1237"
						},
						"id": "dd6c5dbc-9757-c8a9-5645-d7e8aef72fb5",
						"modelName": "GMSpriteFrame",
						"SpriteId": "1633c530-98c5-b898-8636-16636a6d6651",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "42b13aba-a175-8489-5601-7fa4c4363e5b",
										"FrameId": "dd6c5dbc-9757-c8a9-5645-d7e8aef72fb5",
										"id": "9e8a6258-2326-0521-659d-0a05bf8751cf"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 389,
		"height": 369,
		"bbox_bottom": 368,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 194,
		"origin": 4,
		"coltolerance": 0
}