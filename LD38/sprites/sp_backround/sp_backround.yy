{
    "id": "8cb1ecfa-5aa2-4084-a94d-94da809ba1ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_backround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 3839,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3109058e-5ae4-4643-9742-b3a5d6bcbfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cb1ecfa-5aa2-4084-a94d-94da809ba1ea",
            "compositeImage": {
                "id": "466dc249-14f2-4c46-b9f7-c28d2b53b0bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3109058e-5ae4-4643-9742-b3a5d6bcbfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee98d644-803e-4fc8-9e28-f433e134ec61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3109058e-5ae4-4643-9742-b3a5d6bcbfe0",
                    "LayerId": "6a9ca288-baaf-4b05-8469-e88315089bcf"
                }
            ]
        },
        {
            "id": "43c70108-b404-4d6e-aeff-16568bf60b44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cb1ecfa-5aa2-4084-a94d-94da809ba1ea",
            "compositeImage": {
                "id": "c24709f6-3498-4a78-9fa9-0480bdca7513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c70108-b404-4d6e-aeff-16568bf60b44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e756694e-ea4e-4621-a851-00bbfdd65eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c70108-b404-4d6e-aeff-16568bf60b44",
                    "LayerId": "6a9ca288-baaf-4b05-8469-e88315089bcf"
                }
            ]
        },
        {
            "id": "139b261c-022d-4a63-8495-6341c6f7bcb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cb1ecfa-5aa2-4084-a94d-94da809ba1ea",
            "compositeImage": {
                "id": "aa6e5ad5-ba3b-47ad-81ab-251e2442f7da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "139b261c-022d-4a63-8495-6341c6f7bcb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a127b6-c416-4afd-9737-f7a87dedf24e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "139b261c-022d-4a63-8495-6341c6f7bcb4",
                    "LayerId": "6a9ca288-baaf-4b05-8469-e88315089bcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "6a9ca288-baaf-4b05-8469-e88315089bcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cb1ecfa-5aa2-4084-a94d-94da809ba1ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 3840,
    "xorig": 1920,
    "yorig": 540
}