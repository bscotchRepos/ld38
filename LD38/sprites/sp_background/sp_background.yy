{
    "id": "a3b1d990-b3df-4939-92c1-2084f9a52d39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_background",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 3839,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d414a79f-29cb-4b69-b2ce-daeac92065fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b1d990-b3df-4939-92c1-2084f9a52d39",
            "compositeImage": {
                "id": "bf599ca4-1e1c-4376-aba8-d8d9dc6c3d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d414a79f-29cb-4b69-b2ce-daeac92065fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787c6b5d-4033-4ad5-b22c-0018430f240e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d414a79f-29cb-4b69-b2ce-daeac92065fc",
                    "LayerId": "de0b2ac9-216f-46ba-b46d-a54ccfa5fab5"
                }
            ]
        },
        {
            "id": "5e68f902-7456-4df1-b20a-b45e84addb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b1d990-b3df-4939-92c1-2084f9a52d39",
            "compositeImage": {
                "id": "a7f47ec5-263e-4b5b-895c-2ce644edd54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e68f902-7456-4df1-b20a-b45e84addb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ea5ad8e-6760-4b83-a419-451dac9b47eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e68f902-7456-4df1-b20a-b45e84addb45",
                    "LayerId": "de0b2ac9-216f-46ba-b46d-a54ccfa5fab5"
                }
            ]
        },
        {
            "id": "62a9ae5c-ff94-4317-853a-8de80650681d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b1d990-b3df-4939-92c1-2084f9a52d39",
            "compositeImage": {
                "id": "8ab9072e-addd-4e91-bcb7-329f12101424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a9ae5c-ff94-4317-853a-8de80650681d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978e4d9b-e7e7-46ad-8533-4ce9c034e518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a9ae5c-ff94-4317-853a-8de80650681d",
                    "LayerId": "de0b2ac9-216f-46ba-b46d-a54ccfa5fab5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "de0b2ac9-216f-46ba-b46d-a54ccfa5fab5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3b1d990-b3df-4939-92c1-2084f9a52d39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 3840,
    "xorig": 3839,
    "yorig": 1079
}