{
    "id": "de87ca43-e09d-4c53-aaa0-87c5bdcd0669",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_megadirb_wing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0f41cf16-abd8-495f-80aa-02425e936f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de87ca43-e09d-4c53-aaa0-87c5bdcd0669",
            "compositeImage": {
                "id": "c0d38c8f-c11d-4ca8-9aaf-eacef83fbeda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f41cf16-abd8-495f-80aa-02425e936f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367f5717-0198-4f30-a955-600c7b3873c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f41cf16-abd8-495f-80aa-02425e936f2c",
                    "LayerId": "1f9c26e5-135e-4c99-98e7-3dd96a13714c"
                }
            ]
        },
        {
            "id": "331a38e8-d836-49ff-a310-0263b82bdd45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de87ca43-e09d-4c53-aaa0-87c5bdcd0669",
            "compositeImage": {
                "id": "dcd4153c-e5b2-4d3e-b452-28e08817e1af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "331a38e8-d836-49ff-a310-0263b82bdd45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1965b2d1-c7df-415c-9d16-0261d0ef94c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "331a38e8-d836-49ff-a310-0263b82bdd45",
                    "LayerId": "1f9c26e5-135e-4c99-98e7-3dd96a13714c"
                }
            ]
        },
        {
            "id": "66e0ae1a-f08e-4bf4-9237-a4819b64c5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de87ca43-e09d-4c53-aaa0-87c5bdcd0669",
            "compositeImage": {
                "id": "c8fb8cf8-46e4-4c28-bb53-bc1cb36a3219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e0ae1a-f08e-4bf4-9237-a4819b64c5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df42e0b0-c7a4-41cc-adfb-7227cc97355f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e0ae1a-f08e-4bf4-9237-a4819b64c5a7",
                    "LayerId": "1f9c26e5-135e-4c99-98e7-3dd96a13714c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "1f9c26e5-135e-4c99-98e7-3dd96a13714c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de87ca43-e09d-4c53-aaa0-87c5bdcd0669",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 38,
    "xorig": 31,
    "yorig": 27
}