{
    "id": "2d575e8a-b44a-4812-93cf-01295d016ba2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_balloom_strong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 80,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "82ed7fb2-765d-4cf4-8b7c-14c79ef3fc37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d575e8a-b44a-4812-93cf-01295d016ba2",
            "compositeImage": {
                "id": "39217798-8acd-46b0-bdfe-39f779d66f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ed7fb2-765d-4cf4-8b7c-14c79ef3fc37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa21a03a-bd9c-46e5-9039-9d4f6ea931cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ed7fb2-765d-4cf4-8b7c-14c79ef3fc37",
                    "LayerId": "8ea658c4-6f51-49ad-a99a-89d81151c24d"
                }
            ]
        },
        {
            "id": "cd5d18d8-0ace-4582-86d5-d19f9010e71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d575e8a-b44a-4812-93cf-01295d016ba2",
            "compositeImage": {
                "id": "0b01864f-fad7-46b3-b353-d3e5ac1492cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd5d18d8-0ace-4582-86d5-d19f9010e71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60e5326-8586-466e-b148-34fe1c83b723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd5d18d8-0ace-4582-86d5-d19f9010e71d",
                    "LayerId": "8ea658c4-6f51-49ad-a99a-89d81151c24d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "8ea658c4-6f51-49ad-a99a-89d81151c24d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d575e8a-b44a-4812-93cf-01295d016ba2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 81,
    "xorig": 50,
    "yorig": 52
}