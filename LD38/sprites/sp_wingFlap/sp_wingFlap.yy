{
		"name": "sp_wingFlap",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 33,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 27,
		"swatchColours": null,
		"id": "146a5f91-594a-864c-3852-01abf36b52f2",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "146a5f91-594a-864c-3852-01abf36b52f2",
						"id": "13a280b1-79ca-5a9c-45b7-c3a610863483"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "1e0545e3-e642-985c-3832-2513668d70cb",
								"id": "9f33837f-5020-f81e-e408-4b55831753fb"
						},
						"id": "1e0545e3-e642-985c-3832-2513668d70cb",
						"modelName": "GMSpriteFrame",
						"SpriteId": "146a5f91-594a-864c-3852-01abf36b52f2",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "13a280b1-79ca-5a9c-45b7-c3a610863483",
										"FrameId": "1e0545e3-e642-985c-3832-2513668d70cb",
										"id": "0c58fa9c-0075-e58f-99a4-47c9c1fb5ab6"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 32,
		"height": 56,
		"bbox_bottom": 55,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 15,
		"origin": 4,
		"coltolerance": 0
}