{
    "id": "3436ea7b-67fd-45e0-aa93-f60b5de21440",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dirb_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 0,
    "bbox_right": 94,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6342a1a8-4607-4eda-a53b-19cfc80005d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3436ea7b-67fd-45e0-aa93-f60b5de21440",
            "compositeImage": {
                "id": "292125c2-061e-4f15-b259-f68728bd83af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6342a1a8-4607-4eda-a53b-19cfc80005d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e323b4d4-c268-45d3-908e-eaced6a6e239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6342a1a8-4607-4eda-a53b-19cfc80005d6",
                    "LayerId": "dc59e59a-7a5d-418d-81cc-4c280425c512"
                }
            ]
        },
        {
            "id": "cc13cd2b-35e1-4a9a-af56-a28826685c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3436ea7b-67fd-45e0-aa93-f60b5de21440",
            "compositeImage": {
                "id": "2bb3c03a-d411-4571-9175-14d607f1f8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc13cd2b-35e1-4a9a-af56-a28826685c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb2f1fa-093e-45a9-9911-5659810eca56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc13cd2b-35e1-4a9a-af56-a28826685c81",
                    "LayerId": "dc59e59a-7a5d-418d-81cc-4c280425c512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 67,
    "layers": [
        {
            "id": "dc59e59a-7a5d-418d-81cc-4c280425c512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3436ea7b-67fd-45e0-aa93-f60b5de21440",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 95,
    "xorig": 44,
    "yorig": 38
}