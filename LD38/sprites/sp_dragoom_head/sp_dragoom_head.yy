{
    "id": "20220ddb-f498-423b-ffff-06d4ba2e959c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dragoom_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b618cc0b-456e-44c4-a288-25fc59e1f20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20220ddb-f498-423b-ffff-06d4ba2e959c",
            "compositeImage": {
                "id": "ebc340cf-0928-4623-9d30-96e09431e0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b618cc0b-456e-44c4-a288-25fc59e1f20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "809204ee-e0d2-4fcd-95d7-731ee0b1eb26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b618cc0b-456e-44c4-a288-25fc59e1f20a",
                    "LayerId": "a4120cd5-cf69-9847-f130-59de25b670cb"
                }
            ]
        },
        {
            "id": "a56386a4-4fed-47e6-9cdc-b25c7359221e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20220ddb-f498-423b-ffff-06d4ba2e959c",
            "compositeImage": {
                "id": "cba2512f-0850-4532-8017-17961f3e5542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56386a4-4fed-47e6-9cdc-b25c7359221e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831db1c3-b77e-4f01-a25c-8c87c43b4f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56386a4-4fed-47e6-9cdc-b25c7359221e",
                    "LayerId": "a4120cd5-cf69-9847-f130-59de25b670cb"
                }
            ]
        },
        {
            "id": "542d1cb9-8b65-41a9-99c9-1d234ff5adc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20220ddb-f498-423b-ffff-06d4ba2e959c",
            "compositeImage": {
                "id": "e442c6d5-3027-43ea-bf90-7c29424c6212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542d1cb9-8b65-41a9-99c9-1d234ff5adc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b602677a-2e4e-436f-8b4e-0ede7fa4b880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542d1cb9-8b65-41a9-99c9-1d234ff5adc1",
                    "LayerId": "a4120cd5-cf69-9847-f130-59de25b670cb"
                }
            ]
        },
        {
            "id": "f9446037-917b-4ade-bde0-25207fc881ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20220ddb-f498-423b-ffff-06d4ba2e959c",
            "compositeImage": {
                "id": "4f5889a2-69a1-4f64-bec0-2646edda5b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9446037-917b-4ade-bde0-25207fc881ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99aaba15-88a5-43b1-a5fb-cfb312c8486f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9446037-917b-4ade-bde0-25207fc881ca",
                    "LayerId": "a4120cd5-cf69-9847-f130-59de25b670cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "a4120cd5-cf69-9847-f130-59de25b670cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20220ddb-f498-423b-ffff-06d4ba2e959c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 74,
    "xorig": 27,
    "yorig": 47
}