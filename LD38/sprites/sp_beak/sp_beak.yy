{
    "id": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_beak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "232df06a-37a9-4269-a016-20b633e26ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
            "compositeImage": {
                "id": "d983c1fd-1225-4b1f-9c78-f661cbe3a82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "232df06a-37a9-4269-a016-20b633e26ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e55d98ed-f703-4774-a44f-bcae2e3677a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "232df06a-37a9-4269-a016-20b633e26ec3",
                    "LayerId": "07cbe27d-9fdd-70dd-8582-4d5de642021b"
                }
            ]
        },
        {
            "id": "3147e9a7-27d5-4dea-ba76-b7bf1c4b4c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
            "compositeImage": {
                "id": "d50fdab8-2a9a-4e48-9836-22831f02d97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3147e9a7-27d5-4dea-ba76-b7bf1c4b4c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8d48249-c7e6-476e-93ef-36a8901d7fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3147e9a7-27d5-4dea-ba76-b7bf1c4b4c34",
                    "LayerId": "07cbe27d-9fdd-70dd-8582-4d5de642021b"
                }
            ]
        },
        {
            "id": "fc8f7ae6-7fdc-43fc-9740-e8aad3a6acc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
            "compositeImage": {
                "id": "b43634b2-e697-4d4d-bc60-25527e5a9659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8f7ae6-7fdc-43fc-9740-e8aad3a6acc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff3a72d-0ccf-48aa-acec-2ebd2654b6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8f7ae6-7fdc-43fc-9740-e8aad3a6acc5",
                    "LayerId": "07cbe27d-9fdd-70dd-8582-4d5de642021b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "07cbe27d-9fdd-70dd-8582-4d5de642021b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 35,
    "yorig": 29
}