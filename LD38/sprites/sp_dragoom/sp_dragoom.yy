{
		"VTile": false,
		"swfPrecision": 2.525,
		"gridY": 0,
		"name": "sp_dragoom",
		"frames": [
				{
						"images": [
								{
										"LayerId": "3076cca8-e6fd-b743-5d71-a21feba73b94",
										"mvc": "1.0",
										"id": "f430dfd9-6d27-45f2-969c-af3088a1662e",
										"modelName": "GMSpriteImage",
										"FrameId": "192502e3-d757-1051-cb59-fe454c57780c"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "192502e3-d757-1051-cb59-fe454c57780c",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "9ffea5a0-6b3d-9eca-4ec2-58a3df20728f",
								"modelName": "GMSpriteImage",
								"FrameId": "192502e3-d757-1051-cb59-fe454c57780c"
						},
						"SpriteId": "a1fe3011-c33b-32a4-61dd-3172eea89bec"
				}
		],
		"id": "a1fe3011-c33b-32a4-61dd-3172eea89bec",
		"type": 0,
		"HTile": false,
		"bboxmode": 0,
		"bbox_bottom": 91,
		"modelName": "GMSprite",
		"gridX": 0,
		"playbackSpeedType": 0,
		"textureGroup": 0,
		"bbox_top": 0,
		"height": 92,
		"colkind": 1,
		"bbox_left": 0,
		"yorig": 48,
		"layers": [
				{
						"isLocked": false,
						"mvc": "1.0",
						"blendMode": 0,
						"name": "default",
						"opacity": 100,
						"modelName": "GMImageLayer",
						"id": "3076cca8-e6fd-b743-5d71-a21feba73b94",
						"visible": true,
						"SpriteId": "a1fe3011-c33b-32a4-61dd-3172eea89bec"
				}
		],
		"origin": 9,
		"xorig": 61,
		"coltolerance": 0,
		"mvc": "1.12",
		"bbox_right": 119,
		"playbackSpeed": 15,
		"width": 120,
		"sepmasks": false,
		"For3D": false,
		"swatchColours": null
}