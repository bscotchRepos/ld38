{
		"name": "sp_wing2_up",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 38,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 17,
		"swatchColours": null,
		"id": "372ff0b6-32d1-489b-3ba6-982b23b0e88f",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "372ff0b6-32d1-489b-3ba6-982b23b0e88f",
						"id": "e669c2d9-2456-1375-71f1-b0870eda0064"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "837c8f62-6538-3fe3-92ab-54fddd33201e",
								"id": "5d291f67-7196-7e37-6a3e-39c0d630de83"
						},
						"id": "837c8f62-6538-3fe3-92ab-54fddd33201e",
						"modelName": "GMSpriteFrame",
						"SpriteId": "372ff0b6-32d1-489b-3ba6-982b23b0e88f",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "e669c2d9-2456-1375-71f1-b0870eda0064",
										"FrameId": "837c8f62-6538-3fe3-92ab-54fddd33201e",
										"id": "ddb77c68-4b0c-14f0-db9a-afd1b25b7f6b"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 37,
		"height": 35,
		"bbox_bottom": 34,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 18,
		"origin": 4,
		"coltolerance": 0
}