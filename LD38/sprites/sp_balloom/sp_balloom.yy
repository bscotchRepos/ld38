{
    "id": "a8963e9c-787e-4585-9901-3ff489b7219a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_balloom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d1205d3e-0fa0-4fe2-abe2-06309dedac29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8963e9c-787e-4585-9901-3ff489b7219a",
            "compositeImage": {
                "id": "7d7717f2-76f2-4d9e-b4f9-47fc4dbb39d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1205d3e-0fa0-4fe2-abe2-06309dedac29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af7bdcb1-4b67-446b-aea3-1289107d66ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1205d3e-0fa0-4fe2-abe2-06309dedac29",
                    "LayerId": "f19d869a-d3c2-47af-b8b3-a63980a6caab"
                }
            ]
        },
        {
            "id": "aadb84b1-463b-444e-a63c-5e94ec869e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8963e9c-787e-4585-9901-3ff489b7219a",
            "compositeImage": {
                "id": "a0930fbc-5e6f-4bc8-a2b7-b4fbeb8d1ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aadb84b1-463b-444e-a63c-5e94ec869e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3e2d0c-f5a9-4640-bf3c-e209c0ea8da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aadb84b1-463b-444e-a63c-5e94ec869e44",
                    "LayerId": "f19d869a-d3c2-47af-b8b3-a63980a6caab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "f19d869a-d3c2-47af-b8b3-a63980a6caab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8963e9c-787e-4585-9901-3ff489b7219a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 50
}