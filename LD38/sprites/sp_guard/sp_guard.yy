{
    "id": "ebf0c741-4933-4c4d-bdf2-553738f42a7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_guard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0f5c4216-9488-4b57-b210-1bafa65630df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf0c741-4933-4c4d-bdf2-553738f42a7a",
            "compositeImage": {
                "id": "3238be79-42ca-4463-a58a-7f00cf24e441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5c4216-9488-4b57-b210-1bafa65630df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e99ec1bf-8996-493d-803f-6f5008c51750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5c4216-9488-4b57-b210-1bafa65630df",
                    "LayerId": "e750bad7-a1bd-4a6d-b2c1-95ddcced285c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e750bad7-a1bd-4a6d-b2c1-95ddcced285c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebf0c741-4933-4c4d-bdf2-553738f42a7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 29
}