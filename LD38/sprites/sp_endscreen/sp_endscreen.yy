{
    "id": "5ec76ba0-a8fe-4d72-ad7a-0866ab6f8eff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_endscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 0,
    "bbox_right": 1613,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "433db4f7-8308-4960-8c5a-f60248e9d4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec76ba0-a8fe-4d72-ad7a-0866ab6f8eff",
            "compositeImage": {
                "id": "8c486ebe-5d06-4780-af22-f7b83fee7257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433db4f7-8308-4960-8c5a-f60248e9d4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef4278a-5f0c-47af-bb14-bd01b3f65b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433db4f7-8308-4960-8c5a-f60248e9d4f3",
                    "LayerId": "00e94c95-5bc1-452d-8a09-c7559f3c71e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "00e94c95-5bc1-452d-8a09-c7559f3c71e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ec76ba0-a8fe-4d72-ad7a-0866ab6f8eff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1614,
    "xorig": 807,
    "yorig": 73
}