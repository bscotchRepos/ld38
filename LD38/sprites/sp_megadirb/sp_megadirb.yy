{
    "id": "0e3df025-f152-4b79-b562-053c9af34b2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_megadirb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 102,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "59de6d4a-aab3-431e-889b-7f127f0a868f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e3df025-f152-4b79-b562-053c9af34b2f",
            "compositeImage": {
                "id": "736d1897-d13e-4a35-8c9b-52a567ab40a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59de6d4a-aab3-431e-889b-7f127f0a868f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115470c2-55f0-4295-b161-3d9de602e5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59de6d4a-aab3-431e-889b-7f127f0a868f",
                    "LayerId": "05bd66cd-b7cb-4b2b-b46c-60e921748dee"
                }
            ]
        },
        {
            "id": "06bc4e92-5fbd-47c0-892f-67bd4c9b7807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e3df025-f152-4b79-b562-053c9af34b2f",
            "compositeImage": {
                "id": "b412f026-91a4-4070-8446-c5ac5a676e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06bc4e92-5fbd-47c0-892f-67bd4c9b7807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "120b3eef-988c-40d6-ae9b-90ccb66806b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06bc4e92-5fbd-47c0-892f-67bd4c9b7807",
                    "LayerId": "05bd66cd-b7cb-4b2b-b46c-60e921748dee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "05bd66cd-b7cb-4b2b-b46c-60e921748dee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e3df025-f152-4b79-b562-053c9af34b2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 103,
    "xorig": 51,
    "yorig": 37
}