{
    "id": "c8a9955e-710d-4122-9a7d-ecb5a33316fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_main_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 1879,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ace9bdf4-1fbf-40a1-b180-6570da023f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8a9955e-710d-4122-9a7d-ecb5a33316fb",
            "compositeImage": {
                "id": "2145f428-e2e0-47f4-b45f-de04135e6ddd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace9bdf4-1fbf-40a1-b180-6570da023f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1c859b3-8215-4c4e-b0c9-3de12b5ca8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace9bdf4-1fbf-40a1-b180-6570da023f34",
                    "LayerId": "59ef5e55-f0fb-4218-b112-3d8a427391b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "59ef5e55-f0fb-4218-b112-3d8a427391b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8a9955e-710d-4122-9a7d-ecb5a33316fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1880,
    "xorig": 940,
    "yorig": 200
}