{
    "id": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_cloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 398,
    "bbox_left": 0,
    "bbox_right": 1256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8471ca68-a64d-49b1-9f96-158240e3239f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
            "compositeImage": {
                "id": "e3830877-04bb-4b21-9f4b-075b0224ec73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8471ca68-a64d-49b1-9f96-158240e3239f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98a2505-1a86-4cd2-a94b-0e3dd10bc6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8471ca68-a64d-49b1-9f96-158240e3239f",
                    "LayerId": "03e12842-b4b2-4180-838e-04124657d89d"
                }
            ]
        },
        {
            "id": "37dafc38-335c-4ee7-b60f-473347b03421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
            "compositeImage": {
                "id": "69ce4013-b9f6-4c0f-8853-d048b8fab849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37dafc38-335c-4ee7-b60f-473347b03421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1285301-9a14-41be-a00e-5498aba46b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37dafc38-335c-4ee7-b60f-473347b03421",
                    "LayerId": "03e12842-b4b2-4180-838e-04124657d89d"
                }
            ]
        },
        {
            "id": "60d5b6bc-fecd-4562-a80d-ce76e7e75175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
            "compositeImage": {
                "id": "ca75f29e-2196-4d68-83b8-0d65e5d3f5ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d5b6bc-fecd-4562-a80d-ce76e7e75175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125d6636-30d2-4aea-ad41-532a3e0c5470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d5b6bc-fecd-4562-a80d-ce76e7e75175",
                    "LayerId": "03e12842-b4b2-4180-838e-04124657d89d"
                }
            ]
        },
        {
            "id": "7636c663-4693-4e7b-b8bd-d909da5e2ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
            "compositeImage": {
                "id": "715a1933-bcfd-4473-b3f5-4797626cff0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7636c663-4693-4e7b-b8bd-d909da5e2ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4980f86d-d4bf-4f79-be09-5fd813ffc1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7636c663-4693-4e7b-b8bd-d909da5e2ff7",
                    "LayerId": "03e12842-b4b2-4180-838e-04124657d89d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 399,
    "layers": [
        {
            "id": "03e12842-b4b2-4180-838e-04124657d89d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1257,
    "xorig": 1256,
    "yorig": 398
}