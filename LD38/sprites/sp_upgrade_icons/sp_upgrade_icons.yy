{
    "id": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_upgrade_icons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 130,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5080ae7b-0933-413a-8e14-9f0a91626379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "1f2b8c5d-f21a-423b-b1bb-8930019acb13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5080ae7b-0933-413a-8e14-9f0a91626379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1444f6cc-8fa1-4e68-a9b5-e4cd4fb767e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5080ae7b-0933-413a-8e14-9f0a91626379",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "774ba299-bc1a-4032-9b96-a8e40a669ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "a9c6e2b6-59ed-478e-abf2-9a0403ad66c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774ba299-bc1a-4032-9b96-a8e40a669ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33ba22af-c189-4de4-8b88-77b5af989389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774ba299-bc1a-4032-9b96-a8e40a669ecc",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "f674f9b8-bf60-48cb-99cf-2cf110687b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "1cfa52e4-eaac-41c5-863f-d6577a5002da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f674f9b8-bf60-48cb-99cf-2cf110687b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659483af-611d-4ee9-a045-228a251bb971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f674f9b8-bf60-48cb-99cf-2cf110687b24",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "835fee69-875b-4bac-b782-171a28540cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "aa8eb7b2-4f6c-4d66-92c3-764aa76e6810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835fee69-875b-4bac-b782-171a28540cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b8aec59-ac5a-4a00-bf95-f1fef461db5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835fee69-875b-4bac-b782-171a28540cf7",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "6f7b1442-d087-4db4-bcdd-7a2fe534dffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "d979b8a3-82b7-4b93-8be9-255cc4792c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7b1442-d087-4db4-bcdd-7a2fe534dffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6958485e-b46d-4a15-aa38-e75ed003c5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7b1442-d087-4db4-bcdd-7a2fe534dffd",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "08cd8bea-5a5c-4695-bcb9-8c418ccd0021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "235e92de-c8b0-4796-ba60-eb25845cf3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08cd8bea-5a5c-4695-bcb9-8c418ccd0021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660b0f34-d96e-48a7-8703-7aaa8a3b08fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08cd8bea-5a5c-4695-bcb9-8c418ccd0021",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "4387e43e-704a-422c-b197-3332fe120b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "86c62761-c48f-48f0-8338-3b2494c5b7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4387e43e-704a-422c-b197-3332fe120b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c00f6b04-5c6b-4f32-99ee-6fa9c90a0027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4387e43e-704a-422c-b197-3332fe120b3a",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "9646f1b2-1204-43ff-b02d-5dd8bad14b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "3e2d2511-ceaa-4af7-ad0f-d10d45a58830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9646f1b2-1204-43ff-b02d-5dd8bad14b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6a05d4-afc7-469c-8c9a-a690cda448df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9646f1b2-1204-43ff-b02d-5dd8bad14b59",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        },
        {
            "id": "f8d4d284-156a-41d2-875d-988e36122f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "compositeImage": {
                "id": "c97f21f3-ab4c-4d9d-8223-3ae16a8bbe47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d4d284-156a-41d2-875d-988e36122f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a388393-7179-4699-a2eb-09e6c984521a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d4d284-156a-41d2-875d-988e36122f73",
                    "LayerId": "1063969d-c42f-46b5-99b6-f66fff8a9f38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "1063969d-c42f-46b5-99b6-f66fff8a9f38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6c5fc76-8505-430d-8a7c-8ac6bc599531",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 65
}