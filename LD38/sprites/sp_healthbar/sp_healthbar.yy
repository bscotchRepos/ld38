{
    "id": "a0f71be0-4ed7-4411-9633-867a2a0d71a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_healthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 0,
    "bbox_right": 1088,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f3fffa11-0f6c-4f37-9abc-f19ea9214f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0f71be0-4ed7-4411-9633-867a2a0d71a8",
            "compositeImage": {
                "id": "c3222317-a6d2-482a-9129-bf32c604ed35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3fffa11-0f6c-4f37-9abc-f19ea9214f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f70f15-5c2e-4d91-b3b9-3facd1a599c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3fffa11-0f6c-4f37-9abc-f19ea9214f9e",
                    "LayerId": "5396d237-3ec1-4d53-bccb-cffa89f031ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 143,
    "layers": [
        {
            "id": "5396d237-3ec1-4d53-bccb-cffa89f031ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0f71be0-4ed7-4411-9633-867a2a0d71a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1089,
    "xorig": 544,
    "yorig": 71
}