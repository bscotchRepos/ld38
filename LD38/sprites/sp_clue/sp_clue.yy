{
    "id": "898e09a5-0650-4bd7-9093-d7febc2760ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_clue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1036,
    "bbox_left": 0,
    "bbox_right": 1562,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2cc52ce7-655a-45a2-b861-7cab19617880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "898e09a5-0650-4bd7-9093-d7febc2760ca",
            "compositeImage": {
                "id": "62ed81a4-254e-4511-abf0-57796bc01f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cc52ce7-655a-45a2-b861-7cab19617880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b48d054-c533-469d-b702-cf1727315ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cc52ce7-655a-45a2-b861-7cab19617880",
                    "LayerId": "ef385634-d51e-41d3-bb24-a0bccf5510d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1037,
    "layers": [
        {
            "id": "ef385634-d51e-41d3-bb24-a0bccf5510d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "898e09a5-0650-4bd7-9093-d7febc2760ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1563,
    "xorig": 728,
    "yorig": 513
}