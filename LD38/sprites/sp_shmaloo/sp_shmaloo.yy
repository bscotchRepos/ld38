{
    "id": "4db65503-e508-4afc-a03a-09ee2df6bc6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_shmaloo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1097078f-0b9f-4a8d-8a5a-f8f722b9d6cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db65503-e508-4afc-a03a-09ee2df6bc6c",
            "compositeImage": {
                "id": "7b86158f-b3b7-4ced-b1e8-a0c14ba2cbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1097078f-0b9f-4a8d-8a5a-f8f722b9d6cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5bd8b7-8b24-4d92-9ecd-458524d71ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1097078f-0b9f-4a8d-8a5a-f8f722b9d6cd",
                    "LayerId": "3616eb6e-9709-4a7f-9161-45c3695cd8de"
                }
            ]
        },
        {
            "id": "1aff7fb0-99c3-479c-914b-615f0c490c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db65503-e508-4afc-a03a-09ee2df6bc6c",
            "compositeImage": {
                "id": "7e043684-60a9-4ca6-b5c4-65b973e95066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aff7fb0-99c3-479c-914b-615f0c490c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c20a7d5-7c5f-478c-bdad-50c978a25ad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aff7fb0-99c3-479c-914b-615f0c490c70",
                    "LayerId": "3616eb6e-9709-4a7f-9161-45c3695cd8de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "3616eb6e-9709-4a7f-9161-45c3695cd8de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4db65503-e508-4afc-a03a-09ee2df6bc6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 27
}