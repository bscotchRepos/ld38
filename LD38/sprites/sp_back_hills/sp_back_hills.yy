{
		"name": "sp_back_hills",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 3840,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 539,
		"swatchColours": null,
		"id": "2ce31de0-38db-76b2-09ac-41c6a735c47c",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "2ce31de0-38db-76b2-09ac-41c6a735c47c",
						"id": "5f130e50-1b5d-eea4-f6cf-d577bdca70e7"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "31419b78-206f-dcaa-2f27-f5c02d5c6dad",
								"id": "a31f1e8a-2fcb-f6e1-7cff-5832a08d9f11"
						},
						"id": "31419b78-206f-dcaa-2f27-f5c02d5c6dad",
						"modelName": "GMSpriteFrame",
						"SpriteId": "2ce31de0-38db-76b2-09ac-41c6a735c47c",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "5f130e50-1b5d-eea4-f6cf-d577bdca70e7",
										"FrameId": "31419b78-206f-dcaa-2f27-f5c02d5c6dad",
										"id": "9e554e80-2264-0e87-6141-74007b3dfa64"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 3839,
		"height": 1080,
		"bbox_bottom": 1079,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 1919,
		"origin": 4,
		"coltolerance": 0
}