{
    "id": "08a16725-6c62-ca34-3950-281e379e1e38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dirb_basic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "de626b8d-f429-4bb9-8d98-8ea71d12a9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08a16725-6c62-ca34-3950-281e379e1e38",
            "compositeImage": {
                "id": "ab7461a0-be06-4ed2-a08c-beb208862597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de626b8d-f429-4bb9-8d98-8ea71d12a9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844a5d7e-615a-4820-a974-0c43f8194d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de626b8d-f429-4bb9-8d98-8ea71d12a9c8",
                    "LayerId": "ceb322bb-2cef-1e2e-a5fc-1c2d5f6c6ded"
                }
            ]
        },
        {
            "id": "4069aa1d-3513-458f-a30d-dce7c3c1c809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08a16725-6c62-ca34-3950-281e379e1e38",
            "compositeImage": {
                "id": "ee1eef99-9b8e-43fd-8f19-ca289c626724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4069aa1d-3513-458f-a30d-dce7c3c1c809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ec654f-3c11-4722-a40c-73e376bd6fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4069aa1d-3513-458f-a30d-dce7c3c1c809",
                    "LayerId": "ceb322bb-2cef-1e2e-a5fc-1c2d5f6c6ded"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 81,
    "layers": [
        {
            "id": "ceb322bb-2cef-1e2e-a5fc-1c2d5f6c6ded",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08a16725-6c62-ca34-3950-281e379e1e38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 112,
    "xorig": 55,
    "yorig": 47
}