{
    "id": "264ff3f6-a027-46a8-9175-3dc338d6d3e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_upgrade_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 1443,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2c17f82d-2f48-43b5-924c-3998a47b69eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "264ff3f6-a027-46a8-9175-3dc338d6d3e5",
            "compositeImage": {
                "id": "4fbe7c5d-e273-48cf-a6ff-8c4ec3fb8f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c17f82d-2f48-43b5-924c-3998a47b69eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "285dd2bc-c247-4973-ba12-65f618f5f864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c17f82d-2f48-43b5-924c-3998a47b69eb",
                    "LayerId": "7cafd3da-b19d-4fd0-b1a4-858b5bcaef4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "7cafd3da-b19d-4fd0-b1a4-858b5bcaef4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "264ff3f6-a027-46a8-9175-3dc338d6d3e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1444,
    "xorig": 722,
    "yorig": 85
}