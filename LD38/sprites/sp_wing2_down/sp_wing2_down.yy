{
		"name": "sp_wing2_down",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 38,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 17,
		"swatchColours": null,
		"id": "413f5802-59c4-d440-b571-6a6d8385baa2",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "413f5802-59c4-d440-b571-6a6d8385baa2",
						"id": "e77086f9-25e8-6d04-5414-a3a26b367085"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "7497749d-048a-d6bc-5f20-fa124042126c",
								"id": "ce044efb-bc56-6c39-b1b0-5b93317bacc1"
						},
						"id": "7497749d-048a-d6bc-5f20-fa124042126c",
						"modelName": "GMSpriteFrame",
						"SpriteId": "413f5802-59c4-d440-b571-6a6d8385baa2",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "e77086f9-25e8-6d04-5414-a3a26b367085",
										"FrameId": "7497749d-048a-d6bc-5f20-fa124042126c",
										"id": "3e06d985-dda2-e875-d6fa-88139086b1ef"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 37,
		"height": 35,
		"bbox_bottom": 34,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 18,
		"origin": 4,
		"coltolerance": 0
}