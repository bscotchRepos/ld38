{
    "id": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_stun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 97,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "65895af0-3913-4050-bcd7-9e1dad91ec2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
            "compositeImage": {
                "id": "d8c23de6-c7c6-4922-b9a2-fcebc7a54e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65895af0-3913-4050-bcd7-9e1dad91ec2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46504167-f2b2-4e97-9355-8920669c9a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65895af0-3913-4050-bcd7-9e1dad91ec2e",
                    "LayerId": "8de44f8f-1a2d-4568-8f86-57dd8249f5ac"
                }
            ]
        },
        {
            "id": "2b8bfcd4-2713-4d3d-9d36-47393f10dcfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
            "compositeImage": {
                "id": "1eae152d-276a-45a6-9dab-bd662d5ecb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8bfcd4-2713-4d3d-9d36-47393f10dcfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10967f49-03c5-459a-b2ef-369b8c88121d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8bfcd4-2713-4d3d-9d36-47393f10dcfc",
                    "LayerId": "8de44f8f-1a2d-4568-8f86-57dd8249f5ac"
                }
            ]
        },
        {
            "id": "8ce79e29-da2b-42cb-aff8-ac6a0b351650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
            "compositeImage": {
                "id": "1cf4ddb4-2a8f-4920-82c0-bd9e6791d171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce79e29-da2b-42cb-aff8-ac6a0b351650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17fb30c4-0486-419c-a1d0-83a5e7f7d05c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce79e29-da2b-42cb-aff8-ac6a0b351650",
                    "LayerId": "8de44f8f-1a2d-4568-8f86-57dd8249f5ac"
                }
            ]
        },
        {
            "id": "3b19bfb4-ed32-4bb0-afb5-927aeb2e03cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
            "compositeImage": {
                "id": "e9cbc3c8-92ec-4b36-a334-dc1fbe61e28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b19bfb4-ed32-4bb0-afb5-927aeb2e03cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6345da22-87a9-4863-bfc1-8987a7f83a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b19bfb4-ed32-4bb0-afb5-927aeb2e03cf",
                    "LayerId": "8de44f8f-1a2d-4568-8f86-57dd8249f5ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "8de44f8f-1a2d-4568-8f86-57dd8249f5ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a9f4647-45f7-4e53-8a1f-6af69b4116e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 98,
    "xorig": 0,
    "yorig": 0
}