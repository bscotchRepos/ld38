{
		"name": "sp_wing1_up",
		"type": 0,
		"bbox_top": 0,
		"For3D": false,
		"playbackSpeedType": 0,
		"bbox_left": 0,
		"playbackSpeed": 15,
		"gridX": 0,
		"width": 43,
		"colkind": 1,
		"bboxmode": 0,
		"gridY": 0,
		"yorig": 21,
		"swatchColours": null,
		"id": "897fa21d-2f0d-ba4d-70b8-c7e4711b1ee3",
		"swfPrecision": 2.525,
		"layers": [
				{
						"mvc": "1.0",
						"name": "default",
						"blendMode": 0,
						"isLocked": false,
						"visible": true,
						"opacity": 100,
						"modelName": "GMImageLayer",
						"SpriteId": "897fa21d-2f0d-ba4d-70b8-c7e4711b1ee3",
						"id": "043f1e82-9003-3499-0dea-fa075aeb86dd"
				}
		],
		"frames": [
				{
						"mvc": "1.0",
						"compositeImage": {
								"mvc": "1.0",
								"modelName": "GMSpriteImage",
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"FrameId": "18f0adc7-c77a-d254-a1f5-0be5190766ec",
								"id": "e0fd02b7-6010-5bb6-fdf0-ec0499f1ea68"
						},
						"id": "18f0adc7-c77a-d254-a1f5-0be5190766ec",
						"modelName": "GMSpriteFrame",
						"SpriteId": "897fa21d-2f0d-ba4d-70b8-c7e4711b1ee3",
						"images": [
								{
										"mvc": "1.0",
										"modelName": "GMSpriteImage",
										"LayerId": "043f1e82-9003-3499-0dea-fa075aeb86dd",
										"FrameId": "18f0adc7-c77a-d254-a1f5-0be5190766ec",
										"id": "bdeb75db-4b86-95df-d95f-4458f192c5ea"
								}
						]
				}
		],
		"modelName": "GMSprite",
		"bbox_right": 42,
		"height": 44,
		"bbox_bottom": 43,
		"mvc": "1.12",
		"HTile": false,
		"textureGroup": 0,
		"sepmasks": false,
		"VTile": false,
		"xorig": 21,
		"origin": 4,
		"coltolerance": 0
}