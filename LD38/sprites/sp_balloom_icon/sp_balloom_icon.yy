{
    "id": "bf19537d-86aa-4b56-8fd1-39872ff77eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_balloom_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "13903ea8-aafe-4083-b7c3-c2b40fc8d1a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf19537d-86aa-4b56-8fd1-39872ff77eea",
            "compositeImage": {
                "id": "a788d3ae-4f64-48cb-acb9-a36c195840c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13903ea8-aafe-4083-b7c3-c2b40fc8d1a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d1125e-a2db-4342-9aa4-8512958fadec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13903ea8-aafe-4083-b7c3-c2b40fc8d1a6",
                    "LayerId": "7c3f4097-0e4e-4026-8389-6aa5e51eb5f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 111,
    "layers": [
        {
            "id": "7c3f4097-0e4e-4026-8389-6aa5e51eb5f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf19537d-86aa-4b56-8fd1-39872ff77eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 105,
    "xorig": 52,
    "yorig": 55
}