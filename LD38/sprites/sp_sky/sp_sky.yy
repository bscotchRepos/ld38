{
    "id": "bbc33e95-8c53-4397-bce7-949576d59046",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_sky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b6618abc-0f14-42f9-bba2-bd8ac06b806f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc33e95-8c53-4397-bce7-949576d59046",
            "compositeImage": {
                "id": "e113de17-e82b-4646-8962-04f483f0aa7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6618abc-0f14-42f9-bba2-bd8ac06b806f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6401d67-2b16-445c-b59c-4b8063ebbacb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6618abc-0f14-42f9-bba2-bd8ac06b806f",
                    "LayerId": "19bd4a8c-4d63-499c-8e11-c35ca750405c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "19bd4a8c-4d63-499c-8e11-c35ca750405c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc33e95-8c53-4397-bce7-949576d59046",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}