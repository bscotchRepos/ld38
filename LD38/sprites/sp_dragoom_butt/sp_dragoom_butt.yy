{
    "id": "c684f17b-29ef-d458-7b0a-0cdf6a35592d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dragoom_butt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b5dd0653-aa61-2378-bca0-423520d3c3d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c684f17b-29ef-d458-7b0a-0cdf6a35592d",
            "compositeImage": {
                "id": "0b7bcd95-410f-dd32-09ce-e6f84c903390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5dd0653-aa61-2378-bca0-423520d3c3d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccabe12f-52fe-07c0-2ec8-fe8c87b9d0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5dd0653-aa61-2378-bca0-423520d3c3d3",
                    "LayerId": "c307c42e-9745-6888-19ef-8caeeff92e83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "c307c42e-9745-6888-19ef-8caeeff92e83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c684f17b-29ef-d458-7b0a-0cdf6a35592d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 16
}