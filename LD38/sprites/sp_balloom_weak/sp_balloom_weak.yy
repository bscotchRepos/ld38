{
    "id": "4ea37b9b-86cd-4e7b-8d74-aad2b210af97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_balloom_weak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f056cab2-47f4-4d11-ab7a-d0f5383aa676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ea37b9b-86cd-4e7b-8d74-aad2b210af97",
            "compositeImage": {
                "id": "12402087-1cb1-4434-b3ed-6f113e4974ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f056cab2-47f4-4d11-ab7a-d0f5383aa676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b71dac1-c8d7-4d16-b577-6d0f3dd22626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f056cab2-47f4-4d11-ab7a-d0f5383aa676",
                    "LayerId": "308ad61c-8d7b-43cd-b283-d7b453488f0c"
                }
            ]
        },
        {
            "id": "92d5d3be-ebd8-444f-98d4-1b4738ad1176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ea37b9b-86cd-4e7b-8d74-aad2b210af97",
            "compositeImage": {
                "id": "f696afc2-e6be-4de1-9253-d2aef4506243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d5d3be-ebd8-444f-98d4-1b4738ad1176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df967f8f-edec-4372-a066-fa6a20c8310a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d5d3be-ebd8-444f-98d4-1b4738ad1176",
                    "LayerId": "308ad61c-8d7b-43cd-b283-d7b453488f0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "308ad61c-8d7b-43cd-b283-d7b453488f0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ea37b9b-86cd-4e7b-8d74-aad2b210af97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 66,
    "xorig": 32,
    "yorig": 51
}