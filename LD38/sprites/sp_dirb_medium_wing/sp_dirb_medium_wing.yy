{
    "id": "8ff3c08a-fb1a-4969-886a-56452d5c01f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dirb_medium_wing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f449e041-2b32-442d-8fb3-0cf70b5a4de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff3c08a-fb1a-4969-886a-56452d5c01f5",
            "compositeImage": {
                "id": "10c14067-fe03-47d4-a40e-13c0cdb095e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f449e041-2b32-442d-8fb3-0cf70b5a4de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32f4488-591c-42cb-82f6-0637c73a4e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f449e041-2b32-442d-8fb3-0cf70b5a4de7",
                    "LayerId": "03cd580e-64e9-4a5e-ac29-dc853f34c38d"
                }
            ]
        },
        {
            "id": "c1f27ff6-c7bd-4f56-9dd5-1eaca282bbe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff3c08a-fb1a-4969-886a-56452d5c01f5",
            "compositeImage": {
                "id": "8f86b557-f11f-4812-861b-e74cd33942d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f27ff6-c7bd-4f56-9dd5-1eaca282bbe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96829b3c-1728-4464-9038-67cd1781f1d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f27ff6-c7bd-4f56-9dd5-1eaca282bbe1",
                    "LayerId": "03cd580e-64e9-4a5e-ac29-dc853f34c38d"
                }
            ]
        },
        {
            "id": "87280c8c-6a06-496a-bc56-d7ea6aa746b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff3c08a-fb1a-4969-886a-56452d5c01f5",
            "compositeImage": {
                "id": "52057e1d-2020-4923-89ab-0668d9f95d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87280c8c-6a06-496a-bc56-d7ea6aa746b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57039843-22ff-413b-93a2-49fe43f12b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87280c8c-6a06-496a-bc56-d7ea6aa746b8",
                    "LayerId": "03cd580e-64e9-4a5e-ac29-dc853f34c38d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "03cd580e-64e9-4a5e-ac29-dc853f34c38d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ff3c08a-fb1a-4969-886a-56452d5c01f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 43,
    "xorig": 36,
    "yorig": 43
}