{
		"VTile": false,
		"swfPrecision": 2.525,
		"gridY": 0,
		"name": "sp_dragoom_buttwings",
		"frames": [
				{
						"images": [
								{
										"LayerId": "b0221c78-c20b-bb80-c202-714049dee6c1",
										"mvc": "1.0",
										"id": "9fbdb3b1-d736-4f69-a038-08a690e776ca",
										"modelName": "GMSpriteImage",
										"FrameId": "3ea42d14-5841-583f-bf93-14354b4e548f"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "3ea42d14-5841-583f-bf93-14354b4e548f",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "16ace803-e6cd-f199-d1cf-a8790984d3fa",
								"modelName": "GMSpriteImage",
								"FrameId": "3ea42d14-5841-583f-bf93-14354b4e548f"
						},
						"SpriteId": "f76ab0a5-7b8f-37e8-c4b3-886634ea302f"
				},
				{
						"images": [
								{
										"LayerId": "b0221c78-c20b-bb80-c202-714049dee6c1",
										"mvc": "1.0",
										"id": "1087203b-bd89-f89c-ab78-987cfd4d7318",
										"modelName": "GMSpriteImage",
										"FrameId": "9ac0c354-791b-9e7b-e3cf-a76d4ccca8fa"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "9ac0c354-791b-9e7b-e3cf-a76d4ccca8fa",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "9f98b8dd-8ac3-86b2-b86b-05ca57c6c63e",
								"modelName": "GMSpriteImage",
								"FrameId": "9ac0c354-791b-9e7b-e3cf-a76d4ccca8fa"
						},
						"SpriteId": "f76ab0a5-7b8f-37e8-c4b3-886634ea302f"
				},
				{
						"images": [
								{
										"LayerId": "b0221c78-c20b-bb80-c202-714049dee6c1",
										"mvc": "1.0",
										"id": "783f8526-7acf-432a-9b6f-166af8fe3fae",
										"modelName": "GMSpriteImage",
										"FrameId": "cdf2fad3-77e6-9e0b-e58f-54826f3624f2"
								}
						],
						"mvc": "1.0",
						"modelName": "GMSpriteFrame",
						"id": "cdf2fad3-77e6-9e0b-e58f-54826f3624f2",
						"compositeImage": {
								"LayerId": "00000000-0000-0000-0000-000000000000",
								"mvc": "1.0",
								"id": "665a00b3-e9d4-233d-64de-bceb73b6ad61",
								"modelName": "GMSpriteImage",
								"FrameId": "cdf2fad3-77e6-9e0b-e58f-54826f3624f2"
						},
						"SpriteId": "f76ab0a5-7b8f-37e8-c4b3-886634ea302f"
				}
		],
		"id": "f76ab0a5-7b8f-37e8-c4b3-886634ea302f",
		"type": 0,
		"HTile": false,
		"bboxmode": 0,
		"bbox_bottom": 36,
		"modelName": "GMSprite",
		"gridX": 0,
		"playbackSpeedType": 0,
		"textureGroup": 0,
		"bbox_top": 0,
		"height": 37,
		"colkind": 1,
		"bbox_left": 0,
		"yorig": 20,
		"layers": [
				{
						"isLocked": false,
						"mvc": "1.0",
						"blendMode": 0,
						"name": "default",
						"opacity": 100,
						"modelName": "GMImageLayer",
						"id": "b0221c78-c20b-bb80-c202-714049dee6c1",
						"visible": true,
						"SpriteId": "f76ab0a5-7b8f-37e8-c4b3-886634ea302f"
				}
		],
		"origin": 9,
		"xorig": 22,
		"coltolerance": 0,
		"mvc": "1.12",
		"bbox_right": 28,
		"playbackSpeed": 15,
		"width": 29,
		"sepmasks": false,
		"For3D": false,
		"swatchColours": null
}