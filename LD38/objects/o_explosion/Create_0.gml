/// @description Insert description here
// You can write your code in this editor
part_sys = psys_create_system();
var size = 8;
particle1 = psys_create_type(part_sys, .2, c_orange, c_red, .7, 0, 0, 0, 0, size,1,pt_shape_circle);
part_type_shape(particle1,pt_shape_circle);
part_type_size(particle1,2,2,-0.03,0);
part_type_color2(particle1,4227327,255);
part_type_alpha3(particle1,0.70,0.61,0.06);
part_type_blend(particle1,1);
part_type_life(particle1,20,20);
particle2 = psys_create_type(part_sys, 1, c_white, c_orange, 1, 0, 3, 0, 359, size*.5,1,pt_shape_spark);
part_type_shape(particle2,pt_shape_spark);
part_type_size(particle2,1,1,-1,0.50);
part_type_color2(particle2,16777215,33023);
part_type_alpha2(particle2,1,0);
part_type_speed(particle2,3,7,-0.10,1);
part_type_direction(particle2,0,359,0,0);
part_type_gravity(particle2,0,270);
part_type_orientation(particle2,0,359,0.05,1,1);
part_type_blend(particle2,1);
part_type_life(particle2,10,60);
particle3 = psys_create_type(part_sys, 1, c_lime, c_lime, .8, .8, 0, 0, 0, size,1,pt_shape_sphere);
part_type_shape(particle3,pt_shape_flare);
part_type_size(particle3,1,2,0.03,0);
part_type_scale(particle3,1,1);
part_type_color3(particle3,16777215,33023,255);
part_type_alpha1(particle3,1);
part_type_gravity(particle3,0,270);
part_type_orientation(particle3,0,0,0,0,1);
part_type_blend(particle3,1);
part_type_life(particle3,20,20);
alarm[0] = 1;

