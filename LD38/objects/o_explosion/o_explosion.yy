{
    "id": "c4e00c46-c130-4aff-85d8-5303693435f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_explosion",
    "eventList": [
        {
            "id": "21d8d6c3-871d-4036-ba2c-1c20cb2cd6c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4e00c46-c130-4aff-85d8-5303693435f2"
        },
        {
            "id": "38b38b69-17f3-4c79-88ad-c98881c96f5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c4e00c46-c130-4aff-85d8-5303693435f2"
        },
        {
            "id": "e26c32a3-5ddf-48b8-b72d-efbb5a071f60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c4e00c46-c130-4aff-85d8-5303693435f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}