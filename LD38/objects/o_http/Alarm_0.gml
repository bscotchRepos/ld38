/// @description Convert provided data into HTTP request and send.

var processed_url = url ;
var processed_data = "";

if(method == "GET"){
	var query_string = "";
	if( is_string(data) ){ query_string += data ;}
	else if( not is_undefined(data) ){
		query_string = "?"+map_to_urlencoded(data);
	}
	processed_url += query_string;
}
else{
	// In all other cases, data should go into the body.
	if( ds_map_does_exist(data) ){
		if( not is_undefined(header[?"Content-Type"]) ){
			if( "application/json" == header[?"Content-Type"] && typeof(data)=="number" ){
				if( ds_exists(data,ds_type_map) ){
					processed_data = json_encode(data);
				}
			}
			else if( "application/x-www-form-urlencoded" == header[?"Content-Type"] ){
				processed_data = map_to_urlencoded(data);
			}
		}
	}
	else{
		processed_data = data;
	}
}

request = http_request(processed_url, method, headers, processed_data);

