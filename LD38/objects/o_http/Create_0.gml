/// @description (instance with wrapper scripts)

// Core parameters
method  = "GET" ;
url     = "" ;
data    = "" ; // can take a map, string, or buffer (set appropriate Content-Type header field!)
headers = new_map_from_args(
	"Content-Type","application/x-www-form-urlencoded",
	"Accept", "application/json,text/html,text/plain,application/xhtml+xml,application/xml;q=0.9,image/png,*/*;q=0.8",
	"User-Agent","Gamemaker Studio 2",
	"Accept-Encoding", "gzip,deflate,sdch"
); // Can override in wrapper scripts
preferred_response_type = "string" ; //"string"|"map", only matters for JSON responses

// Read-only results parameters
done = false;
failed = false;
status = 0; // If nothing recieved from server, this will stay 0
response_type = "string"; // "string"|"map"
response_raw = "";
response = "";


// Private
request = -92392345 ; // stores the request id (using random start id just in case GMS does something surprising)
ttl = 60 * room_speed ; // Nearly every connection expires in this time.
life = 0;


// Process changes made by wrapper scripts, then submit the request
alarm[0] = 1;
