/// @description Clean up after self

if(not is_undefined(headers[?"Content-Type"])){
	if( "application/octet-stream" == headers[?"Content-Type"] ){
		if( buffer_exists(data) ){ buffer_delete(data);}
	}
	else{ destroy_map_or_ignore_string(data); }
}
else{ destroy_map_or_ignore_string(data); }

destroy_map_or_ignore_string(headers);
destroy_map_or_ignore_string(response);
