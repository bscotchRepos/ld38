{
    "id": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_http",
    "eventList": [
        {
            "id": "f658fda7-2d28-4adb-8c23-4f8e9460ff3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af"
        },
        {
            "id": "51d8c34c-88fa-4e45-b7d1-e53379d129cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af"
        },
        {
            "id": "5f232f9f-5dd4-4a0d-b6a7-5fa5faa1a1b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af"
        },
        {
            "id": "a579c418-5dd7-4cfd-a593-f109f2f5c11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af"
        },
        {
            "id": "51264938-547e-49d7-943d-5994c8703a94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "feaecb8b-62e1-4ff0-bbf9-acf3e7dca9af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}