/// @description Watch for HTTP response
if( map_val_equals(async_load,"id",request)){
	if(async_load[?"status"] <= 0){
		// Either failed or succeeded
		done = true;
		status = async_load[?"http_status"];
		if(async_load[?"status"] < 0 || status > 300){
			// ERROR!
			failed = true;
			response_type="none";
		}
		else if(async_load[?"status"]==0){
			// SUCCESS!
			response_raw = async_load[?"result"];
			response = response_raw ;
			content_type = mapmap_get(async_load,"response_headers","Content-Type");
			if(response_raw != "" and preferred_response_type=="map" and not is_undefined(content_type)){
				if( string_pos("json",content_type) != 0){
					response_type = "map";
				}
			}
		}
	}
}