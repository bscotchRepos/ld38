{
    "id": "e453a6e8-0c1c-4bd7-a631-73f2c0777350",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_main_play",
    "eventList": [
        {
            "id": "413acfa4-09e0-4996-9532-61f700e72379",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e453a6e8-0c1c-4bd7-a631-73f2c0777350"
        },
        {
            "id": "88fc2103-3fe8-46d6-91be-6a4f21d1d4c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e453a6e8-0c1c-4bd7-a631-73f2c0777350"
        },
        {
            "id": "2adc586e-c1c3-49bb-9e4d-c4f6af88ee2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e453a6e8-0c1c-4bd7-a631-73f2c0777350"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cd7ee442-4e9e-431b-aff3-34a33d1dea7f",
    "visible": true
}