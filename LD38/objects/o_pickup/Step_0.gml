/// @description Insert description here
// You can write your code in this editor
x += lengthdir_x(airspeed*SLOMO_SECONDS, airdir);
y += lengthdir_y(airspeed*SLOMO_SECONDS, airdir);
airspeed = lerp(airspeed,0,.1*SPD);
//y = clamp(y,GAMEPLAY_TOP,GAMEPLAY_BOTTOM);

if x < view_xview()-50 {
	destroy = true;
}

var me = id;
with o_player {
	if point_distance(x,y,me.x,me.y) < (hitbox + me.hitbox) {
		me.collected = true;
	}
}

part_particles_create(ps,x,y, pt, 1);
//part_type_speed(pt,50/room_speed,100/room_speed,0,0);
