{
    "id": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pickup",
    "eventList": [
        {
            "id": "cf58dc67-7a92-44df-9088-a4821bec079b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411"
        },
        {
            "id": "93e37f40-ecad-4641-b06e-c9af64eb8f68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411"
        },
        {
            "id": "7f9e8502-cc74-4b68-92e1-60d219734089",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411"
        },
        {
            "id": "4ad29e07-da28-43f2-a551-d10f8bc598d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}