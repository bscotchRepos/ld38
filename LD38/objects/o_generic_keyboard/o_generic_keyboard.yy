{
    "id": "0dc63ec8-e330-494b-8344-223d972969e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_generic_keyboard",
    "eventList": [
        {
            "id": "8f2181f2-d58c-4c6a-ba74-309b22a7f449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        },
        {
            "id": "c13864b2-f295-409d-b016-322ee5c20f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        },
        {
            "id": "8e078206-e985-43fd-a5cc-97923426ecfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        },
        {
            "id": "60359791-9c37-4739-ab7f-96f1df0e4312",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        },
        {
            "id": "e7dfbc7e-3928-44ce-ad1c-706786998262",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        },
        {
            "id": "292a2045-49c7-4bb2-8c41-943030d61f5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0dc63ec8-e330-494b-8344-223d972969e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}