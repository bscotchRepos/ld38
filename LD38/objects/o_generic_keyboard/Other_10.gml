/// Spawn the keys.
with o_generic_keyboard_key { instance_destroy(); }
key_instances   = 0;
widest_row = 0;

for ( var xx = 0; xx < array_height_2d(charlists); xx++){
    for ( var yy = 0; yy < array_length_2d(charlists,xx); yy++){
        ds_list_destroy(charlists[xx,yy]);
    }
}
charlists = 0;

// Have to figure out how wide the rows are.
for (var shifted = 0; shifted < array_height_2d(keys); shifted++){
    for (var row = 0; row < array_length_2d(keys,shifted); row++){
        if shifted == 0 { widths[row] = 0; };
        var current_row_width = 0;
        var the_string = keys[shifted,row];
        
        charlists[shifted,row] = ds_list_create();
        
        for (var c = 1; c <= string_length(the_string); c++){
            var the_key = string_char_at(the_string,c);
            
            if the_key == "~" {
                c++;
                the_key += string_char_at(the_string,c);                
            }
            
            var keywidth = 1;
            if ds_map_exists( key_widths, the_key ) {
                keywidth = key_widths[?the_key];
            }
            
            ds_list_add(charlists[shifted,row], the_key);
            
            current_row_width += keywidth;
            widths[row] = max(widths[row],current_row_width);
            widest_row = max(widest_row,current_row_width);
        }
    }
}

num_rows    = array_length_2d(keys,0);
key_width   = floor(width/widest_row);
key_height  = floor(height/num_rows);

for ( var i = 0; i < array_length_1d(widths); i++){
    widths[i] *= key_width;
}

// WE HAVE PARAMETERS! LET'S DO THIS.
var kw = key_width;
var kh = key_height;
var rowlengths = 0;

// OKAY GETTING TIED UP HERE.
// What's the problem?
/*
    We have a bunch of lists of keys.
    We need to just create 
*/

for (var row = 0; row < num_rows; row++) {
    
    // Get the max length for each row
    row_characters = 0; // # of keys in a row
    for ( var shift_level = 0; shift_level < array_height_2d(keys); shift_level++) {
        row_characters = max(row_characters, ds_list_size(charlists[shift_level,row]));
    }
    
    for (var char_index = 0; char_index < row_characters; char_index++) {
        var keywidth = 1;                
        var char_to_pass = 0;
        
        // Fill up the characters... WITH STUFF.
        for ( var shift_level = 0; shift_level < array_height_2d(keys); shift_level++){
            var character_list = charlists[shift_level,row];
            if char_index < ds_list_size(character_list) {
                char_to_pass[shift_level] = character_list[|char_index];
            }
            else char_to_pass[shift_level] = "";
            
            if ds_map_exists( key_widths, char_to_pass[shift_level] ) {
                keywidth = max(keywidth, key_widths[?(char_to_pass[shift_level])]);
            }  
        }        
        keywidth *= kw;
        
        var the_key = noone;
        
        the_key = create_offscreen(o_generic_keyboard_key);
        var this_keyboard = id;
        
        with the_key {
            myparent = this_keyboard;
            mychar = 0;
            for ( var k = 0; k < array_length_1d(char_to_pass); k++) {
                mychar[k] = char_to_pass[k];
            }

            width               = keywidth;
            base_width          = keywidth;
            height              = kh;
            
            textcolor           = this_keyboard.textcolor;
            text_flashcolor     = this_keyboard.text_flashcolor;
            sprite              = this_keyboard.key_sprite;
            font                = this_keyboard.font;
            bg_color            = this_keyboard.bg_color;
            bg_flashcolor       = this_keyboard.bg_flashcolor;
            bg_alpha            = this_keyboard.bg_alpha;
            dropshadow          = this_keyboard.dropshadow;
            bordercolor         = this_keyboard.bordercolor;
            shift               = this_keyboard.shift;
            sound               = this_keyboard.sound;
            
            my_row = row;
            my_char_index = char_index;
        }
        
        key_instances[row,char_index] = the_key;
    }
}

input           = "";

with key_instances[0,0] { highlighted = true; }

/* */
/*  */
