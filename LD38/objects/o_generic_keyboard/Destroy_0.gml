ds_list_destroy(invalid_chars);
ds_map_destroy(key_widths);
ds_map_destroy(escapes);
for (var shifted = 0; shifted < array_height_2d(keys); shifted++) {    
    for (row = 0; row < array_length_2d(keys,shifted); row++) {
        ds_list_destroy(charlists[shifted,row]);
    }
}
var me = id;
with o_generic_keyboard_key { if myparent == me { instance_destroy(); }}

