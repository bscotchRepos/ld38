// PLACEMENT OF KEYS

// Determine whether to resize keys based on gamepad.
var rw  = 0; // Row widths
var gp  = gamepad_mode;
var gpa = gamepad_alignment;
var kw  = key_width;

for ( var r = 0; r < array_height_2d(key_instances); r++){
    rw[r] = 0;
}

var widest = 0;
var spacebar_row = -1;

with o_generic_keyboard_key {
    var wt = base_width; // Width target
    
    if key_display != " " {
        wt = kw+(base_width-kw)*(1-gp);
    }
    
    width = wt;
    rw[my_row] += width;
    widest = max(widest, rw[my_row]);
}

var top = GUIHEIGHT-height-1;
var ypos = top;

for ( var row = 0; row < array_height_2d(key_instances); row++) {
    
    var row_width = rw[row];
    var row_left = GUIWIDTH*.5-row_width*.5;
    
    // If we are in gamepad mode, we have to also align the keys based on other stuff.    
    var rowdiff = kw*(round(((widest-row_width)*.5)/(kw)));
    var actual_left_targ = GUIWIDTH*.5-widest*.5+rowdiff;
    
    // We need to round up to the nearest # of keys.
    row_left += (actual_left_targ-row_left)*gpa;
    
    ypos += key_height*.5;
    var xpos = row_left;
    
    for ( var k = 0; k < array_length_2d(key_instances, row); k++) {
        var the_instance = key_instances[row,k];
        with the_instance {
            xpos += width*.5;
            x = xpos;
            xpos += width*.5;
            y = ypos;
        }        
    }
    
    ypos += key_height*.5
}

// WHAT WAS THE LAST KEY PRESSED? DOES IT MATCH ONE?

if !instance_exists(o_bsid_confirmation_window) {

    var soundplay = false;
    if last_string != keyboard_string {
        soundplay = true;
        last_string = keyboard_string;
    }
    
    if keyboard_check(vk_anykey) {
        with o_generic_keyboard_key {
            if string_lower(chr(keyboard_key)) == string_lower(mychar[shift_level]) ||
               string_lower(chr(keyboard_key)) == string_lower(key_display) {
                    scale = scale_poke;  
                    if soundplay { play_sound_gv(sound,10,false); }
            }
        }
    }
    
    /// CLICKING KEYS!
    var d, dx = -1000, dy = -1000;
    var key_clicked = noone;
    
    for (d = 0; d < 5; d++) {
        if device_mouse_check_button_pressed(d,mb_left) {
            dx = device_mouse_x(d);
            dy = device_mouse_y(d);
            
            with o_generic_keyboard_key {
                if (abs(x-dx) <= width*.5 && abs(y-dy) <= height*.5) {
                    key_clicked= id;
                }
            }
        }
    }
    
    if CURRENT_GAMEPAD >= 0  {
        if gamepad_hotkey_pressed(gp_face1) {
            with o_generic_keyboard_key {
                if highlighted {
                    key_clicked= id;
                }
            } 
        }        
        else if gamepad_hotkey_pressed(gp_face3) {
            with o_generic_keyboard_key {
                if current_char == "~b" {
                    key_clicked= id;
                }
            } 
        }
        else if gamepad_hotkey_pressed(gp_stickl) {
            with o_generic_keyboard_key {
                if current_char == "~s" {
                    key_clicked= id;
                }
            } 
        }
        else if gamepad_hotkey_pressed(gp_stickr) {
            with o_generic_keyboard_key {
                if current_char == " " {
                    key_clicked= id;
                }
            }
        }
    }
    
    if key_clicked != noone && input_cooldown <= 0 {
        var new_input = "";           
            with key_clicked {                
                scale = scale_poke;
                play_sound_gv(sound,10,false);
                new_input = current_char;                
            }
            
            switch new_input {
                case "~f":
                    if !alt { alt = 2; }
                    else alt = 0;
                    break;
                case "~c":
                    caps = !caps;
                    if !caps { shift = false; }
                    else shift = true;
                    break;
                case "~r":
                    input += chr(10); // New line
                    break;
                case "~t":
                    input += chr(9); // Tab
                    break;
                case "~s":
                    if caps { caps = false; }
                    else { shift = !shift; }
                    break;
                case "~b":
                    input = string_delete(input,string_length(input),1);
                    break;
                default:
                    if new_input == "~~" {
                        new_input = "~";       
                    }                
                    input += new_input;
                    if !caps { shift = 0; }
                    if alt == 2 { alt = 0; }
                    break;
            }
            
            keyboard_string = input;
    }
}

input_cooldown = max(0,input_cooldown-SECONDS_SINCE_UPDATE);