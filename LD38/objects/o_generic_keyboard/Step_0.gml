/// Gamepad navigation
check_show_gamepad();

var gp_b = 1;
var gp_s = 1;
var gp_mode_target = 0;

if CURRENT_GAMEPAD >= 0 && !instance_exists(o_bsid_confirmation_window) {
    gp_mode_target = 1;
    
    gp_b = gamepad_key_brightness;
    gp_s = gamepad_key_scale;
    
    var stick_held = false;
    var gp_xy = get_gamepad_axis(CURRENT_GAMEPAD,gp_axislh,gp_axislv,true);
    var gp_dirdist = get_dirdist(0,0,gp_xy[0],gp_xy[1]);
    var k = key_instances;
    
    if gp_dirdist[1] >= GAMEPAD_DEADZONE {
        stick_held = true;
        
        if gamepad_cooldown <= 0 {
            // Try to change selection.
            var moved = false;
            var sel = noone;
            var next = noone;
            
            with o_generic_keyboard_key { if highlighted { sel = id; }}
            if sel == noone {
                with o_generic_keyboard_key {
                    if sel == noone {
                        sel = id;
                        highlighted = true;
                    }
                }
            }
            
            with sel {
                // Find the next key! // Find the nearest 8, and get the one that's closest to the direction.
                var distances = ds_priority_create();
                var me = id;      
                var row_check = my_row;       
                
                var row_select = row_check;
                var rounded_dir = round_to_nearest(gp_dirdist[0],90);
                
                if rounded_dir == 90 {
                    row_check = my_row-1;
                }
                else if rounded_dir == 270 {
                    row_check = my_row+1;
                }
                
                with o_generic_keyboard_key {
                    if id != me {
                        if my_row == row_check {
                            var pdist = point_distance(x,y,me.x,me.y);
                            ds_priority_add(distances,id,pdist);
                        }
                    }
                }
                
                var numcheck = min(3, ds_priority_size(distances)); // Check the 3 closest
            
                while ds_priority_size(distances) > numcheck {
                    ds_priority_delete_max(distances);
                }
                
                var closest_dir = 80;
                while numcheck > 0 {
                    var instance_check = ds_priority_find_max(distances);
                    with instance_check {
                        var pdir = point_direction(me.x,me.y,x,y);
                        var adiff = abs(angle_diff(pdir,gp_dirdist[0]));
                        if adiff < closest_dir+10 { // 10 degrees of wiggle room, because this new instance is closer than the last one.
                            next = id;
                            closest_dir = adiff;
                        }
                    }
                    
                    ds_priority_delete_max(distances);
                    numcheck--;
                }
                
                ds_priority_destroy(distances);
                //next = nearest_instance_in_direction(round_to_nearest(gp_dirdist[0],90),25,object_index);
                
                if next == noone {
                    // TIME TO DO A LOOP!
                    var force_row = my_row;
                    var force_char = my_char_index;                    
                    
                    if rounded_dir == 90 || rounded_dir == 270 {
                        if rounded_dir == 90 {
                            force_row = my_row-1;
                        }
                        else if rounded_dir == 270 {
                            force_row = my_row+1;
                        }
                        
                        if force_row < 0 {
                            // Find the bottom row
                            with o_generic_keyboard_key {
                                if my_row > force_row {
                                    force_row = my_row;
                                }
                            }
                        }
                        else force_row = 0;
                        
                        var xp = me.x;
                        var maxdiff = view_wview();
                        with o_generic_keyboard_key {
                            if my_row == force_row {
                                var xdiff = abs(x-xp);
                                if xdiff < maxdiff {
                                    maxdiff = xdiff;
                                    next = id;
                                }
                            }
                        }
                    }
                    else {
                        if rounded_dir == 180 {
                            force_char = -1;
                            
                            with o_generic_keyboard_key {
                                if my_row == force_row && my_char_index > force_char {
                                    force_char = my_char_index;
                                    next = id;
                                }
                            }  
                        }    
                        else {
                            force_char = 0;   
                            with o_generic_keyboard_key {
                                if my_row == force_row && force_char == my_char_index {
                                    next = id;
                                }
                            }                            
                        }                                       
                    }
                }
            }
            
            
            if next != noone {
                play_sound_gv(snd_menuclick,10,false);
                with o_generic_keyboard_key {
                    highlighted = false;
                }
                with next { highlighted = true; }
                
                gamepad_cooldown = gamepad_maxcool;
                gamepad_maxcool = max(.04,gamepad_maxcool-.1);
            }
            
        }
    }
    
    if !stick_held {
        gamepad_cooldown = 0;
        gamepad_maxcool = gamepad_maxcool_start;    
    }
    else {
        if gamepad_cooldown <= 0 {
            gamepad_maxcool = gamepad_maxcool_start;
        }
        gamepad_cooldown = max(0,gamepad_cooldown-SECONDS_SINCE_UPDATE);        
    }    
}
else gp_mode_target = 0;

// When going into gamepad mode,
//    First resize, then align.
// When gamepad mode is 0,
//    First align, then resize.

var gamepad_tween_speed = .5;

if gp_mode_target == 1 || (gp_mode_target == 0 && gamepad_alignment == 0) {
    gamepad_mode += tween(gamepad_mode,gp_mode_target,gamepad_tween_speed);
    if abs(gamepad_mode-gp_mode_target) <= .01 {
        gamepad_mode = gp_mode_target;
    }
}

if gp_mode_target == 0 || (gp_mode_target == 1 && gamepad_mode == 1) {
    gamepad_alignment += tween(gamepad_alignment,gp_mode_target,gamepad_tween_speed);
    if abs(gamepad_alignment-gp_mode_target) <= .01 {
        gamepad_alignment = gp_mode_target;
    }
}

with o_generic_keyboard_key {
    var btarg = 1;
    var starg = 1;
    
    if !highlighted {
        btarg = gp_b;
        starg = gp_s;
    }
    else {
        btarg = 1;
        starg = 1;
        scalemod = starg;
        brightness = btarg;
    }
    if brightness != btarg {
        brightness += tween(brightness,btarg,.4);
        if abs(brightness-btarg) < .0025
            brightness = btarg;
    }
    if scalemod != starg {
        scalemod += tween(scalemod,starg,.4);
        if abs(scalemod-starg) < .0025
            scalemod = starg;
    }
}

/// Cleaning up inputs

if !instance_exists(o_bsid_confirmation_window) {
    input = keyboard_string;
    
    if last_input != input {
        /// LET'S CLEAN UP THAT STRIIIING!
        var input_cleaned = input;
        
        var lowest_position = max(1,string_length(input)-3);
        
        if(string_length(input_cleaned) > max_length) {
            input_cleaned = string_copy(input_cleaned,1,max_length) ;
        }
        
        for (var charpos = string_length(input_cleaned); charpos >= lowest_position; charpos--) {
            var charcheck = string_char_at(input_cleaned,charpos);
            if ds_list_contains(invalid_chars,charcheck) {
                echo(charcheck,"is not a valid character. REMOVING!");
                input_cleaned = string_delete(input_cleaned,charpos,1);
            }
            else if only_allow_current_keys {
                // Check if the input contains characters not included in the keyboard.
                // If so, scrub them out!
                var char_found = false;
                
                for ( var ss = 0; ss < array_height_2d(keys) && !char_found; ss++){
                    for ( var r = 0; r < array_length_2d(keys, ss) && !char_found; r++){
                        var char_list = charlists[ss,r];
                        for ( var c = 0; c < ds_list_size(char_list) && !char_found; c++){
                            var char_check_against = char_list[|c];
                            if char_check_against == "~~" { char_check_against = "~" };
                            if string_lower(charcheck) == string_lower(char_check_against) {
                                char_found = true;
                            }
                        }
                    }
                }
                
                if !char_found {
                    echo(charcheck,"is not included in the key set. REMOVING!")
                    input_cleaned = string_delete(input_cleaned,charpos,1);
                }
            }
        }
        
        input = input_cleaned;
        last_input = input;
    }
    
    keyboard_string = input;
}

