/*
    KEYBOARD SPECS
    Internally stores a string called "input"
    Spawn height
    Number of keys in a row
    Have default setup for keys (lowercase and uppercase), can be overwritten
    
    Make a script that spawns this thing
    Always occupy the bottom portion of the screen
    Also take PC input
*/

input_cooldown = .1;

keyboard_string = "";
last_string = keyboard_string;
set_depth_to_top();

input           = "";
last_input      = input;
font            = fnt_bsid_universal_large;
height          = round(view_hview()*.5);
width           = view_wview()-10;
textcolor       = c_white;
dropshadow      = 0;
text_flashcolor = c_black;
key_sprite      = -1;
bg_color        = c_black;
bg_flashcolor   = c_white;
bg_alpha        = .7;
bordercolor     = c_aqua;


escape_character = "~";
max_length = 64;

only_allow_current_keys = true; // This boolean determines whether keyboard input can be used to type keys that aren't within the character set

escapes = ds_map_create();
ds_map_add(escapes,"b","Del");
ds_map_add(escapes,"~","~");
ds_map_add(escapes,"s","Shift");
ds_map_add(escapes,"c","Caps");
ds_map_add(escapes,"t","Tab");
ds_map_add(escapes,"r","Enter");
ds_map_add(escapes,"f","Fn");

caps = false;
alt = 0;
shift = 0;

charlists = 0;

key_widths = ds_map_create();
ds_map_add(key_widths, "~b", 1.5);
ds_map_add(key_widths, " ", 5);
ds_map_add(key_widths, "~c", 1.5);
ds_map_add(key_widths, "~s", 2);
ds_map_add(key_widths, "~t", 1.5);
ds_map_add(key_widths, "~r", 2);
ds_map_add(key_widths, "~f", 1.25);

invalid_chars = ds_list_create();

// Index 0 is lowercase
    keys[0,0] = "1234567890<"
    keys[0,1] = "qwertyuiop"
    keys[0,2] = "asdfghjkl"
    keys[0,3] = "~zxcvbnm_"
// Index 1 is uppercase
    keys[1,0] = "1234567890<"
    keys[1,1] = "QWERTYUIOP"
    keys[1,2] = "ASDFGHJKL"
    keys[1,3] = "~ZXCVBNM_"

widths = 0;
key_instances = 0;        
widest_row = max(
    string_length(keys[0,0]),
    string_length(keys[0,1]),
    string_length(keys[0,2]),
    string_length(keys[0,3])
);
num_rows = array_length_2d(keys,0);
key_width = display_get_gui_width()/widest_row;
key_height = height/num_rows;

//============ GAMEPAD
gamepad_cooldown        = 0;    
gamepad_maxcool_start   = .25;
gamepad_maxcool         = gamepad_maxcool_start;
gamepad_key_scale        = .8;
gamepad_key_brightness   = .7;
gamepad_mode    = 0;
gamepad_alignment = 0;

if CURRENT_GAMEPAD >= 0 { gamepad_mode = 1; }
if CURRENT_GAMEPAD >= 0 { gamepad_alignment = 1; }
//============ END GAMEPAD

/* */
/*  */
