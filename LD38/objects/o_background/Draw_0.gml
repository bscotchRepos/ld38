draw_sprite_ext(sp_sky,0,view_center_x(),view_center_y(),bgscale,bgscale,0,c_white,1);



var general_speed = 1;
var camera_offset = 0;
with o_camera_new{
	camera_offset = movespeed*SLOMO_SECONDS;
}

for (var i = 0; i < cloud_number; i++){
	ds_priority_add(cloud_depth_stack, i, cloud_array[i]);
}

while ds_priority_empty(cloud_depth_stack) == false{
	var cloud_id = ds_priority_delete_max(cloud_depth_stack);
	var cloud_depth = cloud_array[cloud_id];
	var cloud_speed = general_speed*(1 - cloud_depth*.1);
	cloud_x_location[cloud_id] = cloud_x_location[cloud_id] - cloud_speed + camera_offset;
	if cloud_x_location[cloud_id] < view_xview() {
		cloud_x_location[cloud_id] = random_range(view_right() + view_wview(), (view_right() + view_wview())*2);
		cloud_array[cloud_id] = irandom(cloud_number);		
	}
	else{
		var cloud_size = 1 - cloud_depth*.1;
		var subimg = cloud_id mod 3;
		draw_sprite_ext(sp_cloud,subimg,cloud_x_location[cloud_id], view_yview()+cloud_y_location[cloud_id], cloud_size,cloud_size,0,c_white,1);
	}		
}	

var bgwidth = sprite_get_width(sp_background);
	for ( var i = 0; i < sprite_get_number(sp_background); i++) {
	var bgpos = view_xview()*(.9-.25*i);
	while bgpos < view_xview() { bgpos += bgwidth; }
 	
	draw_sprite_ext(sp_background,i,bgpos,view_bottom(),1,1,0,c_white,1);
	draw_sprite_ext(sp_background,i,bgpos+bgwidth,view_bottom(),1,1,0,c_white,1);
}
/*

range is from view_bottom up to 0;
Scaling should be further in the back, less it moves
Closer in the front, faster it moves


*/



/*
var topgreen = make_colour_rgb(50,150,50);
var botgreen = color_change(topgreen,.5);

draw_rectangle_colour(view_xview()-5,view_yview()-5,view_right()+5,view_bottom()+5,topgreen,topgreen,botgreen,botgreen,0);

draw_line_width_colour(view_xview()-5,GAMEPLAY_TOP,view_right()+5,GAMEPLAY_TOP,3,c_white,c_white)
draw_line_width_colour(view_xview()-5,GAMEPLAY_BOTTOM,view_right()+5,GAMEPLAY_BOTTOM,3,c_white,c_white)
