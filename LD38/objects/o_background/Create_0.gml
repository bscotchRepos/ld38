depth = 1000;
xoffset = 0;
bgscale = 1.05*GUIHEIGHT/sprite_get_height(sp_sky);
cloud_depth_stack = ds_priority_create();
cloud_array = 0;
cloud_number = 5;
cloud_x_location = 0;
cloud_y_location = 0;
var upper_bound = sprite_get_height(sp_cloud);
var lower_bound = view_hview()*.5;
for (var i = 0; i < cloud_number; i++){
	cloud_array[i] = irandom(cloud_number);
	cloud_x_location[i] = random_range(0, (view_xview() + view_wview())*2);
	cloud_y_location[i] = random_range(upper_bound, lower_bound);
}

