{
    "id": "72e7fcb4-fa2c-4aea-b62d-9da628be0b2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_background",
    "eventList": [
        {
            "id": "ae717e0d-7690-46a6-8d0b-dababaa0d511",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "72e7fcb4-fa2c-4aea-b62d-9da628be0b2d"
        },
        {
            "id": "18071dec-413b-4649-a4f1-830b06355b6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72e7fcb4-fa2c-4aea-b62d-9da628be0b2d"
        },
        {
            "id": "799e7fb9-aa4b-42c0-b879-9396771f2a90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "72e7fcb4-fa2c-4aea-b62d-9da628be0b2d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}