{
    "id": "74de29b8-4a0f-49da-bfd3-923e3ea2444a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_pause_main",
    "eventList": [
        {
            "id": "3abd56d5-19e5-410e-9e4e-c26bf2eebc63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "74de29b8-4a0f-49da-bfd3-923e3ea2444a"
        },
        {
            "id": "acf90196-8e59-4fb9-b5b4-3d2db844f605",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74de29b8-4a0f-49da-bfd3-923e3ea2444a"
        },
        {
            "id": "4b5af80c-1466-4546-a0fa-dcf4435c5ca7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "74de29b8-4a0f-49da-bfd3-923e3ea2444a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
    "visible": true
}