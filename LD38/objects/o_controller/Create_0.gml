METERS = 0;
play_music(mus_gameplay,true);

instance_create(0,0,o_player);
instance_create(0,0,o_camera_new);
instance_create(0,0,o_background);
instance_create(0,0,o_interface);

// SPAWNING, YEAH BABY!
spawnthing_floater	= 0 ;
spawnthing_balloom	= 1 ;
spawnthing_enemy	= 2 ;
spawnthing_malloom	= 3 ;
spawnthing_guard_pickup = 4;
spawnthing_shmaloo = 5;

var i;
i = spawnthing_floater;
	spawn_min_xgap[i] = 500;
	spawn_max_xgap[i] = 1000;
	spawn_next_x[i] = view_right()+.5*random_range(spawn_min_xgap[i], spawn_max_xgap[i]);

i = spawnthing_balloom;
	spawn_min_xgap[i] = 600;
	spawn_max_xgap[i] = 1500;
	spawn_next_x[i] = view_center_x();

i = spawnthing_enemy;
	spawn_min_xgap[i] = 500;
	spawn_max_xgap[i] = 1000;	
	spawn_next_x[i] = view_right()+random_range(spawn_min_xgap[i], spawn_max_xgap[i]);

i = spawnthing_malloom;
	spawn_min_xgap[i] = 1000*(1-upgrade_get_effect(upg_mallowfind));
	spawn_max_xgap[i] = 2000*(1-upgrade_get_effect(upg_mallowfind));	
	spawn_next_x[i] = view_right();	
			
i = spawnthing_guard_pickup;
	spawn_min_xgap[i] = 1000*(1-upgrade_get_effect(upg_mallowfind));
	spawn_max_xgap[i] = 2000*(1-upgrade_get_effect(upg_mallowfind));	
	spawn_next_x[i] = view_right();	 
	
i = spawnthing_shmaloo;
	spawn_min_xgap[i] = 1000*(1-upgrade_get_effect(upg_mallowfind))*10;
	spawn_max_xgap[i] = 2000*(1-upgrade_get_effect(upg_mallowfind))*10;	
	spawn_next_x[i] = view_right();	 