update_delta();
BEST_DISTANCE = max(BEST_DISTANCE, METERS);

if CURRENT_STAGE < array_length_1d(STAGE_MARK) && METERS > STAGE_MARK[CURRENT_STAGE]
	 CURRENT_STAGE++

if instance_exists(o_pause) {
	SPD = 0;
}
else {
	SPD = 1;
	if keyboard_check_pressed(vk_escape)
		create_offscreen(o_pause);
}



for ( var spawnthing = 0; spawnthing < array_length_1d(spawn_next_x); spawnthing++){
	if (view_right()+200 > spawn_next_x[spawnthing]) {
		var topspawn = GAMEPLAY_TOP+40;
		var bottomspawn = GAMEPLAY_BOTTOM-40;
		
		// OH SHIT, SPAWN A THING!
		var yspawn = random_range(GAMEPLAY_TOP+40, GAMEPLAY_BOTTOM-40);
		var xspawn = spawn_next_x[spawnthing];
		
		switch spawnthing {
			case spawnthing_floater:
				instance_create(xspawn, yspawn, o_floater);
				break;
			case spawnthing_enemy:
				instance_create(xspawn, yspawn, o_enemy);
				break;
			case spawnthing_balloom:
				var num_ballooms = irandom_range(3,3+CURRENT_STAGE*4);
				var balloom_spacing = 60;
				while num_ballooms > 0 {
					var v_ballom = instance_create(spawn_next_x[spawnthing],yspawn,o_balloom)
					yspawn = clamp(yspawn+choose(-1,0,1)*balloom_spacing, topspawn, bottomspawn);
					spawn_next_x[spawnthing] += balloom_spacing;
					num_ballooms--;
				}
				break;
			case spawnthing_malloom:
				instance_create(xspawn, yspawn, o_malloom);		
				break;
			case spawnthing_guard_pickup:
				//instance_create(xspawn, yspawn, o_guard_pickup);		
				break;
			case spawnthing_shmaloo:
				instance_create(xspawn, yspawn, o_shmaloo);		
				break;														
		}
		
		spawn_next_x[spawnthing] += random_range( spawn_min_xgap[spawnthing], spawn_max_xgap[spawnthing] );
	}
}

BALLOOM_PITCH_TIMER = max(0, BALLOOM_PITCH_TIMER-SLOMO_SECONDS);
if BALLOOM_PITCH_TIMER <= 0 {
	BALLOOM_PITCH = 1;
}

if !instance_exists(o_player) && !instance_exists(o_endscreen) {
	instance_create(0,0,o_endscreen);
}
