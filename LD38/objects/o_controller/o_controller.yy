{
    "id": "b8ca41a3-dfcf-4f01-a580-71ed2c69bd16",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_controller",
    "eventList": [
        {
            "id": "4e72fa43-2462-4666-8085-19d6d86fc3ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8ca41a3-dfcf-4f01-a580-71ed2c69bd16"
        },
        {
            "id": "1dd849d9-ccb4-4dd1-8d71-1870f419346b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8ca41a3-dfcf-4f01-a580-71ed2c69bd16"
        },
        {
            "id": "bfb625db-9db9-4eef-bc6d-e32f6eb584cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b8ca41a3-dfcf-4f01-a580-71ed2c69bd16"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}