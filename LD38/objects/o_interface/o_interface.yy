{
    "id": "2c1533e8-11f5-4bcc-9ec8-e014ee9f7b42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_interface",
    "eventList": [
        {
            "id": "d8e632ac-b8fa-42b2-8a45-357ea2ed1110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2c1533e8-11f5-4bcc-9ec8-e014ee9f7b42"
        },
        {
            "id": "ec806cb9-1fe0-4742-a498-1a8cc662f414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c1533e8-11f5-4bcc-9ec8-e014ee9f7b42"
        },
        {
            "id": "085c20cd-e6c6-4d54-8d27-57d51541c5dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c1533e8-11f5-4bcc-9ec8-e014ee9f7b42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "106af0f4-ec90-4b28-b874-6b88939c718d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}