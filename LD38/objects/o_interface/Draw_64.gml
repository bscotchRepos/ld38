METERS = floor(view_center_x()/200);

var txt = add_commas(floor(METERS)) + " Meters";
var tsc = .35;
cloudwidth = lerp(cloudwidth, string_width(txt)*tsc+20,.25);
draw_sprite(sp_howfaryoubegoin,0,GUIWIDTH-cloudwidth,25);

font_set(fnt_header,fa_right,fa_middle);
draw_text_transformed_colour(GUIWIDTH-10,25,add_commas(floor(METERS)) + " Meters", tsc, tsc, 0, c_black,c_black,c_black,c_black, .7);


var icons = 0;
var text = 0;
var icon_size = 40;

font_set(fnt_header,fa_left,fa_middle);

icons[0] = sp_balloom_icon; text[0] = add_commas(BALLOOMS);
icons[1] = sp_beak; text[1] = add_commas(get_total_beaks());

var tw = 0;
for ( var i = 0; i < array_length_1d(text); i++){
	tw = max(tw, string_width(text[i])*tsc);
}
collectwidth = lerp(collectwidth, tw+icon_size+15, .25);
draw_sprite(sp_whatyoubecollectin,0,collectwidth,0);

var vertspacing = icon_size;
for ( var i = 0; i < array_length_1d(icons); i++){
	var icon_scale = min(icon_size/sprite_get_width(icons[i]), icon_size/sprite_get_height(icons[i]));
	draw_sprite_ext( icons[i], 0, icon_size*.5+5, icon_size*.5+5+vertspacing*i, icon_scale, icon_scale, 0, c_white, 1 );
	draw_text_transformed_colour(icon_size+10, icon_size*.5+5+vertspacing*i, text[i], tsc, tsc, 0, c_black,c_black,c_black,c_black, .7);
}

/*
draw_sprite_ext(sp_balloom_icon, 0, icon_size*.5+5, icon_size*.5+5, 

shadowtext(+5,5, add_commas(BALLOOMS) + " Ballooms | " + add_commas(get_total_beaks()) + " Beaks", 1, 1, 0, c_white, 1, 2);