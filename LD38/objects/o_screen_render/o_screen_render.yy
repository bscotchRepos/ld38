{
    "id": "11ad305f-4d9d-488f-a889-553ea4728e94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_screen_render",
    "eventList": [
        {
            "id": "6b54f4e7-f4be-4bc2-a7a5-f6d2ea37b694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11ad305f-4d9d-488f-a889-553ea4728e94"
        },
        {
            "id": "e145121a-c553-4a6d-a12a-2caacb0e0a02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11ad305f-4d9d-488f-a889-553ea4728e94"
        },
        {
            "id": "daf970d0-8c36-4dcf-88ae-7ab768769171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "11ad305f-4d9d-488f-a889-553ea4728e94"
        },
        {
            "id": "bd123906-8493-4a20-963d-5fdff6859997",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "11ad305f-4d9d-488f-a889-553ea4728e94"
        },
        {
            "id": "0f9e7d3d-877c-4a6d-802f-7e9379ce2b21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "11ad305f-4d9d-488f-a889-553ea4728e94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}