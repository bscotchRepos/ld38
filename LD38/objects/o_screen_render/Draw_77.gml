// Global effects should go here

/*
shader_set(shad_gameboy);
brightness = shader_get_uniform( shad_gameboy, 'brightness' ) ;
shader_set_uniform_f(brightness,.1);
*/

shader_set(shad_bloom);
shader_set_uniform_f(bloom_intensity, intensity); //0 = off, 1 = a bit, 80 = extremely intense
//shader_set_uniform_f(bloomblurSize, ((window_mouse_get_x()-250)/1000)); // usually something like 1/512 (0.001)
shader_set_uniform_f(bloom_blursize, blursize);

if window_get_fullscreen()
    draw_surface_stretched(application_surface,0,0,display_get_width(),display_get_height());
else draw_surface_stretched(application_surface,0,0,GUIWIDTH,GUIHEIGHT);
shader_reset() ;


/* */
/*  */
