if keyboard_check(vk_up) {
    intensity += .002;
}
if keyboard_check(vk_down) {
    intensity -= .002;
}

if keyboard_check(vk_left) {
    blursize -= .0002;
}
if keyboard_check(vk_right) {
    blursize += .0002;
}

intensity = max(0,intensity);
blursize = max(0,blursize);

