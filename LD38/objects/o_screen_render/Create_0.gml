application_surface_enable(true);
application_surface_draw_enable(false);

bloom_intensity = shader_get_uniform(shad_bloom, "intensity");
bloom_blursize = shader_get_uniform(shad_bloom, "blurSize");

intensity = .1;
blursize = .001;

//surface_resize(application_surface,window_get_width(),window_get_height())
// Instantiate this object if you want to create a full-screen shader effect.

