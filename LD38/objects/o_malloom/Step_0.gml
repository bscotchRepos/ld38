if !acquired {
	if ( x < view_xview()-50 ) {
		instance_destroy();
		exit;
	}

	var me = id;

	with o_player {
		if point_distance(x,y,me.x,me.y) < (hitbox + me.hitbox) {			
			collided = true;
			play_sound_varied(snd_coinpickup,10,false);
			var following = id;
			if !ds_list_empty( followers ) {
				following = ds_list_find_last( followers );
			}
			me.follow_direction = point_direction( me.x, me.y, following.x, following.y );
			ds_list_add( followers, me );			
			me.acquired = true;
		}
	}
	if acquired { xscale = 1; }
}