var yd = y + 5*sin(3*timer);
if !acquired {
	timer += SLOMO_SECONDS;
}
blinking = blink(blinking);

var rotation_draw = rotation;
if acquired {	
	if rotation > 90 && rotation < 270 {
		xscale = -1;
	}
	else xscale = 1;

	if xscale == -1 { rotation_draw = rotation+180; }
}
else {
	magnet_toward_player(upg_mallow_magnet);
}
draw_sprite_ext(sprite_index, isblinking(blinking), x, yd, xscale, 1, rotation_draw, c_white, 1);
/*
draw_circle_colour(x,y,hitbox+2,c_black,c_black,0);
draw_circle_colour(x,y,hitbox,c_white,c_white,0);