{
    "id": "a546d4d2-3d4c-4f7b-b1bb-741b4efe24a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_malloom",
    "eventList": [
        {
            "id": "c6835a5d-8544-48b1-9977-fc2d27dd3cea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a546d4d2-3d4c-4f7b-b1bb-741b4efe24a4"
        },
        {
            "id": "e6f009f8-f492-4579-8182-f6b4eb88fc68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a546d4d2-3d4c-4f7b-b1bb-741b4efe24a4"
        },
        {
            "id": "e42106f6-f855-4662-adc2-f15af9fe176f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a546d4d2-3d4c-4f7b-b1bb-741b4efe24a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9073e54e-57aa-06c0-dd3b-98e5ba9e80e4",
    "visible": true
}