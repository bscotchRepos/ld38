myparent            = noone;
mychar[0]           = "x";
current_char        = "x";

width               = 50;
height              = 50;
base_width          = width;
base_height         = height;

textcolor           = c_white;
text_flashcolor     = c_white;
sound               = snd_menuclick;
sprite              = -1;
font                = fnt_bsid_universal_large;
bg_color            = c_black;
bg_flashcolor       = c_white;
bg_alpha            = 1;
dropshadow          = 2;
bordercolor         = c_aqua;

highlighted         = false;
brightness          = 1;
scalemod            = 1;
scale               = 1;
flashing            = 0;
scale_poke          = .7;
base_depth = depth;

shift_level = 0;
key_display = "";
caps    = 0;
shift   = 0;
alt     = 0;

my_char_index = 0;
my_row = 0;

