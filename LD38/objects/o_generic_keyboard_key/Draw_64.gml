if instance_exists(myparent) {
    var xd = x;
    var yd = y;
    var s  = scale*scalemod;
    
    var w  = width*s*.5;
    var h  = height*s*.5;
    
    shift = myparent.shift;
    alt = myparent.alt;
    caps = myparent.caps;

    shift_level = min(shift+alt, array_length_1d(mychar)-1);
    key_display = mychar[shift_level];
    current_char = key_display;
    
    var bgcol = bg_color;
    var tcol  = textcolor;
    var bdcol = bordercolor;
    
    if CURRENT_GAMEPAD >= 0 {
        switch mychar[shift_level] {
            case "~b":
                tcol = gamepad_button_color(gp_face3);
                break;
            //case "~s":
            //    tcol = gamepad_button_color(gp_face4);
            //    break;
        }
        if highlighted { tcol = gamepad_button_color(gp_face1); }
    }
    
    if ((key_display == "~c" && caps) || (key_display == "~f" && alt) || (key_display == "~s" && shift && !caps)) {
        bgcol = bg_flashcolor;
        tcol = text_flashcolor;
    }
    else if scale != 1 {
        var flash_percent = clamp(1-abs(scale-scale_poke)/(1-scale_poke),0,1);
        bgcol = merge_colour(bgcol,bg_flashcolor,flash_percent);
        tcol = merge_colour(tcol,text_flashcolor,flash_percent);
    }
        
    if string_char_at(key_display,1) == "~" {        
        var escaped = string_char_at(key_display,2);
        if ds_map_exists(myparent.escapes,escaped) {
            key_display = myparent.escapes[?escaped];
        }
    }
    
    if brightness != 1 {
        bgcol  =  color_change( bgcol, brightness );
        tcol   =  color_change( tcol,  brightness );
        bdcol  =  color_change( bdcol, brightness );
    }
    
    if key_display == "#" { key_display = "\\#"; }

    if sprite != -1 && sprite_exists(sprite) {
        var sp_xscale = width/sprite_get_width(sprite);
        var sp_yscale = height/sprite_get_height(sprite);
        draw_sprite_ext(sprite,0,xd,yd,sp_xscale*s,sp_yscale*s,0,bgcol,bg_alpha);
    }
    else {
        draw_set_alpha(bg_alpha);
        draw_rectangle_colour(xd-w, yd-h, xd+w, yd+h, bgcol, bgcol, bgcol, bgcol, 0);
        draw_set_alpha(1);
        draw_rectangle_colour(xd-w+1, yd-h+1, xd+w-1, yd+h-1, bdcol, bdcol, bdcol, bdcol, 1);
    }
    
    // DRAW THE TEXT!
    var key_space = .8;

    font_set(font,fa_center,fa_middle);
    var tscale = min( (width*key_space)/string_width(string_hash_to_newline(key_display)), (height*key_space)/string_height(string_hash_to_newline(key_display)) )*s;
    if dropshadow != 0 {
        shadowtext(xd,yd,key_display,tscale,tscale,0,tcol,1,dropshadow);
    }
    else {
        draw_text_transformed_colour(xd,yd,string_hash_to_newline(key_display),tscale,tscale,0,tcol,tcol,tcol,tcol,1);
    }
    
    // Draw the hotkey if gamepad is active
    if CURRENT_GAMEPAD >= 0 {
        var gpdraw = -1;
        switch mychar[shift_level] {            
            case "~b":
                gpdraw = gp_face3;
                break;
            case " ":
                gpdraw = gp_stickr;
                break;
            case "~s":
                gpdraw = gp_stickl;
                break;
        }
        if gpdraw != -1 {
            var gpscale = .4;
            draw_gamepad_button(gpdraw,gpscale,xd-w+gpscale*10,yd-h+gpscale*10,1)
        }   
    }
    
    if scale != 1 {
        scale += tween(scale,1,.35);
        if abs(scale-1) <= .005
            scale = 1;
    }
}
else instance_destroy();

