{
    "id": "7c71586e-9049-41a1-9c4d-2522dad26e50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_generic_keyboard_key",
    "eventList": [
        {
            "id": "9d21f3be-f330-4a03-bd72-ab579bf0fb84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c71586e-9049-41a1-9c4d-2522dad26e50"
        },
        {
            "id": "1001219d-fefa-4c3b-9d9c-d40b46771c19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7c71586e-9049-41a1-9c4d-2522dad26e50"
        },
        {
            "id": "b7772e6c-d652-45b9-9798-845908dfdb7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7c71586e-9049-41a1-9c4d-2522dad26e50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}