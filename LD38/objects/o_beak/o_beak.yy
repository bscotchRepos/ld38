{
    "id": "7fceb301-0a4d-471e-9205-e706bee5c0b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_beak",
    "eventList": [
        {
            "id": "4f00e06f-75f7-473a-ac41-4a865bd649d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7fceb301-0a4d-471e-9205-e706bee5c0b2"
        },
        {
            "id": "bcfe16df-03c5-4d0f-8348-548e7a094c72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7fceb301-0a4d-471e-9205-e706bee5c0b2"
        },
        {
            "id": "1cf374ea-6fef-4efc-9787-f3145eb9c7e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7fceb301-0a4d-471e-9205-e706bee5c0b2"
        },
        {
            "id": "ef9c4cd9-7831-421b-bdc6-3a3ec02b9bcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7fceb301-0a4d-471e-9205-e706bee5c0b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "df5a8d1c-56b5-3c60-b1d4-2005f4b690f8",
    "visible": true
}