var corner_size = min(height*.4,width*.4);
draw_set_alpha(bg_alpha);
draw_roundrect_color_ext(x-width*.5,y-height*.5,x+width*.5,y+height*.5,corner_size,corner_size,bg_color,bg_color,false)
draw_set_alpha(1);

font_set(fnt_header,fa_left,fa_middle);

var margin = 10;
var txt_scale = min(
	(height-2*margin)/string_height("W"),
	clamp((width-2*margin)/(string_width(my_text+ "|") ), .01, 1) )

var txtwidth = string_width(my_text + "|")*txt_scale;

var txt_display = my_text;

if selected {
	cursor_flash_timer = max(0,cursor_flash_timer-1/room_speed);
	if cursor_flash_timer <= 0 {
		cursor_show = !cursor_show;
		cursor_flash_timer = .3+.6*cursor_show;
	}
	if cursor_show { txt_display += "|"; }
}

draw_text_transformed_colour(x-txtwidth*.5,y,txt_display,txt_scale,txt_scale,0,text_color,text_color,text_color,text_color,1);