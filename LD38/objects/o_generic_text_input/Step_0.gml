var clicked_off = false;
var clicked		= false;

if gamepad_key != -1 && CURRENT_GAMEPAD > 0 {
	if gamepad_hotkey_pressed(gamepad_key) {
		clicked = true;
	}
}

var d;
for (d = 0; d < 5; d++) {
    if device_mouse_check_button_pressed(d, mb_left) {
        var dx = device_mouse_x_to_gui(d);
        var dy = device_mouse_y_to_gui(d);
		
        if abs( x-dx ) < width*.5 && abs( y-dy ) < height*.5 {
            clicked = true;			
        }
		else {
			clicked_off = true;			
		}
    }
}

if clicked_off { clicked = false; }
	
if clicked_off && selected {
	event_user(0);
}
	
if clicked && active {
	if !selected {
		selected = true;
		aud_play_sound(snd_menuclick,false);
		
		var me = id;
		var to_deactivate = deactivated_items;
		with o_interface_item {
			if id != me {
				ds_list_add(to_deactivate, id);
			}
		}
		deactivate_list_of_instances(deactivated_items);	
		
		my_keyboard = spawn_generic_keyboard(keyboard_layout, "", GUIHEIGHT*.4, GUIWIDTH*.9, snd_click, fnt_header, c_white, 0, c_white, -1, c_black, .8, c_white, c_gray, max_characters, false);
		keyboard_string = my_text;
		my_keyboard.input = my_text;		
    }
}

if selected {
	if keyboard_check_pressed(vk_enter) {
		aud_play_sound(snd_menuclick,false);
		event_user(0);
	}
}

if instance_exists(my_keyboard) {
	my_text = my_keyboard.input;
}
else if selected {
	my_text = string_copy(keyboard_string,1,max_characters);
}