/// Deselect self
with my_keyboard { instance_destroy(); }
my_keyboard = noone;
for ( var i = 0; i < ds_list_size(deactivated_items); i++){
	instance_activate_object(deactivated_items[|i]);
}
ds_list_clear(deactivated_items);
selected = false;