event_inherited();
active = true;
selected = false;
deactivated_items = ds_list_create();
my_keyboard = noone;

cursor_flash_timer = 0;
cursor_show = false;

// Modifiable things (to set when spawning the textbox)
bg_color = c_black;
bg_alpha = .8;
text_color = c_white;
max_characters = 18;
my_text = "Text";
width = 100;
height = 50;
gamepad_key = -1;

keyboard_layout[0,0] = "1234567890-=~b"
keyboard_layout[0,1] = "qwertyuiop[]"
keyboard_layout[0,2] = "~casdfghjkl;\'"
keyboard_layout[0,3] = "~szxcvbnm,./~s"
keyboard_layout[0,4] = " "
keyboard_layout[1,0] = "!@#$%^&*()_+~b"
keyboard_layout[1,1] = "QWERTYUIOP{}"
keyboard_layout[1,2] = "~cASDFGHJKL:\""
keyboard_layout[1,3] = "~sZXCVBNM<>?~s"
keyboard_layout[1,4] = " "