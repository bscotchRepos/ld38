{
    "id": "443a2ef1-fa5e-434d-a975-f5483a06163b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_generic_text_input",
    "eventList": [
        {
            "id": "2b2f363f-0dad-408a-a714-07104e01585a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        },
        {
            "id": "64b2fc12-33ab-4517-a631-156006c531fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        },
        {
            "id": "0cdb7850-dd0d-4fb5-8fc2-c1c9264b30f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        },
        {
            "id": "3253df99-a9a4-46f3-8fb2-6f4ec6fab84d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        },
        {
            "id": "01333526-64cd-40de-94f5-03e4c4e068f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        },
        {
            "id": "50586364-0b02-4fc0-be61-2fd3798c362e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "443a2ef1-fa5e-434d-a975-f5483a06163b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "106af0f4-ec90-4b28-b874-6b88939c718d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}