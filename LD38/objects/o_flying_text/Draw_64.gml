font_set(fnt_header,fa_center,fa_middle);

timer += SECONDS_SINCE_UPDATE;

x = lerp(x,xtarg,.2);
y = lerp(y,ytarg,.2);

var start_scale = .25;
var end_scale = 1;

var ss = scalemod*ease_out_elastic(min(timer,maxtime),start_scale,end_scale-start_scale,maxtime);
shadowtext(x_to_gui(x),y_to_gui(y),string_replace_all(mytext," ","#"), ss, ss,rot,color,vis, 3)

if timer >= maxtime {
	vis -= 3/room_speed;
	if vis < 0
	   instance_destroy()
}

