//vspeed = random_range(-15,-10);
depth = -y*2
var d = depth;
with o_flying_text {
    d = min(depth-1,d);
}
depth = d;

rot = random_range(-25,25)
mytext = "";       
color = c_yellow
scalemod = 1;
dmg = -1;
vis = 1;

var flydir = point_direction(view_center_x(),view_center_y(),x,y)+random_range(-20,20);
var flydist = random_range(150,250);

xtarg = x+lengthdir_x(flydist,flydir);
ytarg = y+lengthdir_y(flydist,flydir);

maxtime = 1;
timer = 0;