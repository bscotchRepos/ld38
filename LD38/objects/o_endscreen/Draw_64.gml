alpha += SECONDS_SINCE_UPDATE;

draw_set_alpha(min(alpha,.7));
draw_rectangle_colour(-5,-5,GUIWIDTH+5,GUIHEIGHT+5,c_black,c_black,c_black,c_black,0);
draw_set_alpha(1);

draw_sprite_ext(sp_endscreen, 0, GUIWIDTH*.5, GUIHEIGHT*.25, 1, 1, 0, c_white, alpha);
font_set(fnt_reg,fa_center,fa_middle);
shadowtext(GUIWIDTH*.5, GUIHEIGHT*.25+100, "But you traveled for " + add_commas(round(distance)) + " meters, so that's good!", 1, 1, 0, c_white, alpha,4);
shadowtext(GUIWIDTH*.5, GUIHEIGHT*.25+150, "(Best Run: " + add_commas(round(BEST_DISTANCE)) + " Meters)", 1, 1, 0, c_white, alpha,4);

font_set(fnt_header,fa_left,fa_middle);
var conversion_mid = GUIHEIGHT*.5;
var tscale = ballooms_scale*.6;
var amount_text = add_commas(BALLOOMS);
var spacing = 10;
var totalwidth = sprite_get_width(sp_balloom_icon)+spacing+string_width(amount_text)*tscale;
draw_sprite_ext(sp_balloom_icon, 0, GUIWIDTH*.5-totalwidth*.5+sprite_get_width(sp_balloom_icon)*.5,conversion_mid,ballooms_scale,ballooms_scale,0,c_white,1);
shadowtext(GUIWIDTH*.5,conversion_mid,amount_text,tscale,tscale,0,c_white,1,2);

if alpha > 1.5 && !total_beaks_shown {
	if !ds_list_empty(beak_sizes_to_create) {
		// Find a spot to draw BEAKS.
		if random(1) <= .4 {	
			var beak_size_index = irandom(ds_list_size(beak_sizes_to_create)-1);
			
			var xp = GUIWIDTH*.5+choose(-1,1)*random_range(GUIWIDTH*.1,GUIWIDTH*.2);
			var yp = conversion_mid+random_range(-60,60);
			ds_list_add(beak_x, xp);
			ds_list_add(beak_y, yp);
			ds_list_add(beak_img, ds_list_find_value(beak_sizes_to_create, beak_size_index));
			ds_list_delete(beak_sizes_to_create, beak_size_index);
			play_sound_varied(snd_menuclick,10,false);
		}
	}
	else {
		total_beaks_shown = true;
		beak_show_time = alpha;
	}
}

if total_beaks_shown && alpha > beak_show_time+.5 && !beaks_converted {
	if random(1) <= .4 && !ds_list_empty(beak_img){
		var beaksize = ds_list_find_last(beak_img);
		var beak_value = BEAK_CONVERSION*(1+beaksize);
		BALLOOMS += beak_value;
		create_flying_text(
			ds_list_find_last(beak_x)+view_xview(), 
			ds_list_find_last(beak_y)+view_yview(),
			string(beak_value), c_white, .25, .1, 20);
		ds_list_delete(beak_x, ds_list_size(beak_x)-1);
		ds_list_delete(beak_y, ds_list_size(beak_y)-1);
		ds_list_delete(beak_img, ds_list_size(beak_img)-1);
		play_sound_gv(snd_brightpickup,10,false);
		ballooms_scale = 1.3;
	}
	
	if ds_list_empty(beak_x) {
		for ( var i = 0; i < array_length_1d(BEAKS); i++){
			BEAKS[i] = 0;
		}	
		saveload_progress(true);
		beaks_converted = true;
		beak_convert_time = alpha;
	}
}

with o_flying_text {
	var ms = 0;
	with o_camera_new { ms = movespeed; }
	x += ms * SLOMO_SECONDS;
}

for ( var i = 0; i < ds_list_size(beak_x); i++){
	draw_sprite_ext(sp_beak,beak_img[|i],beak_x[|i],beak_y[|i],.9+.1*sin(i),.9+.1*sin(i),37*i,c_white,1);
}

if alpha > 1 && !instance_exists(o_button) && beaks_converted && alpha > beak_convert_time+.5{
	instance_create(GUIWIDTH*.75,  GUIHEIGHT*.7, button_end_retry );
	instance_create(GUIWIDTH*.75, GUIHEIGHT*.8, button_end_upgrades );
	instance_create(GUIWIDTH*.75, GUIHEIGHT*.9, button_pause_main );	
	
	with o_button {
		set_depth_to_top();
		scale = 0;
	}
}

ballooms_scale = lerp(ballooms_scale,1,.3);

draw_sprite_ext(sp_dregume_dead,0,GUIWIDTH*.25,GUIHEIGHT*.75,1,1,0,c_white,alpha-.5);