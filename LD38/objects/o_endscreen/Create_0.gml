alpha = -.5;

distance = METERS;

with o_interface_item { instance_deactivate_object(id); }

total_beaks_shown = false;
beaks_converted = false;
beak_x = ds_list_create();
beak_y = ds_list_create();
beak_img = ds_list_create();

beak_show_time = 0;
beak_convert_time = 0;
ballooms_scale = 1;

beak_sizes_to_create = ds_list_create();

beaks_shown = 0;
beaks_converted = false;
for ( var i = 0; i < array_length_1d(BEAKS); i++){
	if BEAKS[i] > 0 {
		beaks_converted = false;
	}
	repeat(BEAKS[i]) {
		ds_list_add(beak_sizes_to_create, i);
	}
	beaks_shown[i] = 0;
}