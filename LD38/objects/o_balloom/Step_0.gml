event_inherited();

if collected {
	BALLOOMS+= value;
	
	play_sound_pitched(snd_mildpickup,10,false, BALLOOM_PITCH);
	BALLOOM_PITCH_TIMER = 1;
	BALLOOM_PITCH = min(4, BALLOOM_PITCH + .15);

	instance_destroy();	
}
else{
	magnet_toward_player(upg_balloom_magnet);
}

if destroy
	instance_destroy();