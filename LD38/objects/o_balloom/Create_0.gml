event_inherited()

hitbox = 30;
airspeed = 0;
airdir = 0;
flung = false;
xscale = choose(1,-1);
blinking = 0;
balloom_strength = 0;
color = c_yellow;
value = 1;
size = 1; 
alarm[0] = 1;
mv_init(upgrade_get_effect(upg_balloom_magnet), -1, -1, -1, 0, 0, 0);

var strength_boost_chance = min(.50,(METERS-30)/200);
if random(1) < strength_boost_chance{
	balloom_strength ++;
}
strength_boost_chance = min(.25,(METERS-150)/500);
if random(1) < strength_boost_chance{
	balloom_strength ++;
}
switch balloom_strength {
	case 0:
		sprite_index = sp_balloom_weak;
		value = 1;
		break;
	case 1:
		sprite_index = sp_balloom;
		value = 5;
		break;
	case 2:
		sprite_index = sp_balloom_strong;
		value = 20;
		break;		
}