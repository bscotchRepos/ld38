{
    "id": "129f0189-ba2c-4ecd-a3e1-fab9e27d9d36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_balloom",
    "eventList": [
        {
            "id": "1804c006-2bdc-4ec8-847a-8815cd2356ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "129f0189-ba2c-4ecd-a3e1-fab9e27d9d36"
        },
        {
            "id": "24f25ae0-7345-40fc-8047-f5440cc115b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "129f0189-ba2c-4ecd-a3e1-fab9e27d9d36"
        },
        {
            "id": "9fed6f77-2b07-4a07-b618-7de9ca8837ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "129f0189-ba2c-4ecd-a3e1-fab9e27d9d36"
        },
        {
            "id": "b4877cf2-aff3-4c0b-a1aa-c8f1c6ad467b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "129f0189-ba2c-4ecd-a3e1-fab9e27d9d36"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a8963e9c-787e-4585-9901-3ff489b7219a",
    "visible": false
}