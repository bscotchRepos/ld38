{
    "id": "9f26e29f-a38a-4c82-b7b8-822c9fadd88a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_aura",
    "eventList": [
        {
            "id": "db0340e1-844c-4d9e-8015-ea5fba020e6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f26e29f-a38a-4c82-b7b8-822c9fadd88a"
        },
        {
            "id": "4f7587e4-9e16-46ef-8deb-7178e5a03510",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9f26e29f-a38a-4c82-b7b8-822c9fadd88a"
        },
        {
            "id": "cba3f751-939c-4662-a0ab-7d22d3fc5c51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9f26e29f-a38a-4c82-b7b8-822c9fadd88a"
        },
        {
            "id": "66f11cf2-6417-4e21-bf3f-5ace432149e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9f26e29f-a38a-4c82-b7b8-822c9fadd88a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}