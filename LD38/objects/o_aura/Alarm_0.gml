/// @description Insert description here
// You can write your code in this editor
if psys_can_emit{
	var v_depth = 0
	with owner{
		v_depth = depth + 1;
	}
	part_system_depth(part_sys,v_depth)
	echo("part_type created");
	part_particles_create(part_sys, 0,0, part_type, 1);
}