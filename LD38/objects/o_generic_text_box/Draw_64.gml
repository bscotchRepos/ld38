// Draw information about the character!
height = margins*2;
var text_space_width = width-2*margins;

for ( var i = 0; i < ds_list_size(my_text); i+=2 ){
	var the_text = my_text[|i];
	var is_header = my_text[|(i+1)];
	
	if is_header {
		draw_set_font(header_font);
		height += (string_height("W")*header_scale)+header_spacing_below;
	}
	else {
		font_set(body_font,body_alignment,fa_top);
		height += max(
			string_height("W")*body_scale,
			string_height_ext(the_text,FONT_SPACING,text_space_width/body_scale)*body_scale) + body_spacing_below; 
	}
}

draw_set_alpha(bg_alpha);
	draw_roundrect_colour_ext(
		x-width*.5, y-height*.5,
		x+width*.5, y+height*.5,
		corner_radius,corner_radius, bg_color, bg_color,0);
draw_set_alpha(1);

// DRAW THE TEXT!
var ypos = y-height*.5+margins;

for ( var i = 0; i < ds_list_size(my_text); i+=2 ){
	var the_text = my_text[|i];
	var is_header = my_text[|(i+1)];
	
	if is_header {
		var text_xpos = x;
		if header_alignment == fa_left {
			text_xpos -= width*.5-margins;
		}
		if header_alignment == fa_right {
			text_xpos += width*.5-margins;
		}
		
		font_set(header_font,header_alignment,fa_top);
		draw_text_transformed_colour(text_xpos, ypos, the_text ,header_scale,header_scale,0,header_color,header_color,header_color,header_color,1);
		ypos += (string_height("W")*header_scale)+header_spacing_below;
		
	}
	else {
		var text_xpos = x;
		if body_alignment == fa_left {
			text_xpos -= width*.5-margins;
		}
		if body_alignment == fa_right {
			text_xpos += width*.5-margins;
		}
		
		font_set(body_font,body_alignment,fa_top);
		draw_text_ext_transformed_colour(text_xpos, ypos, the_text,FONT_SPACING,text_space_width/body_scale,body_scale,body_scale,0,body_color,body_color,body_color,body_color,1);		
		ypos += (string_height_ext(the_text,FONT_SPACING,text_space_width/body_scale)*body_scale) + body_spacing_below; 
	}
}