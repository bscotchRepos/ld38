{
    "id": "d305fb07-8993-4e41-8170-b68840ab512c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_generic_text_box",
    "eventList": [
        {
            "id": "4e816996-6a84-405e-a6d5-08a53d235c83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d305fb07-8993-4e41-8170-b68840ab512c"
        },
        {
            "id": "c1c4e18d-4cc6-463d-91ca-57a42cf1b458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d305fb07-8993-4e41-8170-b68840ab512c"
        },
        {
            "id": "7b714f1f-f808-43ef-8364-7f794a117421",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d305fb07-8993-4e41-8170-b68840ab512c"
        },
        {
            "id": "4ac4b8ff-a85d-465e-9ec9-5f3653170908",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d305fb07-8993-4e41-8170-b68840ab512c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "106af0f4-ec90-4b28-b874-6b88939c718d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}