event_inherited();
width = GUIWIDTH*.5;
margins = 10;

header_font = fnt_header;
header_scale = 1;
header_spacing_below = 0;
header_alignment = fa_left;
header_color = c_white;

body_font = fnt_reg;
body_scale = 1;
body_spacing_below = 0;
body_alignment = fa_left;
body_color = c_white;

corner_radius = 20;

bg_color = c_black;
bg_alpha = .75;

height = 10; // This will get set dynamically.

my_text = ds_list_create();
// Format of my_text:
// [ text, type, text, type...]
// type is 0 for body or 1 for header
