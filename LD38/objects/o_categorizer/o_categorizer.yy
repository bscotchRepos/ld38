{
    "id": "24bb7c0d-35af-4351-9aeb-b8eda9bdb983",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_categorizer",
    "eventList": [
        {
            "id": "b536fc56-2868-41ac-95a7-32e6b4033002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24bb7c0d-35af-4351-9aeb-b8eda9bdb983"
        },
        {
            "id": "6866cf13-9f3c-45b6-9895-b9870a4d8b77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "24bb7c0d-35af-4351-9aeb-b8eda9bdb983"
        },
        {
            "id": "efb2e4c2-d7cd-47b8-a066-9bbc8e047256",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "24bb7c0d-35af-4351-9aeb-b8eda9bdb983"
        },
        {
            "id": "921bad31-bc84-424b-9e27-99f6acdff83e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "24bb7c0d-35af-4351-9aeb-b8eda9bdb983"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "106af0f4-ec90-4b28-b874-6b88939c718d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}