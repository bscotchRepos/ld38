if category_list != -1 && category_contents != -1 && id_names != -1 && id_sprites != -1 {

var halfwidth = width*.5;
var halfheight = height*.5;
var rectop = y-halfheight;
var recbottom = y+halfheight;
var recleft = x-halfwidth;
var recright = x+halfwidth;

scroll_check(0,recleft,rectop,recright,recbottom);

draw_set_alpha(.75);
draw_rectangle_colour(recleft,rectop,recright,recbottom,c_black,c_black,c_black,c_black,0);
draw_set_alpha(1);
draw_rectangle_colour(recleft,rectop,recright,recbottom,c_white,c_white,c_white,c_white,true);

var mxgui = -100;
var mygui = -100;
var clicked = false;

if mouse_check_button_pressed(mb_left) {
    mxgui = x_to_gui(mouse_x);
    mygui = y_to_gui(mouse_y);
    clicked = true;
}

// Draw the tiles
var side_margins = 5;

font_set(font,fa_left,fa_middle);

var yp = rectop+selector_height*.5+sc_offset[0,1];
var totalheight = 0;

var depth_indent = 10;
for ( var c = 0; c < ds_list_size(category_list); c++){
	var this_category = category_list[|c];
	var category_display = this_category;
	while string_count("/", category_display) > 0 {
		category_display = string_delete(category_display, 1, string_pos("/", category_display));
	}
	
	var category_depth = string_count("/",this_category);
	var category_left = recleft+depth_indent*category_depth+side_margins;
	var txt_prefix = "- ";
	if ds_list_contains(collapsed_folders, this_category) { txt_prefix = "+ "; }
	
	if yp >= rectop && yp <= recbottom {
		add_blend();
		draw_line_colour(category_left-side_margins+1,yp+selector_height*.5-1,recright,yp+selector_height*.5-1,c_white,c_black);
		normal_blend();	
		draw_text_to_fit_oneline(txt_prefix+category_display, width - selector_height-3*side_margins, selector_height, category_left, yp, .1, folder_maxscale, c_softgreen, 2);
	}
	
	if clicked {
	    // Select it!
	    if point_in_rectangle(mxgui,mygui,recleft,rectop,recright,recbottom) {
	        if mygui > yp-selector_height*.5 && mygui < yp+selector_height*.5 {
	            aud_play_sound(snd_menuclick,false);							
	            // Collapse it! COLLAPSE ALL OF IT! Only applies to things further down the line.
				
				var collapsing = true;
				if ds_list_contains(collapsed_folders, this_category) {
					collapsing = false;
				}
				
				if collapsing { ds_list_add_nodupe(collapsed_folders, this_category); }
				else { ds_list_delete_item(collapsed_folders, this_category); }				
	        }
	    }
	}
	
	yp += selector_height;
	totalheight += selector_height;
	
	if ds_list_contains(collapsed_folders, this_category) {
		// Skip ahead until we find it!
		var depth_reached = false;
		for ( var j = c+1; j < ds_list_size(category_list) && !depth_reached; j++){
			var check_category = category_list[|j];
			var check_category_depth = string_count("/",check_category);
			if check_category_depth <= category_depth {
				depth_reached = true;
				c = j-1;
			}
		}
	}	
	else {	
		var these_items = category_contents[?this_category];
		for ( var i = 0; i < ds_list_size(these_items); i++){
			var this_item = these_items[|i];
		    // Draw the tile sprite and the name of the tile
		    var item_xdraw = category_left+selector_height*.5+depth_indent;
			var selector_left = item_xdraw-selector_height*.5;
			var selector_width = recleft+width-selector_left;
			var itemtext_xdraw = item_xdraw + selector_height*.5 + side_margins;
			var text_width = selector_width-selector_height-3*side_margins;
		    var spdraw = id_sprites[?this_item];
		    var sp_size = selector_height;

		    if clicked {
		        // Select it!
		        if point_in_rectangle(mxgui,mygui,recleft,rectop,recright,recbottom) {
		            if mygui > yp-selector_height*.5 && mygui < yp+selector_height*.5 {
		                aud_play_sound(snd_menuclick,false);
		                selected_id = this_item;
		            }
		        }
		    }
			
			if yp >= rectop && yp <= recbottom {
			    if selected_id == this_item {
			        draw_rectangle_colour(selector_left,yp-selector_height*.5+1,selector_left+selector_width,yp+selector_height*.5-1,c_aqua,c_aqua,c_aqua,c_aqua,1);
			    }
    
			    if sprite_exists(spdraw) {
					var spsize = selector_height-2;
			        var sprite_drawscale = min(spsize/sprite_get_width(spdraw), spsize/sprite_get_height(spdraw));
			        draw_sprite_centered_ext(spdraw, 0, item_xdraw, yp, sprite_drawscale, sprite_drawscale, 0, c_white, 1);
			    }
    
			    draw_text_to_fit_oneline(id_names[?this_item], text_width, selector_height, itemtext_xdraw, yp, .1, text_maxscale, c_white, 2);
			}
		    yp += selector_height;
			totalheight += selector_height;
		}	
	}
}
scroll_set_minmax(0,0,0,min(0,-totalheight+height*.5),0);

}