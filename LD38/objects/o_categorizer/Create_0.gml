// Needs:
// List of categories
// Map of lists of category contents (the list contains ID numbers)
// Map of names associated with the ID numbers
// Map of sprite IDs associated with the ID numbers
event_inherited();

selected_id = -1;

collapsed_folders = ds_list_create();
scroll_init(0,false,true,false,false,false,-1,-1,-1,-1,true,0,0,0,0);

// To set upon spawning.
category_list = -1;
category_contents = -1;
id_names = -1;
id_sprites = -1;
width = 200;
height = GUIHEIGHT;
selector_height = 30; // Vertical spacing
font = fnt_reg;
folder_maxscale = 1;
text_maxscale = 1;