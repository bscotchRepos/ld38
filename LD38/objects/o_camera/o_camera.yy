{
    "id": "3372387e-1620-4316-84d6-dab009e2d5df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_camera",
    "eventList": [
        {
            "id": "ffcd2d06-f625-465e-9f46-bc0ac7f5cc7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3372387e-1620-4316-84d6-dab009e2d5df"
        },
        {
            "id": "f9e7c06c-1438-42c7-b864-6e497507d9c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "3372387e-1620-4316-84d6-dab009e2d5df"
        },
        {
            "id": "319be087-c3a0-447b-909e-e7543a02b4c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3372387e-1620-4316-84d6-dab009e2d5df"
        },
        {
            "id": "84cbcdec-47b0-49da-b449-3d8c6d69713b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "3372387e-1620-4316-84d6-dab009e2d5df"
        },
        {
            "id": "58b2dad7-15a5-4bdf-84cf-2593b750f2fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3372387e-1620-4316-84d6-dab009e2d5df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}