audio_falloff_set_model(audio_falloff_linear_distance)
audio_listener_orientation(0,0,-1000,0,1,0)

my_targets = ds_list_create();
shake = 0;
xdest = 0;
ydest = 0;
target_weights = ds_list_create();

breath_xtimer = random(10);
breath_ytimer = random(10);

base_wview = view_wview();
base_hview = view_hview();
view_margin = .35;

xtargs = ds_list_create();
ytargs = ds_list_create();
movement_xoffset = 0;
movement_yoffset = 0;
num_targs = 5;

/*
view_visible[1] = true;
view_wport[1] = GUIHEIGHT;
view_hport[1] = GUIWIDTH*.5;
view_angle[1] = 90;
view_xport[1] = GUIWIDTH*.5;
view_yport[1] = 0;

/* */
/*  */
