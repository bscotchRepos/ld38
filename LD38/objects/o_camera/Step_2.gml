xdest = 0;
ydest = 0;

var xmin = 1000000;
var xmax = -1000000;
var ymin = 1000000;
var ymax = -1000000;

var total_weight = 0;

if ds_list_empty(my_targets) {
    xdest = x;
    ydest = y;
}
else {
    for ( var i = ds_list_size(my_targets)-1; i >= 0 ; i--){
        var this_targ = my_targets[|i];
        if instance_exists(this_targ) {
            total_weight += target_weights[|i];
            var yt = this_targ.y-this_targ.z*.3+this_targ.yoffset;
            xmin = min(xmin, this_targ.x);
            xmax = max(xmax, this_targ.x);
            ymin = min(ymin, yt);
            ymax = max(ymax, yt);
        }
        else {
            ds_list_delete(my_targets, i);
            ds_list_delete(target_weights, i);
        }
    }
    
    for ( var i = 0; i < ds_list_size(my_targets); i++) {
        var yt = my_targets[|i].y-my_targets[|i].z*.3+my_targets[|i].yoffset;
        var this_prop = target_weights[|i] / total_weight;
        xdest += my_targets[|i].x * this_prop;
        ydest += yt * this_prop;
    }
}

// Watch for movement and adjust offsets
if room == rm_gameplay {
	if !(os_type == os_windows || os_type == os_macosx) {
	    ds_list_insert(xtargs, 0, xdest);
	    ds_list_insert(ytargs, 0, ydest);
	    while ds_list_size(xtargs) > num_targs {
	        ds_list_delete(xtargs, ds_list_size(xtargs)-1);
	        ds_list_delete(ytargs, ds_list_size(ytargs)-1);
	    }
    
	    if ds_list_size(xtargs) > 3 {
	        var dd = get_dirdist(
	            ds_list_find_value(xtargs,ds_list_size(xtargs)-1), ds_list_find_value(ytargs,ds_list_size(ytargs)-1),
	            ds_list_find_value(xtargs,0), ds_list_find_value(ytargs,0));
        
	        movement_xoffset = lerp(movement_xoffset, lengthdir_x(dd[1]*10, dd[0]), .01);
	        movement_yoffset = lerp(movement_yoffset, lengthdir_y(dd[1]*10, dd[0]), .01);
	    }
	    else {
	        movement_xoffset = lerp(movement_xoffset,0,.05);
	        movement_yoffset = lerp(movement_yoffset,0,.05);
	    }
	    x += tween(x,xdest,.25);
	    y += tween(y,ydest,.25);
	}
	else {
	    // on desktop, track the mouse OR GAMEPAD.
	    if instance_number(o_player) == 1 {
	        if CURRENT_GAMEPAD < 0 {
	            var mx = window_mouse_get_x()/window_get_width();
	            var my = window_mouse_get_y()/window_get_height();
	            var mx_offset = mx-.5;
	            var my_offset = my-.5;
	            var mx_dirdist = get_dirdist(0,0,clamp(mx_offset/.25,-1,1),clamp(my_offset/.25,-1,1));
            
	            movement_xoffset = lerp(movement_xoffset, lengthdir_x(mx_dirdist[1], max(.35, mx_dirdist[0]))*120, .02);
	            movement_yoffset = lerp(movement_yoffset, lengthdir_y(mx_dirdist[1], max(.35, mx_dirdist[0]))*120, .02);
            
	        }
	        else {
	            var gp_axis = get_gamepad_axis(CURRENT_GAMEPAD, gp_axisrh, gp_axisrv, false);
	            var gp_dirdist = get_dirdist(0, 0, gp_axis[0], gp_axis[1]);
            
	            if gp_dirdist[1] >= GAMEPAD_DEADZONE {
	                var m_dist = gp_dirdist[1]*120;
	                movement_xoffset = lerp(movement_xoffset, lengthdir_x(m_dist, gp_dirdist[0]), .04);
	                movement_yoffset = lerp(movement_yoffset, lengthdir_y(m_dist, gp_dirdist[0]), .04);
	            }
	        }
	    }
		else if !instance_exists(o_player) {
			movement_xoffset = lerp(movement_xoffset,0,.05);
	        movement_yoffset = lerp(movement_yoffset,0,.05);
		}
    
	    x += tween(x,xdest,.35);
	    y += tween(y,ydest,.35);
	}
}

// Adjust the zoom level based on the view.
var wview_targ = max(base_wview, (xmax-xmin)+view_margin*view_hview());
var hview_targ = max(base_hview, (ymax-ymin)+view_margin*view_hview());
var max_viewscale = max(wview_targ/base_wview, hview_targ/base_hview);

camera_set_view_size(view_camera[0], view_wview()+tween(view_wview(),base_wview*max_viewscale,.1), view_hview()+tween(view_hview(),base_hview*max_viewscale,.1))

breath_xtimer += SECONDS_SINCE_UPDATE;
breath_ytimer += SECONDS_SINCE_UPDATE*.7;

camera_set_view_pos(
	view_camera[0], 
	x-view_wview()*.5+random_range(-shake,shake)+8*sin(breath_xtimer)*REZ+movement_xoffset,
	y-view_hview()*.5+random_range(-shake,shake)+8*sin(breath_ytimer)*REZ+movement_yoffset);

shake = max(shake-15*SECONDS_SINCE_UPDATE,0);

///set audio position
audio_listener_position(x,y,0)