{
    "id": "759a153a-777f-40ef-b19a-b80da335bde2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_end_retry",
    "eventList": [
        {
            "id": "128506bd-0ca2-4020-a838-5b4e57681c0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "759a153a-777f-40ef-b19a-b80da335bde2"
        },
        {
            "id": "4ac4e9c5-063c-4f36-9f37-1ee72a8c6cdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "759a153a-777f-40ef-b19a-b80da335bde2"
        },
        {
            "id": "395387b7-e811-44c6-89fa-b28bdf6f59dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "759a153a-777f-40ef-b19a-b80da335bde2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
    "visible": true
}