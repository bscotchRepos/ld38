randomize();

layer_force_draw_depth(true,0);
init_bscotch();
init_balance();
init_upgrades();
resolution_set();

globalvar BALLOOM_PITCH; BALLOOM_PITCH = 1;
globalvar BALLOOM_PITCH_TIMER; BALLOOM_PITCH_TIMER = 0;
globalvar BEAK_CONVERSION; BEAK_CONVERSION = 5;
globalvar BEST_DISTANCE; BEST_DISTANCE = 0;
globalvar BALLOOMS; BALLOOMS = 0;
globalvar METERS; METERS = 0;
globalvar BEAKS; BEAKS = 0;
for ( var i = 0; i < sprite_get_number(sp_beak); i++){
	BEAKS[i] = 0;
}
saveload_progress(false);
instance_create(0,0,o_particle_manager);
room_goto(rm_mainmenu);

