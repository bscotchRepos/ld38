// Check for particles to kill if their instances die.
for ( var i = ds_list_size(particle_systems)-1; i >= 0; i--) {
    var ps = particle_systems[|i];
    if ds_map_exists(particle_instances, ps) {
        if !instance_exists(particle_instances[?ps]) {
            if !ds_list_contains(ps_death, ps) {
                psys_purge_ps(ps);
            }
        }
    }
}

// Handle destruction of particle systems
for ( var i = ds_list_size(ps_death_timers)-1; i >= 0; i--){
    var the_timer = ps_death_timers[|i];
    the_timer -= SLOMO_SECONDS;
    if the_timer <= 0 {
        psys_purge_ps( ps_death[|i] );
    }
    else ds_list_replace(ps_death_timers, i, the_timer);
}

// Particle Animations
part_anim_timer += SLOMO_SECONDS;
if part_anim_timer >= 1/room_speed {
    can_emit = true;
    part_anim_timer = 0;
    // Update all particles
    for ( var i = 0; i < ds_list_size(particle_systems); i++){
        part_system_update(particle_systems[|i]);
    }
}
else can_emit = false;

