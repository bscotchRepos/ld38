{
    "id": "d20287ad-e5b5-48dc-b171-835760fbafd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_particle_manager",
    "eventList": [
        {
            "id": "a6f8cf81-4142-4797-b4df-3110360324c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d20287ad-e5b5-48dc-b171-835760fbafd6"
        },
        {
            "id": "3e8ccd72-6379-4cbb-893d-5c4756b66efa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d20287ad-e5b5-48dc-b171-835760fbafd6"
        },
        {
            "id": "5378f61b-c79c-4dc1-b058-ca2d6d734a0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d20287ad-e5b5-48dc-b171-835760fbafd6"
        },
        {
            "id": "ebb4fc95-4855-4d1b-b9b1-ba42f053f9e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d20287ad-e5b5-48dc-b171-835760fbafd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}