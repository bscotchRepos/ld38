{
    "id": "859eccb3-5818-4bce-925f-c3cbc88a312b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shmaloo",
    "eventList": [
        {
            "id": "009344ac-8fc5-4c54-a42a-341b7149f303",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "859eccb3-5818-4bce-925f-c3cbc88a312b"
        },
        {
            "id": "edbf14a9-290f-423f-b1ad-83f9c7c555a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "859eccb3-5818-4bce-925f-c3cbc88a312b"
        },
        {
            "id": "29039fbc-5902-43fe-a05c-f58b74fb970f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "859eccb3-5818-4bce-925f-c3cbc88a312b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4db65503-e508-4afc-a03a-09ee2df6bc6c",
    "visible": true
}