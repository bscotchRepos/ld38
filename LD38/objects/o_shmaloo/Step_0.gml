event_inherited();
if collected{
	with o_player{
		var recover_amount = floor(hpmax*HP_RECOVERY);
		hp = min (hp + recover_amount, hpmax);
		create_flying_text(x,y,add_commas(recover_amount), c_lime, .5, .5, 150);
		play_sound_pitched(snd_coinpickup, 10, false, 2);
	} 
	instance_destroy();
}