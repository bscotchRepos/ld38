{
    "id": "59777e2f-e46f-454e-9be4-f4af57a9abbd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tips",
    "eventList": [
        {
            "id": "f34eb42d-b6a7-468d-8007-7cf5618b64f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "59777e2f-e46f-454e-9be4-f4af57a9abbd"
        },
        {
            "id": "09d1b199-856b-4a24-afca-01c0bbfcba30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "59777e2f-e46f-454e-9be4-f4af57a9abbd"
        },
        {
            "id": "5cf3a51c-e8bb-40ea-82d3-6a66e7a195ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "59777e2f-e46f-454e-9be4-f4af57a9abbd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "898e09a5-0650-4bd7-9093-d7febc2760ca",
    "visible": true
}