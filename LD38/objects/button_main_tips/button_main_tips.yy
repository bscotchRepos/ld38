{
    "id": "e300c749-79de-406c-ba36-6686b4ef761d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_main_tips",
    "eventList": [
        {
            "id": "4dbee3cd-1dfc-4565-aaa0-aab9a4dfb26b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e300c749-79de-406c-ba36-6686b4ef761d"
        },
        {
            "id": "912beddf-1188-4689-94d5-7591b49c6c02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e300c749-79de-406c-ba36-6686b4ef761d"
        },
        {
            "id": "6392ccfe-2b21-4023-a517-ee40999d5935",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e300c749-79de-406c-ba36-6686b4ef761d"
        },
        {
            "id": "eae2f1d9-668e-4973-a117-b0906748a84a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e300c749-79de-406c-ba36-6686b4ef761d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
    "visible": true
}