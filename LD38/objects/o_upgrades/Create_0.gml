create_offscreen(o_background);

anim_timer = 0;
SPD = 1;

for (var i = 0; i < array_height_2d(UPGRADES); i++){
	with create_offscreen(o_upgrade_buy) {
		my_upgrade = i;
	}
}

scroll_init(0,false,true,false,false,false,ord("W"),ord("S"),-1,-1,true,0,0,0,0);