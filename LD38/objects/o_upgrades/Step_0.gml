update_delta();

if keyboard_check_pressed(vk_escape) {
	room_goto(rm_mainmenu);
	exit;
}

var yp = 220 + sc_offset[0,1];

var spacing = 10;
var ht = 0;
with o_upgrade_buy {
	x = GUIWIDTH*.5;
	yp += height*.5;
	y = yp;
	yp += height*.5 + spacing;
	ht += height+spacing;
}
scroll_set_minmax(0, 0, 0, -ht, 0);
scroll_check(0, 0, 0, GUIWIDTH, GUIHEIGHT );