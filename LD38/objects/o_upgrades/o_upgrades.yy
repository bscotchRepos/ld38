{
    "id": "8e73d1d6-3ca7-4c83-962b-294d7317877d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_upgrades",
    "eventList": [
        {
            "id": "be5823c8-5fc3-4fbe-8361-60f014e682f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8e73d1d6-3ca7-4c83-962b-294d7317877d"
        },
        {
            "id": "63b7c003-d6e7-4e43-9fa8-44f219f92ca1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e73d1d6-3ca7-4c83-962b-294d7317877d"
        },
        {
            "id": "ad644ddc-5214-4e60-9508-194a9fa70beb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e73d1d6-3ca7-4c83-962b-294d7317877d"
        },
        {
            "id": "c262905d-4aa3-4034-8b31-3a149a0e2029",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8e73d1d6-3ca7-4c83-962b-294d7317877d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}