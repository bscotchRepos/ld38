anim_timer += 2*SLOMO_SECONDS;
font_set(fnt_header,fa_center,fa_middle);
draw_sprite_ext(sp_flex,0,GUIWIDTH*.5,120+sc_offset[0,1],
	.95+.04*sin(anim_timer)+.01*sin(2*(anim_timer+1)),
	.95+.04*sin(1.5*(anim_timer+1))+.01*sin(.5*(anim_timer+6.3)),
	5*sin(anim_timer+.2),c_white,1);
	
// shadowtext(GUIWIDTH*.5,120+sc_offset[0,1],"FLEX YOUR DREGUME!",
// 	.9+.05*sin(anim_timer)+.05*sin(2*(anim_timer+1)),
// 	.9+.05*sin(1.5*(anim_timer+1))+.05*sin(.5*(anim_timer+6.3)),
// 	5*sin(anim_timer+.2),c_white,1,5);

//font_set(fnt_reg,fa_right,fa_top);
//shadowtext(GUIWIDTH-5,5, add_commas(BALLOOMS) + " Ballooms",1,1,1,c_white,1,2);

var icons = 0;
var text = 0;
var icon_size = 40;
var tsc = .4;
font_set(fnt_header,fa_left,fa_middle);

icons[0] = sp_balloom_icon; text[0] = add_commas(BALLOOMS);
icons[1] = sp_beak; text[1] = add_commas(get_total_beaks());

var tw = 0;
for ( var i = 0; i < array_length_1d(text); i++){
	tw = max(tw, string_width(text[i])*tsc);
}
var collectwidth = tw+icon_size+15;
draw_sprite(sp_whatyoubecollectin,0,collectwidth,0);

var vertspacing = icon_size;
for ( var i = 0; i < array_length_1d(icons); i++){
	var icon_scale = min(icon_size/sprite_get_width(icons[i]), icon_size/sprite_get_height(icons[i]));
	draw_sprite_ext( icons[i], 0, icon_size*.5+5, icon_size*.5+5+vertspacing*i, icon_scale, icon_scale, 0, c_white, 1 );
	draw_text_transformed_colour(icon_size+10, icon_size*.5+5+vertspacing*i, text[i], tsc, tsc, 0, c_black,c_black,c_black,c_black, .7);
}