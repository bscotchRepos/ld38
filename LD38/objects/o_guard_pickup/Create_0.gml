echo("created o_guard_pickup")
event_inherited();
part_sys_2 = psys_create_system();
part_system_position(part_sys_2, x, y);
particle1 = psys_create_type(part_sys_2); 
part_type_shape(particle1,pt_shape_star);
part_type_size(particle1,2,2,-0.03,0);
part_type_color2(particle1,16711680,4259584);
part_type_alpha3(particle1,0.70,0.61,0.06);
part_type_orientation(particle1,0,359,0.10,0,0);
part_type_blend(particle1,1);
part_type_life(particle1,20,20);

pulse_timer = 0;
pulse_timer_max = .3;