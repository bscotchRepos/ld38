event_inherited();
if psys_can_emit(){
	pulse_timer += SLOMO_SECONDS;
	if pulse_timer > pulse_timer_max{
		pulse_timer = 0;
		echo("creating star particle")
		part_particles_create(part_sys_2,0,0,particle1,2);
	}
}

if collected{
	with o_player{
		anim_bones[bone_guard, anim_active] = true;
	}
	instance_destroy();
}