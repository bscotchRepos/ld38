{
    "id": "7c61605d-77f3-45de-a58c-1427712d6f5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_guard_pickup",
    "eventList": [
        {
            "id": "771da070-6a8b-4987-9fb2-3e4a129ac373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c61605d-77f3-45de-a58c-1427712d6f5f"
        },
        {
            "id": "4b603c8e-7954-4a65-bb41-9dfcb7967614",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7c61605d-77f3-45de-a58c-1427712d6f5f"
        },
        {
            "id": "0111bec8-9fe9-45eb-94e6-0effae7818cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7c61605d-77f3-45de-a58c-1427712d6f5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b5a94ee-0a4a-41f2-97b5-a3ff73f16411",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}