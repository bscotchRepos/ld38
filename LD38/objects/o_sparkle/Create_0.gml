/// @description Insert description here
// You can write your code in this editor
ps = psys_create_system()
pt = part_type_create()
part_system_depth(ps,-y*2)

part_type_shape(pt, pt_shape_flare)
part_type_blend(pt, true)
part_type_size(pt, .2,.3,0,.05)
part_type_speed(pt, 0,0,0,0)
part_type_orientation(pt,0,360,1,0,0)
part_type_direction(pt,0,360,0,0)
part_type_color2(pt,make_color_rgb(202,149,50),c_black)
part_type_life(pt,.5*room_speed,.75*room_speed)
psys_assign_type_owner(pt,ps);

part_particles_create(ps,x,y,pt,5)
psys_destroy_ps(ps,.75);
instance_destroy();
