my_layer = layer_create(0);
button_layer = layer_create(-1);

layer_add_instance(my_layer, id);

timer = 0;
instance_deactivate_object(o_interface);
instance_deactivate_object(o_interface_item);

spawned_buttons = ds_list_create();

submenus = ds_map_create();

main_buttons = ds_list_create();
ds_list_add(main_buttons, button_pause_main);
ds_map_add_list(submenus, "main", main_buttons);

current_menu = "";

fn_pause_change_submenu("main");

depth = 10;