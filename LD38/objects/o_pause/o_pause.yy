{
    "id": "6c22b74a-6f45-4100-a82f-84cbd84f433e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pause",
    "eventList": [
        {
            "id": "31401ced-d337-493d-9cf3-d1ab6fa8bf3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c22b74a-6f45-4100-a82f-84cbd84f433e"
        },
        {
            "id": "128489d9-7b22-4ec5-a88f-29c0de41364c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6c22b74a-6f45-4100-a82f-84cbd84f433e"
        },
        {
            "id": "7f0f91ef-c7c3-4dd9-bc6f-88467ccfa156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c22b74a-6f45-4100-a82f-84cbd84f433e"
        },
        {
            "id": "3f06bb1d-6f21-479b-8d43-ad950ac3f9d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6c22b74a-6f45-4100-a82f-84cbd84f433e"
        },
        {
            "id": "6d1c25b8-9110-428f-9480-750213956be8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c22b74a-6f45-4100-a82f-84cbd84f433e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}