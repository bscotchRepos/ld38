var yp = GUIHEIGHT*.4;

for (var i = 0; i < ds_list_size(spawned_buttons); i++){
    with spawned_buttons[|i] {
        x = GUIWIDTH*.5;
        yp += height*.5;
        y = yp;
        yp += height*.5+5;
    }
}

