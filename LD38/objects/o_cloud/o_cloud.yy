{
    "id": "84e1f16c-886c-4d3b-bb81-40e3d667833c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cloud",
    "eventList": [
        {
            "id": "1ad779dc-1fa9-4c2f-92a8-d204d51534d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84e1f16c-886c-4d3b-bb81-40e3d667833c"
        },
        {
            "id": "1ceacaf2-896d-4d72-be53-12d323c5829b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84e1f16c-886c-4d3b-bb81-40e3d667833c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "def4b6d4-7db7-4c87-a3c1-ef5d93f09093",
    "visible": true
}