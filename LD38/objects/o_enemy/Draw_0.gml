var xdraw = x+shake_get();
var ydraw = y+shake_get();

blinking = blink(blinking);
rotation = mv_movedir;

while rotation < 0 { rotation += 360; }
while rotation > 360 { rotation -= 360; }

var rotation_draw = rotation;
if rotation > 90 && rotation < 270 { xscale = -1; }
else xscale = 1;

if (xscale == -1) { rotation_draw = -rotation+180; }

anim_set_bone_direction(bone_body, rotation_draw, 0, false);

anim_timer += SLOMO_SECONDS;
var flap_speed = 10;

anim_bones[bone_body, anim_subimage] = isblinking(blinking) || shake > 0;
anim_bones[bone_wing_front, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer)),0,2);
anim_bones[bone_wing_back, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer)),0,2);
anim_bones[bone_stunstar, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer)),0,2);
flash_update();
anim_set_bone_offset(bone_body, anim_yoffset, 20*power(sin(flap_speed*.5*anim_timer),2), 0, false);
anim_render(xdraw,ydraw,xscale,1,c_white,0);

//draw_circle_colour(xdraw,ydraw,hitbox,color,color,1);
shake_update();
shader_reset();

/*
var line_lenght = 80;
var dir_x = x + lengthdir_x(line_lenght, mv_movedir);
var dir_y = y + lengthdir_y(line_lenght, mv_movedir);
draw_line_color(x, y, dir_x, dir_y, c_white, c_white);