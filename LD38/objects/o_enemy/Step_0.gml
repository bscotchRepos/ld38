/// @description Insert description here
// You can write your code in this editor
var camera_ms = 0;
with o_camera_new { camera_ms = movespeed; }
x += camera_ms*SLOMO_SECONDS;

if instance_exists(o_player) && impacted == false && dying == false {
	var ptarg = instance_nearest(x,y,o_player);
	var tgdir = point_direction(x,y,ptarg.x,ptarg.y);
	mv_update_direction(tgdir);	
}

else impact_update();
anim_bones[bone_stunstar, anim_active] = impacted_timer > 0;

mv_accelerate(!impacted);

if hp <= 0 {
	if shake_started == false{
		shake = 10;
		shake_started = true;
	}
	death_timer -= SLOMO_SECONDS;
	if death_timer <= 0 {
		instance_destroy();		
	}	
}

mv_move(true);

if x < view_xview()-100 {
	despawn_timer -= SLOMO_SECONDS;
	if despawn_timer <= 0 {
		instance_destroy();
	}
}
else despawn_timer = 3;


