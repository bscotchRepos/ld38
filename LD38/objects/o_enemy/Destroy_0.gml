if hp <= 0 {
	play_sound_gv(snd_explode_mid,10, false);
	var balloom_number = 1+strength;
	var balloom_spawn_y = y;
	var balloom_spawn_x = x;
	var me = id;
	var player = instance_find(o_player,0);
	instance_create(x,y,o_explosion);
	repeat balloom_number {	
		with instance_create(balloom_spawn_x, balloom_spawn_y, o_beak){
			image_index = me.strength;
			airspeed = random_range(1000,2000);
			if instance_exists(player) {
				airdir = point_direction(x,y,player.x,player.y)+random_range(-60,60);
			}
			else airdir = irandom(360);
		}
		//beak_spawn_y += random_range(-10, 10);
		//beak_spawn_x += random_range(-10, 10);
	}
}