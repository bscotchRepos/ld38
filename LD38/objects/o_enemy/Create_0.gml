hitbox = 30;
color = c_red;

shake_init();
death_timer = .5;
dying = false;
shake_started = false;
impacted = false;
impacted_duration = 1;
impacted_timer = 0;
init_impact();
xscale = 1;
hp = 1;
blinking = 0;
flash_init();

anim_setup();
strength = 0;

// PIXELS FOR GROWTH!
var strength_boost_chance = min(.5, (view_xview()-10000)/30000);
if random(1) <= strength_boost_chance { strength++; }

strength_boost_chance = min(.5, (view_xview()-50000)/50000);
anim_timer = 0;
if random(1) <= strength_boost_chance { strength++; }
image_speed = 5;

switch strength {
	case 0:
		mv_init(DERB_MOVESPEED_BASE,-1,-1,100,0,0,0);
		bone_body = anim_add_bone(sp_dirb_basic,-1,1,1,0,0,0,0,0,bm_normal);
		bone_wing_front = anim_add_bone(sp_dirb_wing,bone_body,1,1,41,50,0,-1,0,bm_normal);
		bone_wing_back = anim_add_bone(sp_dirb_wing,bone_body,1,1,41,48,0,1,0,bm_normal);
		bone_stunstar = anim_add_bone(sp_stun,bone_body,.5,.5,20,-60,0,0,image_index,bm_normal)
		break;
	case 1:
		mv_init(DERB_MOVESPEED_BASE*1.5,-1,-1,120,0,0,0);
		bone_body = anim_add_bone(sp_dirb_medium,-1,1,1,0,0,0,0,0,bm_normal);
		bone_wing_front = anim_add_bone(sp_dirb_medium_wing,bone_body,1,1,37,37,0,-1,0,bm_normal);
		bone_wing_back = anim_add_bone(sp_dirb_medium_wing,bone_body,1,1,45,37,0,1,0,bm_normal);
		bone_stunstar = anim_add_bone(sp_stun,bone_body,.5,.5,20,-60,0,0,image_index,bm_normal);
		break;
	case 2:
		mv_init(DERB_MOVESPEED_BASE*.7,-1,-1,60,0,0,0);
		bone_body = anim_add_bone(sp_megadirb,-1,1,1,0,0,0,0,0,bm_normal);
		bone_wing_front = anim_add_bone(sp_megadirb_wing,bone_body,1,1,36,24,0,-1,0,bm_normal);
		bone_wing_back = anim_add_bone(sp_megadirb_wing,bone_body,1,1,40,22,0,1,0,bm_normal);
		bone_stunstar = anim_add_bone(sp_stun,bone_body,.5,.5,20,-60,0,0,image_index,bm_normal)
		break;
}

anim_bones[bone_stunstar, anim_active] = false;
level = 4*strength;
hp = round(upgrade_get_effect(upg_hp,level));
damage = round(upgrade_get_effect(upg_hp,level)*.3);

mv_set_direction(180+random_range(-30,30));

despawn_timer = 3;
anim_set_ordering();