{
    "id": "51b68a00-d9fd-4720-8035-8b909ec9ed4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_end_upgrades",
    "eventList": [
        {
            "id": "0cafa0f9-d230-4ae2-a552-5b80ac2cf672",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "51b68a00-d9fd-4720-8035-8b909ec9ed4d"
        },
        {
            "id": "9dc982db-aa6a-48e6-a254-6f79ba90edb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51b68a00-d9fd-4720-8035-8b909ec9ed4d"
        },
        {
            "id": "31f082b1-4292-428b-8486-bd274429ab85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "51b68a00-d9fd-4720-8035-8b909ec9ed4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e4cb250d-4566-4eda-b9d5-9faeb9a33372",
    "visible": true
}