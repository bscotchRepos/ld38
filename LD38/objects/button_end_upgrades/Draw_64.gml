event_inherited();
var txt = "UPGRADES";
var bcol = c_white;
if can_afford_upgrades {
	txt += " (!!)"
	bcol = c_aqua;
}
font_set(fnt_header, fa_center, fa_middle);
draw_text_transformed_color(x,y, txt, scale*.4, scale*.4, 0,bcol,bcol,bcol,bcol,1);