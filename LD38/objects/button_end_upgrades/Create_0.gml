event_inherited();
width = 500;
height = 100;

can_afford_upgrades = false;
for ( var i = 0; i < array_height_2d(UPGRADES) && !can_afford_upgrades; i++){
	if BALLOOMS >= upgrade_get_cost(i) {
		can_afford_upgrades = true;
	}
}