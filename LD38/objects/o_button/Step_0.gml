scale += tween(scale,1,.2);
if active {
	var clicked = false;
    if gamepad_key != -1 && CURRENT_GAMEPAD > 0 {		
		if gamepad_hotkey_pressed(gamepad_key) {
			clicked = true;
		}
	}
	
	var d;
    for (d = 0; d < 5; d++){
        if device_mouse_check_button_pressed(d,mb_left) {
            var dx = device_mouse_x_to_gui(d);
            var dy = device_mouse_y_to_gui(d);
            if abs(x-dx) < width*.5 && abs(y-dy) < height*.5 {
                clicked = true;
            }
        }
    }
	
	if clicked {
		aud_play_sound(snd_menuclick,false);
        scale = .9;
        alarm[0] = .15*room_speed;
	}
}

