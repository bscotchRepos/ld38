{
    "id": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_button",
    "eventList": [
        {
            "id": "60a6d917-93ae-4991-a47d-9771141edc93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "236f484b-4fe3-46ac-8afa-66a6feb95518"
        },
        {
            "id": "8d94f9eb-8979-4b86-a45c-e50c37838930",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "236f484b-4fe3-46ac-8afa-66a6feb95518"
        },
        {
            "id": "f266b66e-edf5-43a8-9189-24eaf21325fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "236f484b-4fe3-46ac-8afa-66a6feb95518"
        },
        {
            "id": "9fff04b0-8e61-458b-903c-f028669039fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "236f484b-4fe3-46ac-8afa-66a6feb95518"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "106af0f4-ec90-4b28-b874-6b88939c718d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "361c1f77-81a9-42d4-a10d-4ee5f0dec0f9",
    "visible": true
}