var xdraw = x+shake_get();
var ydraw = y+shake_get();
blinking = blink(blinking);

mv_movedir = mv_movedir mod 360;
rotation += angle_difference(mv_movedir, rotation)*.25*SPD;
rotation = rotation mod 360;

while rotation < 0 { rotation += 360; }

var rotation_draw = rotation;
if rotation > 90 && rotation < 270 {
	xscale = -1;
}
else xscale = 1;

if xscale == -1 { rotation_draw = -rotation+180; }

flash_update();

if inflated {
	draw_sprite_ext(sp_dragoom_spike,0,xdraw,ydraw,xscale*fatscale,fatscale,0,c_white,1);
	fatscale = lerp(fatscale,1,.35);
}
else {
	anim_set_bone_direction(bone_body, rotation_draw, 0, false);

	// WIGGLE SHITS
	anim_timer += SLOMO_SECONDS;
	var flap_speed = 20;

	anim_bones[bone_head, anim_subimage] = isblinking(blinking);
	anim_bones[bone_wing_butt_close, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer)),0,2);
	anim_bones[bone_wing_butt_far, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer)),0,2);
	anim_bones[bone_wing_head_close, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer+.5*pi)),0,2);
	anim_bones[bone_wing_head_far, anim_subimage] = clamp(floor(1.99+2*sin(flap_speed*anim_timer+.5*pi)),0,2);

	anim_set_bone_direction(bone_leg_back_close, 225+20*sin(10*(anim_timer+.5*pi)), 0, true);
	anim_set_bone_direction(bone_leg_front_close, 315+20*sin(10*anim_timer), 0, true);
	anim_set_bone_direction(bone_leg_front_far, 315+20*sin(10*(anim_timer+.5*pi)), 0, true);
	anim_set_bone_direction(bone_guard, anim_timer*30, 0, true);
	
	anim_set_bone_offset(bone_body, anim_yoffset, 5*power(sin(flap_speed*.25*anim_timer),2), 0, false);

	anim_set_bone_offset(bone_butt_close, anim_yoffset, -3*power(abs(sin(4*anim_timer)), .25), 0, false);
	anim_set_bone_offset(bone_butt_far, anim_yoffset, -3*power(abs(sin(4*(anim_timer+.125*pi))), .25), 0, false);

	
	anim_render(xdraw,ydraw,xscale,1,c_white,0);
}
shader_reset();
shake_update();
