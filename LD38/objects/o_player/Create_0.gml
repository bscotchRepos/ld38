hitbox_base = 40;
hitbox = hitbox_base;
hitbox_large_mod = 2;

shake_init();
flash_init();

blinking = 0;

fatscale = 1;

hpmax = upgrade_get_effect(upg_hp);
hp = hpmax;
dmg_cooldown = 0;

inflated = false;
inflate_timer = 0;
inflate_warned = false;
inflate_eff = noone;

movement_keys[0] = ord("D");
movement_keys[1] = ord("W");
movement_keys[2] = ord("A");
movement_keys[3] = ord("S");

followers = ds_list_create();

mv_init(upgrade_get_effect(upg_movespeed), -1, -1, -1, 0, 0, 0);

impacted = false;
impacted_duration = .5;
impacted_timer = 0;

// Animations
anim_timer = 0;
rotation = 0;
xscale = 1;
anim_setup();
bone_body = anim_add_bone(sp_dragoom, -1, 1, 1, 0, 0, 0, 0, 0, bm_normal);
bone_head = anim_add_bone(sp_dragoom_head, bone_body, 1, 1, 69, 45, 0, -3, 0, bm_normal);

bone_butt_far = anim_add_bone(sp_dragoom_butt, bone_body, 1, 1, 40, 35, 0, -1, 0, bm_normal);
bone_butt_close = anim_add_bone(sp_dragoom_butt, bone_body, 1, 1, 28, 40, 0, -2, 0, bm_normal);

bone_leg_front_close = anim_add_bone(sp_dragoom_feets, bone_body, 1, 1, 75, 64, 225, -5, 0, bm_normal);
bone_leg_front_far = anim_add_bone(sp_dragoom_feets, bone_body, 1, 1, 101, 64, 225, 5, 0, bm_normal);

bone_leg_back_close = anim_add_bone(sp_dragoom_feets, bone_body, 1, 1, 31, 65, 225, -5, 0, bm_normal);

bone_wing_head_close = anim_add_bone(sp_dragoom_earwings, bone_head, 1, 1, 19, 22, 0, -5, 0, bm_normal);
bone_wing_head_far = anim_add_bone(sp_dragoom_earwings, bone_head, 1, 1, 41, 16, 0, 5, 0, bm_normal);

bone_wing_butt_close = anim_add_bone(sp_dragoom_buttwings, bone_butt_close, 1, 1, 9, 13, 0, -5, 0, bm_normal);
bone_wing_butt_far = anim_add_bone(sp_dragoom_buttwings, bone_butt_far, 1, 1, 9, 10, 0, 5, 0, bm_normal);

bone_guard = anim_add_bone(sp_guard, bone_body, 1, 1, 0, -50, 0, -5, 0, bm_normal);
anim_bones[bone_guard, anim_active] = false;

anim_set_ordering();

anim_set_bone_direction(bone_wing_head_far, -15, 0, true);
anim_set_bone_direction(bone_wing_butt_far, -15, 0, true);
anim_set_bone_direction(bone_leg_front_close, 45, 0, true);
anim_set_bone_direction(bone_leg_front_far, 45, 0, true);
anim_set_bone_direction(bone_guard, 45, 0, true);


init_impact()