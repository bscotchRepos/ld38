if SPD > 0 {
	draw_progress_bar(GUIWIDTH*.5,30,865,40,c_lime,hp/hpmax,0);
	draw_sprite(sp_healthbar,0,GUIWIDTH*.5,30);

	font_set(fnt_header,fa_center,fa_middle);
	shadowtext(GUIWIDTH*.5-10,30,string(hp) + "/" + string(hpmax), .3, .3, 0, c_white, 1, 2);
}