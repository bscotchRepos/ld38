var camera_ms = 0;
with o_camera_new { camera_ms = movespeed; }
x += camera_ms*SLOMO_SECONDS;
dmg_cooldown = max(0,dmg_cooldown-SLOMO_SECONDS);

if inflate_timer > 0 {
	inflate_timer = max(0, inflate_timer-SLOMO_SECONDS);	
	if inflate_timer <= 0 {
		inflated = false;
		shake = 6;
		play_sound_varied(snd_thud_mild,10,false);
		inflate_warned = false;
		with inflate_eff {
			instance_destroy();
		}
	}
	else if inflate_timer < 1 {
		if inflate_warned == false {
			var me = id;
			inflate_eff = instance_create(x,y, o_aura)
			with inflate_eff {
				owner = me;
				psys_assign_instance(part_sys,me);
			}
			inflate_warned = true;
		}
	}
}

if keyboard_check(vk_space) && !inflated {
	if !ds_list_empty(followers) {
		// EAT A MALLOW!
		with ds_list_find_value(followers, 0) {
			instance_destroy();
		}
		ds_list_delete(followers,0);
		inflate_timer = upgrade_get_effect(upg_inflate_time);
		inflated = !inflated;
		shake = 5;
		if inflated {
			play_sound_varied(snd_flutter,10,false);
		}
		else play_sound_varied(snd_thud_mild,10,false);
	}
}

var mx = 0;
var my = 0;

for ( var i = 0; i < array_length_1d(movement_keys); i++){
	if keyboard_check(movement_keys[i]) {
		mx += lengthdir_x(1,i*90);
		my += lengthdir_y(1,i*90);
	}
}

var moving = false;
if point_distance(0,0,mx,my) > 0{
	var mdir = point_direction(0,0,mx,my);
	moving = true;
	mv_update_direction(mdir);
}

impact_update();

var movespeed_mod = 1+inflated*PLAYER_MOVESPEED_LARGEMOD;
mv_accelerate(moving*movespeed_mod);
mv_move(true);

if inflated { y -= PLAYER_UPWARD_FLOAT_SPEED*SLOMO_SECONDS; }

hitbox = hitbox_base*(1+inflated*hitbox_large_mod);

var me = id;

with o_floater {
	if point_distance(x,y,me.x,me.y) < (hitbox + me.hitbox) {
		var player_new_dir = point_direction(x, y, me.x, me.y);
		var player_new_speed = 1.5*IMPACT_SPEED_BASE*(1-upgrade_get_effect(upg_fatitude));
		with me {
			shake_camera(8);
			damage_player(hpmax*.25);
			apply_impact(player_new_speed,player_new_dir,impacted_duration);
		}
	}
}

with o_enemy {
	if point_distance(x,y,me.x,me.y) < (hitbox + me.hitbox) and impacted == false {
		var player_new_dir = point_direction(x,y,me.x,me.y);
		var player_new_speed = (strength*.5+1)*IMPACT_SPEED_BASE*(1-me.inflated)*(1-upgrade_get_effect(upg_fatitude));
		var enemy_new_dir = player_new_dir - 180;
		var enemy_new_speed = IMPACT_SPEED_BASE*(1+me.inflated*.9);
		echo("Enemy new speed is", enemy_new_speed)
		echo("")
		if !me.inflated {
			play_sound_varied(snd_thud_wet, 10, false);
			damage_player(damage);
		}
		with me {
			play_sound_varied(snd_thud_hard, 10, false);
			fatscale = 1.3;			
			apply_impact(player_new_speed,player_new_dir,impacted_duration);
		}
		shake_camera(4);
		apply_impact(enemy_new_speed,enemy_new_dir,impacted_duration);
		
		if me.inflated{
			var dmg_amt = round(upgrade_get_effect(upg_dirbpoke)*random_multiplier(.2));
			hp -= dmg_amt;
			create_flying_text(x,y,string(dmg_amt),c_white,.5,.5,150);
			flash = 1;						
		}
	}
}

// Move the tail of followers!
for ( var i = ds_list_size(followers)-1; i >= 0; i--){
	if !instance_exists(followers[|i]) {
		ds_list_delete(followers,i);
	}
}

var following = id;
for ( var i = 0; i < ds_list_size(followers); i++) {
	var this_instance = followers[|i];
	with this_instance {
		var fdir = point_direction(x,y,following.x,following.y);		
		follow_direction += angle_difference(fdir, follow_direction)*.25*SPD;
		x = lerp(x, following.x + lengthdir_x(hitbox+following.hitbox+10, fdir+180), .5*SPD);
		y = lerp(y, following.y + lengthdir_y(hitbox+following.hitbox+10, fdir+180), .5*SPD);
		while follow_direction < 0 { follow_direction += 360; }
		while follow_direction > 360 { follow_direction -= 360; }
		rotation = follow_direction;
	}
	following = this_instance;
}

y = lerp(y, clamp(y, GAMEPLAY_TOP+hitbox*.75, GAMEPLAY_BOTTOM-hitbox*.75), .35*SPD);

if x < view_xview() {
	impacted = true;
	shake_camera(2);
	apply_impact(IMPACT_SPEED_BASE, 0, .5);
}
if x > view_right(){
	impacted = true;
	shake_camera(2);
	apply_impact(IMPACT_SPEED_BASE, 180, .5);
}

if hp <= 0 {
	shake_camera(10);
	instance_create(x,y,o_explosion);
	instance_destroy();
	exit;
}