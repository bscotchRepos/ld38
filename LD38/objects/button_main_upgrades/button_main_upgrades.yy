{
    "id": "76875771-4494-4465-84dd-95512668e86b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_main_upgrades",
    "eventList": [
        {
            "id": "e6ce537c-54aa-41e6-bd31-845752323448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "76875771-4494-4465-84dd-95512668e86b"
        },
        {
            "id": "237987bc-7199-4002-9b09-ad4ae3b16372",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76875771-4494-4465-84dd-95512668e86b"
        },
        {
            "id": "851ea608-a7d1-42b2-b300-987c85e2dbae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "76875771-4494-4465-84dd-95512668e86b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "579ad86b-8a88-42cd-90bd-070fb03ec349",
    "visible": true
}