create_offscreen(o_background);
instance_create(GUIWIDTH*.5,GUIHEIGHT*.5,button_main_play);
instance_create(GUIWIDTH*.5,GUIHEIGHT*.75,button_main_upgrades);
var xdraw = GUIWIDTH - sprite_get_width(sp_button_basic)/2 - 20;
var ydraw = GUIHEIGHT - sprite_get_height(sp_button_basic)/2 - 20;
instance_create(xdraw, ydraw, button_main_tips);
play_music(mus_mainmenu,true);
anim_timer = 0;
SPD = 1;
depth = 10;

saveload_progress(true);