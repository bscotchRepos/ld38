{
    "id": "cd2a325c-d281-4b15-b99b-b70ff5954b58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_mainmenu",
    "eventList": [
        {
            "id": "50c63f9a-4843-4879-bc42-232effa435d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd2a325c-d281-4b15-b99b-b70ff5954b58"
        },
        {
            "id": "ab90fd50-b907-4359-84d3-0f85ddbe2e94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cd2a325c-d281-4b15-b99b-b70ff5954b58"
        },
        {
            "id": "7566c3a2-8aa4-434b-9622-533207a8ac90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cd2a325c-d281-4b15-b99b-b70ff5954b58"
        },
        {
            "id": "c5caca1e-1f75-4172-89d1-a4907d758aec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd2a325c-d281-4b15-b99b-b70ff5954b58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}