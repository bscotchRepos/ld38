anim_timer += 2*SLOMO_SECONDS;
font_set(fnt_header,fa_center,fa_middle);

draw_sprite_ext(sp_main_title,0,GUIWIDTH*.5,200,
	.97+.01*sin(anim_timer)+.02*sin(2*(anim_timer+1)),
	.97+.01*sin(1.5*(anim_timer+1))+.02*sin(.5*(anim_timer+6.3)),
	2*sin(anim_timer+.2),c_white,1);
	
font_set(fnt_header,fa_center,fa_bottom);
shadowtext(GUIWIDTH*.5,GUIHEIGHT-5,"Best Distance: " + add_commas(BEST_DISTANCE) + " Meters", .5, .5, 0, c_white, 1, 4);

font_set(fnt_reg,fa_left,fa_bottom);
draw_text_ext_transformed_colour(5,GUIHEIGHT-10,"Art:\rTiffa Minsal\rMichael Ha\r\rProgramming:\rSeth Coster\rShi Chen\rAndrew Blaine\r\rMusic\rBoxcat Games\r\rMade in 48 hours for Ludum Dare 38",41,1000,.5, .5, 0, c_white, c_white, c_white, c_white, 1);


/*
shadowtext(GUIWIDTH*.5,100,"DREGUMES!",
	
	,c_white,1,5);
	
