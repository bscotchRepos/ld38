var w = width*scale*.5;
var h = height*scale*.5;
draw_sprite_ext(sprite_index,0,x,y,scale,scale,0,c_white,1);
//draw_set_alpha(.75);
//draw_rectangle_colour(x-w,y-h,x+w,y+h,c_black,c_black,c_black,c_black,0);
//draw_set_alpha(1);

//draw_rectangle_outline(x-w,y-h,x+w,y+h,3,c_white);

font_set(fnt_header, fa_left, fa_top);
var margin = 10*scale;
var tsc = scale*.5;
var nameshow = string_upper(UPGRADES[my_upgrade,upginfo_name]);
draw_text_transformed_colour(x-w+margin*2+160,y-h+margin,nameshow,tsc,tsc,0,c_aqua,c_aqua,c_aqua,c_aqua,1);

draw_sprite_ext(sp_upgrade_icons,my_upgrade,x-w+80*scale,y,scale,scale,0,c_white,1);

var yp = y-h+margin+string_height("W")*.5*tsc;
var xp = x-w+margin*2+string_width(nameshow)*tsc+margin*2+160;

font_set(fnt_reg, fa_left, fa_middle);
tsc = scale*.7;
draw_text_transformed_colour(xp,yp,UPGRADES[my_upgrade,upginfo_description],tsc,tsc,0,c_white,c_white,c_white,c_white,1);

font_set(fnt_header, fa_right, fa_middle);
draw_text_transformed_colour(x+w-margin*2,yp,string(UPGRADES[my_upgrade,upginfo_level]) + "/" + string(UPGRADES[my_upgrade,upginfo_maxlevel]),tsc,tsc,0,c_white,c_white,c_white,c_white,1);

var cur_amt_text = string(upgrade_get_effect(my_upgrade, UPGRADES[my_upgrade, upginfo_level])) + " " + UPGRADES[my_upgrade, upginfo_label];
var next_amt_text = string(upgrade_get_effect(my_upgrade, UPGRADES[my_upgrade, upginfo_level]+1)) + " " + UPGRADES[my_upgrade, upginfo_label];
maxed = UPGRADES[my_upgrade, upginfo_level] >= UPGRADES[my_upgrade, upginfo_maxlevel];

if maxed { next_amt_text = "MAXED!"; }

var tcol = c_white;
can_afford = BALLOOMS >= upgrade_get_cost(my_upgrade);

font_set(fnt_reg, fa_left, fa_middle);
tsc = .75*scale;
draw_text_transformed_colour(x-w+margin*2+160,y+h*.4,cur_amt_text + " >> " + next_amt_text,tsc,tsc,0,tcol,tcol,tcol,tcol,1);

if !maxed {
	font_set(fnt_reg, fa_right, fa_middle);
	tsc = scale;
	var tcol = c_white;
	if !can_afford { tcol = c_red; }
	var bsize = 46;
	var bscale = min(bsize/sprite_get_width(sp_balloom_icon), bsize/sprite_get_height(sp_balloom_icon));
	
	var cost_text = add_commas(upgrade_get_cost(my_upgrade));
	draw_sprite_ext(sp_balloom_icon, 0, x+w-margin*2-bsize*.5, y+h*.4, -bscale, bscale, 0, c_white, 1);
	draw_text_transformed_colour(x+w-margin*2-bsize,y+h*.4,cost_text,tsc,tsc,0,tcol,tcol,tcol,tcol,1);
}