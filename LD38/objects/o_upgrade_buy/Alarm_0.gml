event_inherited();
if can_afford && !maxed {
	play_sound_gv(snd_coinpickup,10,false);
	BALLOOMS -= upgrade_get_cost(my_upgrade);
	UPGRADES[my_upgrade,upginfo_level]++;
	saveload_progress(true);
}