{
    "id": "c46d7f31-801f-405e-bc4e-3cf685ee11f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_upgrade_buy",
    "eventList": [
        {
            "id": "943aa2d0-1a71-43b6-a316-1c9cac73d12a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c46d7f31-801f-405e-bc4e-3cf685ee11f1"
        },
        {
            "id": "c25dcd11-c018-4060-afdf-c2f52c19a6d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c46d7f31-801f-405e-bc4e-3cf685ee11f1"
        },
        {
            "id": "313d5215-6a57-4370-afea-cf573c17d188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c46d7f31-801f-405e-bc4e-3cf685ee11f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "236f484b-4fe3-46ac-8afa-66a6feb95518",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "264ff3f6-a027-46a8-9175-3dc338d6d3e5",
    "visible": true
}