{
    "id": "ea6db6d6-965b-408a-b5e2-ab890fae9f9f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_header",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Berlin Sans FB Demi",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9137a0c5-6afe-4906-98d6-c63bba0e63c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 157,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 1619,
                "y": 320
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7ab3b517-c487-4c6a-984f-6f568e2f6a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 130,
                "offset": 7,
                "shift": 39,
                "w": 26,
                "x": 92,
                "y": 479
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a11d7e9a-4277-40b6-a76f-c6e19a0a3e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 67,
                "offset": 5,
                "shift": 54,
                "w": 45,
                "x": 120,
                "y": 479
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bf3d8d7d-1b21-4bad-b40c-ca9b2d119970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 135,
                "offset": 5,
                "shift": 99,
                "w": 86,
                "x": 944,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f835b941-9679-4b5a-b492-1717a74b7e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 140,
                "offset": 5,
                "shift": 68,
                "w": 59,
                "x": 268,
                "y": 320
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "904c2c18-28b6-458d-9e44-5c1997c9b276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 131,
                "offset": 2,
                "shift": 112,
                "w": 108,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fcf881a9-68bf-43eb-ad12-bfd6d71bfe85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 130,
                "offset": 5,
                "shift": 97,
                "w": 88,
                "x": 1216,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6d7a918b-d266-4728-8d82-a4c496545330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 71,
                "offset": 5,
                "shift": 32,
                "w": 22,
                "x": 232,
                "y": 479
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8e81131e-4748-4f58-8e61-590867e48657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 156,
                "offset": 7,
                "shift": 55,
                "w": 47,
                "x": 598,
                "y": 320
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1255057b-7e5f-4ca2-a684-4b9e8e839be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 156,
                "offset": 2,
                "shift": 55,
                "w": 46,
                "x": 695,
                "y": 320
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "03355ec3-cf88-4480-a5d5-5430e8b98390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 86,
                "offset": 3,
                "shift": 59,
                "w": 53,
                "x": 1901,
                "y": 320
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1774ebd5-e07b-445c-b043-643f66655e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 119,
                "offset": 2,
                "shift": 66,
                "w": 60,
                "x": 848,
                "y": 320
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1e0e0558-5fc2-4b79-9e95-162b56a01a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 146,
                "offset": -1,
                "shift": 39,
                "w": 37,
                "x": 1698,
                "y": 320
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3280dda1-dfdf-40f6-bb04-b4c5bd17422b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 99,
                "offset": 5,
                "shift": 59,
                "w": 49,
                "x": 1811,
                "y": 320
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8a32ac15-c1ae-401d-a55c-c72004ef02dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 129,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 1956,
                "y": 320
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ded9a7db-6151-480e-9e23-b14c8e357438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 137,
                "offset": 2,
                "shift": 46,
                "w": 42,
                "x": 1531,
                "y": 320
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c8ac6be5-b130-40c6-893a-b2db3c4a39aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 130,
                "offset": 3,
                "shift": 88,
                "w": 82,
                "x": 2,
                "y": 161
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "065fdb0a-f24f-46ee-8bfb-b96be378a11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 128,
                "offset": 0,
                "shift": 45,
                "w": 37,
                "x": 1862,
                "y": 320
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "97c146f8-5ab0-44f0-8dc9-d929a76533a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 130,
                "offset": 2,
                "shift": 72,
                "w": 69,
                "x": 1607,
                "y": 161
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "da9861b3-115a-43fd-9d06-173c37cefa2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 130,
                "offset": 2,
                "shift": 68,
                "w": 64,
                "x": 136,
                "y": 320
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0ea6c84f-3453-4fd9-be11-503712e54270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 129,
                "offset": 3,
                "shift": 77,
                "w": 72,
                "x": 1170,
                "y": 161
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7f81006f-90d9-45b7-b4b2-4c2cd58b0b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 130,
                "offset": 3,
                "shift": 70,
                "w": 64,
                "x": 202,
                "y": 320
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6478d49f-c366-43eb-935b-28a4df981351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 130,
                "offset": 3,
                "shift": 77,
                "w": 71,
                "x": 1244,
                "y": 161
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "27859d9c-ad0d-4251-acc5-82466c99010b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 130,
                "offset": 1,
                "shift": 66,
                "w": 63,
                "x": 395,
                "y": 320
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5f0e4b70-70c0-4eaf-addf-9acdd54afdc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 130,
                "offset": 1,
                "shift": 68,
                "w": 65,
                "x": 2,
                "y": 320
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d1f76836-5e48-4d23-a671-bffc62968505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 131,
                "offset": 3,
                "shift": 79,
                "w": 72,
                "x": 1021,
                "y": 161
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b34a8b7c-fea1-4ae5-acc9-922c03c7a6db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 129,
                "offset": 4,
                "shift": 35,
                "w": 27,
                "x": 63,
                "y": 479
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac8b7709-c880-4975-b77b-aa7e9086d354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 146,
                "offset": -4,
                "shift": 34,
                "w": 34,
                "x": 1775,
                "y": 320
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d9ac1b44-2647-4a53-8629-a9b73f659c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 124,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 1341,
                "y": 320
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4be43ffd-4e05-4036-a433-8be030034325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 108,
                "offset": 5,
                "shift": 67,
                "w": 57,
                "x": 1164,
                "y": 320
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "74be9df0-c143-4731-be36-382a4cdd59b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 123,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 1439,
                "y": 320
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4217ac50-98fa-48c9-a73f-a53b95b36e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 130,
                "offset": 3,
                "shift": 59,
                "w": 55,
                "x": 791,
                "y": 320
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "70bdfc16-d4af-403c-a025-e630470312a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 131,
                "offset": 4,
                "shift": 96,
                "w": 86,
                "x": 1474,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c4949dc9-007e-4533-9297-b41b6a7ec6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 129,
                "offset": 0,
                "shift": 96,
                "w": 97,
                "x": 751,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d9e916ff-9e60-4b85-9af0-820c8acabf52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 130,
                "offset": 5,
                "shift": 89,
                "w": 82,
                "x": 1892,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "aeb0b856-131b-4921-ba80-69651fd20b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 130,
                "offset": 4,
                "shift": 87,
                "w": 82,
                "x": 1808,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0ab1a943-943e-4a65-9ac8-0969e83e28b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 129,
                "offset": 7,
                "shift": 99,
                "w": 88,
                "x": 1306,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8fadfdf9-7b9f-41c4-a757-a5a25abf5f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 130,
                "offset": 5,
                "shift": 79,
                "w": 71,
                "x": 1317,
                "y": 161
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0d57e0e9-429d-44e1-ba13-8263a759a5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 129,
                "offset": 5,
                "shift": 78,
                "w": 71,
                "x": 1390,
                "y": 161
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c7974db0-1e2e-430d-ae32-80a3afdf0ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 130,
                "offset": 4,
                "shift": 99,
                "w": 92,
                "x": 850,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2d236c90-33f5-4cb9-b5bd-af1345e1101a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 129,
                "offset": 7,
                "shift": 104,
                "w": 90,
                "x": 1032,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d79cca6c-3d1a-4478-9dc3-330386c23fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 129,
                "offset": 6,
                "shift": 42,
                "w": 30,
                "x": 1990,
                "y": 320
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e189be94-e3d5-4e80-9026-9983b06d248a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 144,
                "offset": 0,
                "shift": 46,
                "w": 40,
                "x": 1489,
                "y": 320
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "69a0b196-70a4-42e5-9ee2-47bdf918c0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 129,
                "offset": 7,
                "shift": 91,
                "w": 83,
                "x": 1638,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6f2abef1-c543-4855-af2d-a0d80a14534a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 129,
                "offset": 7,
                "shift": 76,
                "w": 67,
                "x": 1891,
                "y": 161
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "99f44884-894b-44bc-b79c-f92d1d5b69ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 130,
                "offset": 7,
                "shift": 114,
                "w": 100,
                "x": 551,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "06dee53b-a391-493b-8d5b-88aa13739e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 129,
                "offset": 6,
                "shift": 103,
                "w": 90,
                "x": 1124,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "00ab993f-f54e-4ce5-b2b0-2ab8047445b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 131,
                "offset": 4,
                "shift": 104,
                "w": 96,
                "x": 653,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "65b0280a-c633-4d2c-a4f4-9690305aba26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 129,
                "offset": 5,
                "shift": 91,
                "w": 83,
                "x": 1723,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a5258710-4a02-4ffd-a86c-c546df416e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 145,
                "offset": 4,
                "shift": 104,
                "w": 96,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a59e4ea0-5ee9-4116-b97a-63ed3180cb54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 128,
                "offset": 7,
                "shift": 89,
                "w": 81,
                "x": 551,
                "y": 161
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "415263fc-f1cf-43c4-b399-b266a3186c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 132,
                "offset": 3,
                "shift": 58,
                "w": 50,
                "x": 1060,
                "y": 320
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "080fba1b-b7f7-4ab8-b247-c8cb2de28424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 129,
                "offset": 0,
                "shift": 73,
                "w": 73,
                "x": 1095,
                "y": 161
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ee555f95-88f6-4db4-81e5-ba03d2286477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 130,
                "offset": 6,
                "shift": 94,
                "w": 82,
                "x": 86,
                "y": 161
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1c8d728d-ac8d-4019-8052-b31dd16b6b21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 128,
                "offset": 5,
                "shift": 91,
                "w": 82,
                "x": 323,
                "y": 161
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7023c2cd-d1b8-4e49-94b5-05cf81120f70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 128,
                "offset": 5,
                "shift": 131,
                "w": 122,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e311915d-7d20-4c8a-8bb7-fbd2edec50f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 129,
                "offset": 4,
                "shift": 86,
                "w": 79,
                "x": 634,
                "y": 161
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "24df54be-c3be-47a9-8c8c-3107d1e79a4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 128,
                "offset": 1,
                "shift": 86,
                "w": 82,
                "x": 239,
                "y": 161
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f2dd85ea-1c82-4431-9afb-6567d51e347a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 129,
                "offset": 4,
                "shift": 75,
                "w": 67,
                "x": 1960,
                "y": 161
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7a4b1b3d-5ca5-42ca-98d5-0425adf22e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 154,
                "offset": 9,
                "shift": 57,
                "w": 46,
                "x": 910,
                "y": 320
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "aec43033-af0d-4dfa-8482-63e462787a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 137,
                "offset": 3,
                "shift": 46,
                "w": 40,
                "x": 1656,
                "y": 320
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8f260082-d901-4a6f-a421-1bb07ca6fb46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 155,
                "offset": 6,
                "shift": 57,
                "w": 45,
                "x": 958,
                "y": 320
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "48ad211a-7f69-4254-8b34-ce5e588c29fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 90,
                "offset": 3,
                "shift": 72,
                "w": 67,
                "x": 1272,
                "y": 320
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5cc87cc2-c16e-43ac-a2d9-3def6f5462af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 157,
                "offset": -2,
                "shift": 64,
                "w": 67,
                "x": 170,
                "y": 161
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3562ba27-1150-45f3-9125-d7b5dad42088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 54,
                "offset": 4,
                "shift": 51,
                "w": 42,
                "x": 188,
                "y": 479
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8e81a6dd-5df8-4e70-8e7f-4d0b595f6a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 130,
                "offset": 3,
                "shift": 82,
                "w": 73,
                "x": 946,
                "y": 161
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a88bc267-c174-4f03-9f00-aec1ee5a5f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 129,
                "offset": 5,
                "shift": 82,
                "w": 74,
                "x": 870,
                "y": 161
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "de6708b5-bd54-4110-9578-4242b05785ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 130,
                "offset": 3,
                "shift": 58,
                "w": 53,
                "x": 1005,
                "y": 320
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1e4232fe-1f95-46c2-ab2b-f793770be85f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 129,
                "offset": 3,
                "shift": 82,
                "w": 75,
                "x": 793,
                "y": 161
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cb62f183-7953-4a97-862c-776590aee005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 130,
                "offset": 3,
                "shift": 70,
                "w": 65,
                "x": 69,
                "y": 320
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bae4fafe-2b83-49f4-bda1-af6abd438017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 129,
                "offset": 2,
                "shift": 49,
                "w": 46,
                "x": 1391,
                "y": 320
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e0f1dbf1-e4f0-404f-a3c8-52f2ba1a0d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 147,
                "offset": 3,
                "shift": 79,
                "w": 71,
                "x": 407,
                "y": 161
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8be2f922-9a61-4600-b0cd-45e3229bb894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 129,
                "offset": 4,
                "shift": 79,
                "w": 70,
                "x": 1535,
                "y": 161
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9548b39a-f282-4c0d-a374-6ea45c90d17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 128,
                "offset": 5,
                "shift": 38,
                "w": 28,
                "x": 33,
                "y": 479
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e8f040d0-878f-4147-a292-328ead0384b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 146,
                "offset": -2,
                "shift": 39,
                "w": 36,
                "x": 1737,
                "y": 320
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "10a40322-84eb-49e7-9f07-783adba6b1a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 129,
                "offset": 6,
                "shift": 84,
                "w": 76,
                "x": 715,
                "y": 161
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3af9a6ec-c2f4-4f69-803d-b9cccc92872b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 129,
                "offset": 5,
                "shift": 37,
                "w": 29,
                "x": 2,
                "y": 479
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c53c8475-a2f8-4694-8eab-072e22a7fc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 129,
                "offset": 5,
                "shift": 123,
                "w": 111,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "29489568-ad87-4a4c-8b31-50dd75242b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 128,
                "offset": 4,
                "shift": 79,
                "w": 70,
                "x": 1678,
                "y": 161
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d6acc1a8-e83f-49de-b121-8c1015ba9c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 129,
                "offset": 3,
                "shift": 74,
                "w": 69,
                "x": 1750,
                "y": 161
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "acd305f9-2abf-4450-8126-5c1b9f7c304e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 149,
                "offset": 5,
                "shift": 84,
                "w": 76,
                "x": 1396,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fc8fa6e5-789f-4afe-a765-ca0ef5bdd5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 149,
                "offset": 3,
                "shift": 82,
                "w": 74,
                "x": 1562,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1212be83-32a9-4272-a4c2-abe860022b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 129,
                "offset": 4,
                "shift": 53,
                "w": 47,
                "x": 1223,
                "y": 320
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1f72eef5-28e5-4b69-8449-85e54ef88bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 131,
                "offset": 3,
                "shift": 49,
                "w": 42,
                "x": 1575,
                "y": 320
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "518126c6-84b2-439e-96c7-c25663922575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 129,
                "offset": 1,
                "shift": 53,
                "w": 50,
                "x": 1112,
                "y": 320
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d7557402-fc27-4453-aa40-fbf71ab370ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 130,
                "offset": 6,
                "shift": 81,
                "w": 70,
                "x": 1463,
                "y": 161
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0874cfcb-582c-4b94-9480-2428e11c0a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 129,
                "offset": 3,
                "shift": 75,
                "w": 68,
                "x": 1821,
                "y": 161
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0b52a281-b9c6-4447-961d-54a3d8c782e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 129,
                "offset": 2,
                "shift": 106,
                "w": 102,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "495603e1-b8d0-4819-9121-b94a40b1b534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 129,
                "offset": 2,
                "shift": 68,
                "w": 64,
                "x": 329,
                "y": 320
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c60f06d7-0dfd-4cd4-ab3d-f07979600a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 151,
                "offset": 4,
                "shift": 75,
                "w": 69,
                "x": 480,
                "y": 161
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f996963d-99da-4e18-af43-0abe4187d931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 129,
                "offset": 4,
                "shift": 65,
                "w": 60,
                "x": 536,
                "y": 320
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "57f76b48-db68-4497-bc7b-4b2982b7bff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 156,
                "offset": 4,
                "shift": 54,
                "w": 46,
                "x": 743,
                "y": 320
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "43692cb0-3855-47bf-be3a-b02db1af8da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 138,
                "offset": 12,
                "shift": 43,
                "w": 19,
                "x": 167,
                "y": 479
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "06f22334-21bb-4673-b57d-1471370c9fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 156,
                "offset": 4,
                "shift": 54,
                "w": 46,
                "x": 647,
                "y": 320
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "57d6536b-f44b-4b32-83f0-7a390d11ec2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 105,
                "offset": 1,
                "shift": 75,
                "w": 74,
                "x": 460,
                "y": 320
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "DRAGOOMS\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 104,
    "styleName": "Bold",
    "textureGroup": 0
}