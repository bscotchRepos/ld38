{
    "id": "32c7c941-7855-4457-877f-bd089b3bfb68",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "AR ESSENCE",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e0903c2c-072a-4127-af1b-f616f4b42e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9b0ba608-0073-4272-bba8-59c113087c5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 366,
                "y": 191
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "da7dbe93-8950-4067-b075-587a9271a507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 313,
                "y": 191
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "21c120bc-6259-49f6-8936-9b1358a03f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1c50af80-bb89-44e8-aaf5-40e04d0e601c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 243,
                "y": 128
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8d9ccae8-86b9-4a31-a3d0-eff62923fb6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fa48836f-bc0c-4f4b-87ad-84f5f0d032b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c8769941-afb1-4f3b-b1ad-5b7228837289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 399,
                "y": 191
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cebe9299-2ad5-4a60-b10f-01bfb374721f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 172,
                "y": 191
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9fa9a4a7-2057-4390-9939-5e226edc1b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 186,
                "y": 191
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "20fd9330-61da-471f-afe3-808d9d0510ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 27,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 287,
                "y": 191
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a4ac9437-2f9a-413c-a98c-447dfce4784f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 44,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 169,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "18fd66ea-e967-43eb-af52-b5e8bffe551d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 330,
                "y": 191
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "64e22d15-68ef-4ad9-84d6-8b1e9afb07cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 200,
                "y": 191
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7e3b8bcc-c143-4b71-bcad-a8340d36c424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 375,
                "y": 191
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "523beac4-bbb3-4ad5-8696-50435810c609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 383,
                "y": 128
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b1e5d60c-10a4-470d-a0fc-76dbf767a845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 328,
                "y": 65
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "09f2aa92-eb10-49c3-b71f-00759f120320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 217,
                "y": 191
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f91c24dc-3695-46ac-968a-46a877f7029b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 201,
                "y": 128
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "697ac819-66a7-4208-9ed2-01640751f5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 323,
                "y": 128
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5a21f64c-1de7-475a-87ff-4f4951fad9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": -1,
                "shift": 22,
                "w": 21,
                "x": 351,
                "y": 65
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "10254557-9b28-4659-9297-ed6b82160557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 477,
                "y": 128
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bf3888da-4605-4f16-b020-af950889678f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 138,
                "y": 128
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "530efd8e-a81a-467b-b6e4-ac4836fc1ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 397,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a9b0d635-9b5d-4102-aebb-c6a8fb6bd105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 282,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a38d426e-5174-45db-b87b-8c6f2b158e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 463,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bff6615c-2bd0-4652-a493-0e521dac1dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 348,
                "y": 191
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "117851e7-d300-43a7-b5cb-074c370a530c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 339,
                "y": 191
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d2b7933c-5979-4ce4-bb30-c897df7d228e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 43,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 31,
                "y": 128
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9b4e9185-fb05-4c9c-a40e-e2dc66eeaf0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 436,
                "y": 128
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ca72cdec-890d-4afa-a0d9-7ddb530f1feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 43,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 53,
                "y": 128
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "23471f26-cd99-4b9c-b5c6-8af199184e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 123,
                "y": 191
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6d1bfe5c-31fe-4b1c-ba4c-ec5ab910ca27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 54,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7e9a66f9-b5cc-4d87-bd87-ebc5be965605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "661cba17-0dbb-41fe-b1ea-05680800821e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 374,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "58c543b2-9786-48cc-be5f-84a2e6ca382d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 303,
                "y": 128
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "94fe688e-faa2-48e8-ba56-b3391b8c3085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 77,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e2b8c465-180a-4d8a-9f6c-f9c394d9e226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 117,
                "y": 128
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "239d7d6e-7ca0-4a59-a5bc-56a0d522d437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 458,
                "y": 128
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1cfd499e-56a6-4091-be1b-8b3b4da50371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 52,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ea20d141-02b9-4879-8416-79fb37637172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 214,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bb24310e-f9bf-4496-ac94-056a238f3699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 303,
                "y": 191
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "81c6a2b8-9992-41d1-8e9f-a9cd7eae73bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 53,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 419,
                "y": 128
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fd860363-9b99-4b3f-a4d0-bacf9a8ef709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 259,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d6e95678-a90f-4942-8f5e-60e50079f475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 363,
                "y": 128
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4e1e7482-f031-489d-a7aa-a3f3b05159ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c1d8b912-73c3-4810-a709-67e1212101ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9cb71954-7327-44f6-a016-a0eaaaa5e62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 102,
                "y": 65
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b9ccf7a3-1daa-415e-8cd2-3a66894476c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 222,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e4ac5799-fd8c-47d2-b5e3-bfc525b0ee51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 51,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f865735c-686d-4536-95b8-3b34c9124056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "139dee4c-3934-4b0e-8cf1-6099894b9123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 159,
                "y": 128
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c7573a9a-9f2a-4bec-95f5-77d97adc7532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 305,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ab25e91d-714c-4e8f-bcd7-874b0a118364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 27,
                "y": 65
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f020e031-5987-4726-a2f6-01918a1cbfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b9ea81fe-fc54-41f3-9a91-8e2fa8c4e3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "56f2638b-b928-40ba-9768-e11bfe3393c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "acf91361-01b6-4d41-a577-af3c467a5f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d3e3b3f1-b2c7-4bb2-a0db-9fa31feef710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 441,
                "y": 65
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "056c38d9-02f0-4147-bbfd-cc85aeb0bdc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 244,
                "y": 191
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8ec81a3c-b5d6-4eb0-bc77-5deafc50b0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2909d0fe-059a-45c0-ac85-402f7c3566aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 276,
                "y": 191
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b9247f0b-a119-4e4a-b472-895993f7d7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 256,
                "y": 191
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ec0eb430-375a-420b-b63c-dbfc450047d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1ad15156-7c53-4a1d-9154-fb3c4563a2f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 408,
                "y": 191
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7c02142f-c73b-403a-b95d-bb869acee247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 69,
                "y": 191
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c2e32f1d-c821-4ef7-8155-108a1355e6a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 180,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5eb4af6c-88d6-4052-a8ea-a2f43280e46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 87,
                "y": 191
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3508b0e3-b98a-4f59-96f5-c9f20a862242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 96,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9c5ea3ec-6b14-4e76-84ff-9cce737468c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 283,
                "y": 128
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "54167bf2-02a7-406a-acab-fc7ce59f7587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 156,
                "y": 191
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2799fa57-b58a-4fac-aaaf-aa3fc3e6b587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 57,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 127,
                "y": 65
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4e952424-a2e9-4f96-8d98-100bf8ab7fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 51,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 147,
                "y": 65
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c86f2b12-2563-4bb1-9dc7-20ddd653209f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 357,
                "y": 191
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "80f059ca-a8e0-4632-bf4b-6f7bbc81111a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 57,
                "offset": -2,
                "shift": 13,
                "w": 13,
                "x": 36,
                "y": 191
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "87393c4b-c2de-4e78-8128-9152514bf407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 263,
                "y": 128
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "80650ce8-fb44-4c6d-8997-7a2c7edc8326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 392,
                "y": 191
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9ba99111-8bad-4e10-95f6-d7b6513b3985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 51,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "67ec7608-f17a-4785-8f17-125f83bb62bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 51,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 238,
                "y": 65
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "241cc63d-4016-4676-9ce2-f29d2a7e28cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 484,
                "y": 65
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e54c2e8b-aa77-4335-955c-e68418df6244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f854b1a2-c6a8-493a-94b3-de537a4aa0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 194,
                "y": 65
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1a8094fe-1c1b-4d25-a480-8a11e517f1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 231,
                "y": 191
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cefb0b64-0b55-4b95-abfb-06f70c625e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 105,
                "y": 191
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6d0ce896-99c4-4ffb-aac9-c66d44ac7f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 140,
                "y": 191
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b9c6daa6-f9e2-4e60-a5fb-f811ef086636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 343,
                "y": 128
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0ef5f20d-396e-48d9-9032-a246639d5265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 75,
                "y": 128
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "69d694cc-371e-42e2-ba19-fa0ecaa4ebea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1cf1e9c7-172f-4f8d-b026-27ab84fb7660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 419,
                "y": 65
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0d2a19b2-b1ef-439f-a06e-3e22cb9fabed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 57,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7df30a14-c9ec-41aa-a071-14e1a6f67647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 51,
                "y": 191
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9486aa8f-8dad-4d0d-ba51-abb050e29a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 53,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 402,
                "y": 128
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "da021ca4-ec22-4ca5-bdbd-64e52603e9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 52,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 384,
                "y": 191
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c1131167-30b6-4b1f-a897-01531294549e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 53,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 191
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a6110a19-5a4c-4b64-9454-fd90f553eb51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 128
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Regular",
    "textureGroup": 0
}